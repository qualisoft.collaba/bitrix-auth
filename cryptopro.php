<?/*
function SetupStore($location, $name, $mode)
{
    $store = new CPStore();
    return $store->Open($location, $name, $mode);
}

function SetupCertificates($location, $name, $mode)
{
    $store = SetupStore($location, $name, $mode);
    return $store->get_Certificates();
}

function SetupCertificate($location, $name, $mode,
                           $find_type, $query, $valid_only,
                           $number)
{
    $certs = SetupCertificates($location, $name, $mode);
    if ($find_type != NULL)
    {
        $certs = $certs->Find($find_type, $query, $valid_only);
        if (is_string($certs))
            return $certs;
        else
            return $certs->Item($number);
    }
    else
    {
        $cert = $certs->Item($number);
        return $cert;
    }
}

try
{
    $content = "test content";
    $tsp_addres = "http://testca.cryptopro.ru/tsp/tsp.srf";
    $cert = SetupCertificate(CURRENT_USER_STORE, "My", STORE_OPEN_READ_ONLY,
                             CERTIFICATE_FIND_SUBJECT_NAME, "test", 0,
                             1);

    if (!$cert)
    {
        printf("Certificate not found\n");
        return;
    }

    $signer = new CPSigner();
    $signer->set_TSAAddress($tsp_addres);
    $signer->set_Certificate($cert);

    $sd = new CPSignedData();
    $sd->set_Content($content);

    $sm = $sd->Sign($signer, 0, STRING_TO_UCS2LE);
    printf("Signature is:\n");
    printf($sm);
    printf("\n");
    $sd->Verify($sm, 0, VERIFY_SIGNATURE_ONLY);
    printf("Verify OK\n");
}
catch (Exception $e)
{
    printf($e->getMessage());
}

*/?>  
<script>
public function actionSign()
{

    if (\Yii::$app->request->post())
    {
        $rr = new RequestResponse();

        $fileName   = \Yii::$app->request->post('fileName');
        $signature  = \Yii::$app->request->post('signature');
        $cert       = \Yii::$app->request->post('cert');

        if ($signature && $fileName && $cert)
        {

            $fileName = ProjectComponent::translit($fileName);

            $dirName = md5(time() . rand(1, 1000));
            $rootDir = \Yii::getAlias('@frontend/web');
            $dir = $rootDir . '/tmp/signs/' . $dirName;
            $filePath = $dir . "/" . $fileName . ".sgn";

            if (FileHelper::createDirectory($dir))
            {
                $file = fopen($filePath, "w+");
                fwrite($file, $signature);
                fclose($file);
            }

            $rr->data['src'] = \Yii::getAlias('@web/tmp/signs/' . $dirName . "/" . $fileName . ".sgn");
            $rr->data['name'] = $fileName . ".sgn";

            $rr->success = true;
            $rr->message = '';


        } else
        {
            $rr->success = false;
            $rr->message = 'Недостаточно данных';
        }

        return $rr;

    }
    return $this->render($this->action->id);
}
</script>


<div class="row">
    <div class="col-md-12">

        <?= \frontend\widgets\CryptoProWidget::widget(); ?>

        <div class="sx-content-wrapper">

            <div style="display: none; text-align: center;" id="sx-process-crypto-info-global">
                <? \yii\bootstrap\Alert::begin([
                    'options' =>
                    [
                        'class' => 'alert-info',
                    ]
                ]); ?>
                    Исполнение может занять некоторое время. Ожидайте...
                <? \yii\bootstrap\Alert::end(); ?>
            </div>

            <div class="sx-hidden-success">
                <?
                $model = new \yii\base\DynamicModel(['file']);

                $form = \yii\bootstrap\ActiveForm::begin([
                        'action' => "/tools/sign-and-encrypt",
                        'id' => "sx-sign-form",
                        'options' => [
                            'enctype' => 'multipart/form-data'
                        ]
                    ]); ?>

                    <?= $form->field($model, 'file')->label("Файл для подписи")->fileInput([
                        'class' => 'sx-app-file'
                    ]); ?>

                    <div style="display: none;">
                        <input type="hidden" name="sgn" id="sx-sgn-file"/>
                        <button type="submit">Отправить</button>
                    </div>

                <? \yii\bootstrap\ActiveForm::end(); ?>

                <div style="text-align: center">
                    <div style="display: none; text-align: center;" id="sx-process-crypto-info">
                        <? \yii\bootstrap\Alert::begin([
                            'options' =>
                            [
                                'class' => 'alert-info',
                            ]
                        ]); ?>
                            Исполнение может занять некоторое время. Ожидайте...
                        <? \yii\bootstrap\Alert::end(); ?>
                    </div>

                    <button class="btn btn-lg btn-primary sx-btn-submit" onclick="sx.FileApiSigner.execute(); return false;">Подписать</button>
                </div>
            </div>

            <div style="display: none; text-align: center;" id="sx-result-wrapper-succss">

                <? \yii\bootstrap\Alert::begin([
                    'options' =>
                    [
                        'class' => 'alert-success',
                    ]
                ]); ?>
                    Вы можете скачать или просмотреть подпись, зашифровать файл и подпись
                <? \yii\bootstrap\Alert::end(); ?>
                <a href="#" class="btn btn-lg btn-primary sx-btn-download" target="_blank">Скачать подпись</a>
                <a href="#" class="btn btn-lg btn-primary sx-btn-view" target="_blank">Посмотреть подпись</a>
                <a href="#" class="btn btn-lg btn-primary sx-btn-encrypt" target="_blank">Зашифровать</a>
            </div>
            <div style="display: none;">
                <div>
                    Файл подписи: <div id="sx-result-file">Тут будет результат</div>
                </div>

                <div>
                    <p id="info_msg" name="SignatureTitle">содержимое:</p>
                    <div id="item_border">
                        <textarea id="SignatureTxtBox" readonly style="font-size:9pt;height:100px;width:100%;resize:none;border:0;">
                        </textarea>
                    </div>
                </div>
            </div>


            <div class="sx-show-success" style="display: none;">
                <hr />
                <a href="#" onclick="window.location.reload(); return false;" class="pull-right btn btn-default">
                    <i class="glyphicon glyphicon-repeat"></i>
                    Подисать еще один файл
                </a>
            </div>
        </div>



    </div>
</div>
