<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
//header('Content-Type: application/json; charset=UTF-8');
global $USER;
							//AddMessage2Log($_POST, true);

CModule::IncludeModule("iblock");

$el = new CIBlockElement;

if($_POST["DELETE"]=="Y")
{
		$_SESSION["ZAYAVKA_STATUS"] = "Заявка №".$_POST['ID']." удалена";

    $DB->StartTransaction();
    if(!CIBlockElement::Delete($_POST['ID']))
    {
        echo "Error";
        $DB->Rollback();
    }
    else
        $DB->Commit();
    
}
else
{
         //echo 'PRINT_R:<pre style="font:16px Courier">', print_r($ar), '</pre>';
	if(isset($_POST['ID'])){
         //$arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$_POST['FIZ_BALANCE']);
         //CIBlockElement::SetPropertyValueCode($_POST['ID'], "FIZ_BALANCE", $arFile);
			
	}	 
    if(empty($_POST['ID'])){
          $rs = CIBlockElement::GetList(array("ID"=>'DESC'), array("IBLOCK_ID"=>3), false,  array("nPageSize" => "1"),array("ID", "PROPERTY_NOMER_ZAYAVKI")); 
          if($ar = $rs->GetNext())
          $nomer_zayavki=$ar["PROPERTY_NOMER_ZAYAVKI_VALUE"]+1;
            $_POST['B'] = str_replace(' ', '', $_POST['B']);
            $PROP = array_merge($_POST, $_FILES);  
            
            //$PROP["FILE"] = CFile::MakeFileArray($_FILES); 
    
            $PROP['DATE_STATUS'] = date('d.m.Y H:i');
            $PROP['NOMER_ZAYAVKI'] = $nomer_zayavki;
            $PROP['OGRN_PRINC'] = $_POST["OGRN_PRINC"];
			$PROP['NMC'] = $_POST["NMC"];
			$PROP['INN_ZAK'] = $_POST["INN_ZAK"];
			$PROP['KPP_ZAK'] = $_POST["KPP_ZAK"];
			$PROP['OGRN_ZAK'] = $_POST["OGRN_ZAK"];
			$PROP['BENEFIC'] = $_POST["BENEFIC"];
			$PROP['LINK_ZAKUPKA'] = $_POST["LINK_ZAKUPKA"];
            
			for($i = 1; $i < 19; $i++)$PROP[$i + 133] = $_POST["prop"][$i - 1];
			
			
			
            if($_POST['A'])$name = $_POST['A'];
            else $name = date('d.m.Y H:i');
    
            $arLoadProductArray = Array(
              "MODIFIED_BY"    => $USER->GetID(),
              "DATE_CREATE" => date('d.m.Y H:i'), 
              "IBLOCK_SECTION_ID" => false,       
              "IBLOCK_ID"      => 3,
              "PROPERTY_VALUES"=> $PROP,
              "DETAIL_TEXT"    => '',
              "NAME"           => $name,
              "CODE"           => date('d.m.Y H:i:s'),
              "ACTIVE"         => "Y",            
              );
    
            if($PRODUCT_ID = $el->Add($arLoadProductArray)) {			
			    $arFile_FIZ_BALANCE = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$_POST['FIZ_BALANCE']);
                CIBlockElement::SetPropertyValueCode($PRODUCT_ID, "FIZ_BALANCE", $arFile_FIZ_BALANCE);
				
				$arFile_CB = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$_POST['CB']);
                CIBlockElement::SetPropertyValueCode($PRODUCT_ID, "CB", $arFile_CB);  
				
				$arFile_SOGLASIE_V_BURO = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$_POST['SOGLASIE_V_BURO']);
                CIBlockElement::SetPropertyValueCode($PRODUCT_ID, "SOGLASIE_V_BURO", $arFile_SOGLASIE_V_BURO);
				
				$arFile_EGRUL = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$_POST['EGRUL']);
                CIBlockElement::SetPropertyValueCode($PRODUCT_ID, "EGRUL", $arFile_EGRUL);
				
				$arFile_ARTICLES = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$_POST['ARTICLES']);
                CIBlockElement::SetPropertyValueCode($PRODUCT_ID, "ARTICLES", $arFile_ARTICLES);
				
				$arFile_RESH_O_NAZNACH = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$_POST['RESH_O_NAZNACH']);
                CIBlockElement::SetPropertyValueCode($PRODUCT_ID, "RESH_O_NAZNACH", $arFile_RESH_O_NAZNACH);
				
				$arFile_POROTOCOL_OD_SDELKI = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$_POST['POROTOCOL_OD_SDELKI']);
                CIBlockElement::SetPropertyValueCode($PRODUCT_ID, "POROTOCOL_OD_SDELKI", $arFile_POROTOCOL_OD_SDELKI);
				
				$arFile_BUH_OT_IFNS = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$_POST['BUH_OT_IFNS']);
                CIBlockElement::SetPropertyValueCode($PRODUCT_ID, "BUH_OT_IFNS", $arFile_BUH_OT_IFNS);
				
				$arFile_SPRAVKI_IFNS = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$_POST['SPRAVKI_IFNS']);
                CIBlockElement::SetPropertyValueCode($PRODUCT_ID, "SPRAVKI_IFNS", $arFile_SPRAVKI_IFNS);
				
				$arFile_SPRAVKI_IFNS_RAS_SH = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$_POST['SPRAVKI_IFNS_RAS_SH']);
                CIBlockElement::SetPropertyValueCode($PRODUCT_ID, "SPRAVKI_IFNS_RAS_SH", $arFile_SPRAVKI_IFNS_RAS_SH);
				
				$arFile_DOC_PODTV_PRAVO = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$_POST['DOC_PODTV_PRAVO']);
                CIBlockElement::SetPropertyValueCode($PRODUCT_ID, "DOC_PODTV_PRAVO", $arFile_DOC_PODTV_PRAVO);
				
				$arFile_OTZYVY_KONTR = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$_POST['OTZYVY_KONTR']);
                CIBlockElement::SetPropertyValueCode($PRODUCT_ID, "OTZYVY_KONTR", $arFile_OTZYVY_KONTR);
				
				$arFile_OTZYVY_KRED = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$_POST['OTZYVY_KRED']);
                CIBlockElement::SetPropertyValueCode($PRODUCT_ID, "OTZYVY_KRED", $arFile_OTZYVY_KRED);
				
				
				$elem = GetIBlockElement($PRODUCT_ID);
				
				$zip = new ZipArchive();
				$zip_name = $_SERVER['DOCUMENT_ROOT']."/archives/".$elem["PROPERTIES"]['M']['VALUE']."/".$PRODUCT_ID.".zip"; // имя файла
				if($zip->open($zip_name, ZIPARCHIVE::CREATE)!==TRUE)
				{
				  $error = "Sorry ZIP creation failed at this time";
				}
				$zip->addFile($_SERVER['DOCUMENT_ROOT']."/archives/".$elem["PROPERTIES"]['M']['VALUE']."/fin.xlsm", "fin.xlsm"); // добавляем файлы в zip архив
				
				
				
				$URL_CB = CFile::GetPath($elem["PROPERTIES"]['CB']['VALUE']);
				$file_CB = explode(".", $URL_CB);
				$zip->addFile($_SERVER['DOCUMENT_ROOT']."/".$URL_CB, "pasport.".$file_CB[1]); // добавляем файлы в zip архив
				
				$URL_FIZ_BALANCE = CFile::GetPath($elem["PROPERTIES"]['FIZ_BALANCE']['VALUE']);
				$file_FIZ_BALANCE = explode(".", $URL_FIZ_BALANCE);
				$zip->addFile($_SERVER['DOCUMENT_ROOT']."/".$URL_FIZ_BALANCE, "balance.".$file_FIZ_BALANCE[1]); // добавляем файлы в zip архив
				
				$URL_SOGLASIE_V_BURO = CFile::GetPath($elem["PROPERTIES"]['SOGLASIE_V_BURO']['VALUE']);
				$file_SOGLASIE_V_BURO = explode(".", $URL_SOGLASIE_V_BURO);
				$zip->addFile($_SERVER['DOCUMENT_ROOT']."/".$URL_SOGLASIE_V_BURO, "soglasie.".$file_SOGLASIE_V_BURO[1]); // добавляем файлы в zip архив				
				$zip->close(); 


                
                    $result['type'] = 'true';
                    $arEventFields = array(
                        'URL'=>'http://'.$_SERVER['SERVER_NAME'].'/zayavki/detail.php?id='.$PRODUCT_ID,
                        'NAME'=>$arLoadProductArray["NAME"],
                        'ID' => $nomer_zayavki,
                    );
                    changeStatus($PRODUCT_ID,15);
                    //CEvent::Send("NEW_BG", 's1', $arEventFields);
                    if ($_POST['STATUS']==16){
                     CEvent::Send("NEW_BG", 's1', $arEventFields);    
                    }
                    
            } else {
                    $result['type'] = 'false';
                    $result['text'] = $el->LAST_ERROR;
                    $result['text'] = str_replace('Не введено название.<br>', 'Номер извещения не заполнен', $result['text']);
            }
    }else{

            if($_POST['A'] || $_POST['A']!='&nbsp')$name = $_POST['A'];
            else $name = date('d.m.Y H:i');
          $rs = CIBlockElement::GetList(array("ID"=>'DESC'), array("IBLOCK_ID"=>3, "ID"=>$_POST['ID']), false,  array("nPageSize" => "1"),array("ID", "PROPERTY_NOMER_ZAYAVKI")); 
          if($ar = $rs->GetNext())
          $nomer_zayavki=$ar["PROPERTY_NOMER_ZAYAVKI_VALUE"];


            changeStatus($_POST['ID'],$_POST['STATUS']);
    
   
            foreach($_FILES as $k=>$item){
                if(empty($_FILES[$k]['name'])){
                        unset($_FILES[$k]);
                }
            }
            $_POST['B'] = str_replace(' ', '', $_POST['B']);
            $PROP = array_merge($_POST, $_FILES);
            
           // preprint($PROP);
           // die();
            unset($PROP['ID']);
            $PROP['DATE_STATUS'] = date('d.m.Y H:i');
            //$PROP['NOMER_ZAYAVKI'] = $nomer_zayavki;
            
            CIBlockElement::SetPropertyValuesEx($_POST['ID'],3,$PROP);
            
            $arLoadProductArray = Array(
              "IBLOCK_SECTION_ID" => false,
              //"DATE_CREATE" => date('d.m.Y H:i'),       
              "IBLOCK_ID"      => 3,
              "DETAIL_TEXT"    => '',
              "NAME"           => $name,
              "CODE"           => date('d.m.Y H:i:s'),
              "ACTIVE"         => "Y",            
              );

            if($_POST["STATUS"]==16) {
                
              $arEventFields = array(
                        'URL'=>'http://'.$_SERVER['SERVER_NAME'].'/zayavki/detail.php?id='.$_POST['ID'],
                        'NAME'=>$nomer_zayavki,
                        'ID' => $nomer_zayavki,
                         );
               //CEvent::Send("NEW_BG", 's1', $arEventFields);
            }

              
            if($el->Update($_POST['ID'],$arLoadProductArray)){
              $result['type'] = 'true';
							if($_POST["STATUS"]==16) {
								$_SESSION["ZAYAVKA_STATUS"] = "Заявка №".$nomer_zayavki." передана в банк";
                                $arEventFields = array(
                                   'URL'=>'http://'.$_SERVER['SERVER_NAME'].'/zayavki/detail.php?id='.$_POST['ID'],
                                   'NAME'=>$arLoadProductArray["NAME"],
                                    'ID' => $nomer_zayavki,
                                );
                                
                                //CEvent::Send("NEW_BG", 's1', $arEventFields);
	            	//отправляем почту
						/*		AddMessage2Log("add_BG.php: Запись с кодом ".$nomer_zayavki." изменена.\n"); //print_r($result, true)
								$adminEmail = COption::GetOptionString('main', 'email_from', 'default@admin.email');
								$subject ="Новая Заявка ".$nomer_zayavki." со статусом \"Передано в банк\"";
								$text= "Новая Заявка ".$nomer_zayavki." со статусом \"Передано в банк\". Ссылка: http://".$_SERVER['SERVER_NAME'].
								"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=3&type=zayavki&ID=".$_POST['ID']."&lang=ru";
                         */       
                                
						//		if(mail($adminEmail, $subject, $text) )
                        		if(CEvent::Send("NEW_BG", 's1', $arEventFields)>0 )
								{ AddMessage2Log('add_BG.php: Успешно отправлено!'); }
                                
								else{ AddMessage2Log('add_BG.php: Отправка не удалась!'); }
							}elseif($_POST["STATUS"]==15) {
								$_SESSION["ZAYAVKA_STATUS"] = "Черновик №".$nomer_zayavki." сохранен";
							}
            } else {
                    $result['type'] = 'false';
                    $result['text'] = $el->LAST_ERROR;
                    $result['text'] = str_replace('Не введено название.<br>', 'Номер извещения не заполнен', $result['text']);
            }
    
            
    }
}
if($_POST["STATUS"]==16) {
    if(isset($PRODUCT_ID))LocalRedirect('https://pbg.nodomain.me/zayavki/dobavlenie-zayavki/?id_zayvki='.$PRODUCT_ID.'&finish');
	else LocalRedirect('https://pbg.nodomain.me/zayavki/dobavlenie-zayavki/?id_zayvki='.$_POST['ID'].'&finish');
} else {
    LocalRedirect('/zayavki/');
}
//echo json_encode($result);