<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
header('Content-Type: application/json; charset=UTF-8');
global $USER;

if((empty($_POST['password']))||(empty($_POST['new_password']))){
        $result['type'] = 'false';
        $result['text'] = 'Поля не заполнены';
}

if($_POST['password']!=$_POST['new_password']){
        $result['type'] = 'false';
        $result['text'] = 'Пароли не совпадают';
}else{
        $user = new CUser;
        $fields = Array(
                "PASSWORD"          => $_POST['password'],
                "CONFIRM_PASSWORD"  => $_POST['new_password'],
        );
        if ($user->Update($_POST['id_element'], $fields)){
                $result['type'] = 'true';
        }else{
                $result['type'] = 'false';
                $result['text'] = $user->LAST_ERROR;
        }
        
}

echo json_encode($result);