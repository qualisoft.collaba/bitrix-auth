<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
header('Content-Type: application/json; charset=UTF-8');
//preprint($_POST);
if(($_POST['BG']<10000000)){
        if($_POST['NG'] == 9){                          //общая
                ob_start();
                        require($_SERVER["DOCUMENT_ROOT"]."/include/result_a_a.php");
                        require($_SERVER["DOCUMENT_ROOT"]."/include/result_a_u.php");
                $text = ob_get_contents();
                ob_clean();
                ob_end_clean();
        }elseif($_POST['NG'] == 10){                    //упрощенка
                ob_start();
                        require($_SERVER["DOCUMENT_ROOT"]."/include/result_a_b.php");
                        require($_SERVER["DOCUMENT_ROOT"]."/include/result_a_u.php");
                $text = ob_get_contents();
                ob_clean();
                ob_end_clean();
        }
}else{
        if($_POST['NG'] == 9){                          //общая
                ob_start();
                        require($_SERVER["DOCUMENT_ROOT"]."/include/result_b_a.php");
                        require($_SERVER["DOCUMENT_ROOT"]."/include/result_b_u.php");
                $text = ob_get_contents();
                ob_clean();
                ob_end_clean();
                
        }elseif($_POST['NG'] == 10){                    //упрощенка
                ob_start();
                        require($_SERVER["DOCUMENT_ROOT"]."/include/result_b_b.php");
                        require($_SERVER["DOCUMENT_ROOT"]."/include/result_b_u.php");
                $text = ob_get_contents();
                ob_clean();
                ob_end_clean();
        }
        
}

if(!empty($text)){
        $result['text'] = $text;
}else{
        $result['text'] = 'Извините, произошла ошибка. Попробуйте повторить позже.';
}

echo json_encode($result);
?>