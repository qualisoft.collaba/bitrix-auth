<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if($USER->IsAuthorized())
{
    $rsfile = CFile::GetByID($_GET['ID']);
    $arfile = $rsfile->Fetch();
    $url = CFile::GetPath($_GET['ID']);
    
    $file=$_SERVER["DOCUMENT_ROOT"].$url;
    
    if (!empty($url)) {
            // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
            // если этого не сделать файл будет читаться в память полностью!
            if (ob_get_level()) {
              ob_end_clean();
            }
            // заставляем браузер показать окно сохранения файла
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . $arfile['ORIGINAL_NAME']);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . $arfile['FILE_SIZE']);
            // читаем файл и отправляем его пользователю
            readfile($_SERVER["DOCUMENT_ROOT"].$url);
            exit;
    }
}else
{
    LocalRedirect('/auth/');
}

?>