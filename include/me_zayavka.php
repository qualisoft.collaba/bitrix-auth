<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
header('Content-Type: application/json; charset=UTF-8');
global $USER;
CModule::IncludeModule("iblock");
$rsUser = CUser::GetByID($USER->GetID());
$arUser = $rsUser->Fetch();                                


$el = new CIBlockElement;

$arLoadProductArray = Array(
        "MODIFIED_BY"   => $USER->GetID(), 
        'DETAIL_TEXT'   => $arUser['ID'],
);
if($el->Update($_POST['id'],$arLoadProductArray)){
        $result['type'] = 'true';
        $result['text'] = $arUser['NAME'];
} else {
        $result['type'] = 'false';
        $result['text'] = $el->LAST_ERROR;
}
echo json_encode($result);
