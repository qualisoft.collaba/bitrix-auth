<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
header('Content-Type: application/json; charset=UTF-8');
$user = new CUser;

if((empty($_POST['PASSWORD']))||(empty($_POST['EMAIL']))||(empty($_POST['CONFIRM_PASSWORD']))){
        $result['type'] = 'false';
        $result['text'] = 'Поля не заполнены';
}else{

        if($_POST['PASSWORD']!=$_POST['CONFIRM_PASSWORD']){
                $result['type'] = 'false';
                $result['text'] = 'Пароли не совпадают';
        }else{

                $arFields = Array(
                        "NAME"                  => $_POST['NAME'],
                        "EMAIL"                 => $_POST['EMAIL'],
                        "LOGIN"                 => $_POST['EMAIL'],
                        "ACTIVE"                => "Y",
                        "PASSWORD"              => $_POST['PASSWORD'],
                        "GROUP_ID"              => array(3,4,$_POST['GROUP']),
                        "CONFIRM_PASSWORD"      => $_POST['CONFIRM_PASSWORD'],
                        'PERSONAL_MOBILE'       => $_POST['PERSONAL_MOBILE'],
                );
                $ID = $user->Add($arFields);
                if (intval($ID) > 0){
                        $result['type'] = 'true';
                }else{
                        $result['type'] = 'false';
                        $result['text'] = str_replace('Логин', 'E-Mail', $user->LAST_ERROR);
                }
        }
}
echo json_encode($result);