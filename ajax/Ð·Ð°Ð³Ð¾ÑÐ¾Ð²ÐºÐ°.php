<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Персональный раздел");
?>
<script>
function del(id) {
    var vc = id.replace(/\D/g,'');	
	vc = parseInt(vc);  
	$("#img"+vc).remove();
	$("#img"+vc).remove();
}



$(document).ready(function() { 	 
    $('#ggg').click(function() {
	       $('.file_button').trigger('click');	
	})   
	$("input.file_button").bind('change', function showFile(e, idid = $(this).attr("id")) {		
		var aaa = $(this).attr("id").replace(/\D/g,'');
		aaa = parseInt(aaa);
		bbb = aaa;
		aaa++;
				
		var old_id = idid;
		$(this).attr("id", "img"+aaa);
		$(this).attr("class", "file");
	    $(".div_inputs").append($(this).clone());
		$(this).attr("id", "file"+aaa);	
		$(this).attr("class", "file_button");
		var files = e.target.files;
		for (var i = 0, f; f = files[i]; i++) {
		  if (!f.type.match('image.*')) continue;
		  var fr = new FileReader();
		  fr.onload = (function(theFile) {
			return function(e) {			  			  
			    $("#list").append("<div onclick = 'del(\"img"+aaa+"\")' id='img"+aaa+"' class='col-xs-6 col-sm-4 padding-top-20'><div class='attached-photo active'><span class='ico throw-white'></span><img style='z-index: 10000'  class='img' src='" + e.target.result + "'><span class='caption'>Главная</span></div></div>")			
			};
		  })(f);
		  fr.readAsDataURL(f);
		}
	})
	
}); 	
</script>

<section class="gray-bg container-fluid min-height-700">
    <div class="container padding-bottom-70">
        <!--breadcrumbs-->
        <div class="row hidden-xs">
            <div class="col-xs-12">
                <ol class="breadcrumb th-breadcrumb">
                    <li><a href="#">Хостелы</a></li>
                    <li class="active">Добавить хостел</li>
                </ol>
            </div>
        </div><!--\end breadcrumbs-->

        <div class="row">
            <!--left sidebar-->
            <div class="col-md-3 padding-sm-top-20">
                <div class="lk-menu">
                    <ul>
                        <li><a href="reservation.html">Брони</a></li>
                        <li class="active"><a href="hostels.html">Хостелы</a></li>
                        <li><a href="numbers.html">Номера</a></li>
                        <li><a href="balance.html">Баланс</a></li>
                        <li><a href="settings.html">Настройки</a></li>
                    </ul>
                </div>
            </div>
            <!--\end left sidebar-->

            <!--content-->
            <div class="col-md-9">
                <div class="row padding-sm-top-20">
                    <div class="col-sm-6">
                        <h1 class="font-graublau-slab-semibold size-30 uppercase margin-top-0">Добавить хостел</h1>
                    </div>
                </div>

                <form name="gbook" id="foo" target="_self" method="post" enctype="multipart/form-data" action="<?=SITE_TEMPLATE_PATH?>/ajax/add_hostel.php">
                <!--add number-->
                <div class="white-box margin-top-20">
                    <div class="row padding-top-10 padding-bottom-10">
                        <div class="col-sm-3">Название хостела:</div>
                        <div class="col-sm-5">
                            <input type="text" name="name_hostel" class="form-control"  placeholder="">
                        </div>
                    </div>
                    <div class="row padding-top-10 padding-bottom-10">
                        <div class="col-sm-3">Адрес:</div>
                        <div class="col-sm-5">
						
						
						
						
                            <div class="row">
                                <div class="col-sm-2">
                                    <span class="size-12 line-height-34">Город:</span>
                                </div>
                                <div class="col-sm-10">
                                   
									
									
									  <?CModule::IncludeModule("iblock")?>
                        <select name="town" class="form-control transparent-bg" style="width: 100%">
                             <?$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>"6", "CODE"=>"TOWN"));
						while($enum_fields = $property_enums->GetNext())
						{					 
						  echo '<option value="'.$enum_fields["ID"].'">'.$enum_fields["VALUE"].'</option>';
						}
						?>
                        </select>
              
									
									
									    
									
									
                                </div>
                            </div>
				
							

							
		 
							
							
							
							
                            <div class="row padding-top-10 padding-bottom-10">
                                <div class="col-sm-2">
                                    <span class="size-12 line-height-34">Улица:</span>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" name="city" class="form-control"  placeholder="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2">
                                    <span class="size-12 line-height-34">Дом:</span>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" name="house" class="form-control"  placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-top-10 padding-bottom-10">
                        <div class="col-sm-3">Количество номеров:</div>
                        <div class="col-sm-1">
                            <input type="text" name="count_numbers" class="form-control"  placeholder="">
                        </div>
                    </div>
                    <div class="row padding-top-10 padding-bottom-10">
                        <div class="col-sm-3">Количество мест:</div>
                        <div class="col-sm-1">
                            <input type="text" name="count_places" class="form-control"  placeholder="">
                        </div>
                    </div>
                    <div class="row padding-top-10 padding-bottom-10">
                        <div class="col-sm-3">Регистрация заезда:</div>
                        <div class="col-sm-5">
                            <input type="text" name="reg_input" class="form-control"  placeholder="">
                        </div>
                    </div>
                    <div class="row padding-top-10 padding-bottom-10">
                        <div class="col-sm-3">Регистрация выезда:</div>
                        <div class="col-sm-5">
                            <input type="text" name="reg_output" class="form-control"  placeholder="">
                        </div>
                    </div>
                    <div class="row padding-top-10 padding-bottom-10">
                        <div class="col-sm-3">Питание в хостеле:</div>
                        <div class="col-sm-5">
                            <!--<select  class="form-control transparent-bg" id="mealsInHostel">
                                <option value="1">Питание по меню</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>-->
							
							  <select name="eating" class="form-control transparent-bg" style="width: 100%">
                             <?$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>"6", "CODE"=>"EATING"));
						while($enum_fields = $property_enums->GetNext())
						{					 
						  echo '<option value="'.$enum_fields["ID"].'">'.$enum_fields["VALUE"].'</option>';
						}
						?>
                        </select>
                        </div>
                    </div>
                    <div class="row padding-top-10 padding-bottom-10">
                        <div class="col-sm-3">Варианты оплаты:</div>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="checkbox">
                                        <label class="padding-bottom-5">
                                            <input type="checkbox"> <i class="visa ico margin-right-5"></i>  Visa
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label class="padding-bottom-5">
                                            <input type="checkbox"> <i class="master-card ico margin-right-5"></i> MasterCard
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label class="padding-bottom-5">
                                            <input type="checkbox">  <i class="maestro ico margin-right-5"></i> Maestro
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">

                                    <div class="checkbox">
                                        <label class="padding-bottom-5">
                                            <input type="checkbox"> <i class="notes ico margin-right-5"></i> Наличные
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label class="padding-bottom-5">
                                            <input type="checkbox"> <i class="card ico margin-right-5"></i> Безналичный рассчет
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-top-10 padding-bottom-10">
                        <div class="col-sm-3">О хостеле:</div>
                        <div class="col-sm-9">
                            <textarea  name="aboutHostel" class="form-control" rows="8"></textarea>
                        </div>
                    </div>

                    <div class="row padding-top-10 padding-bottom-10">
                        <div class="col-sm-3">Услуги:</div>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Парковка охраняемая
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Домашние животные разрешены
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Доступ в Интернет
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Отдельные номера для некурящих
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Бар
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Мини-кухня (СВЧ-печь) в отеле
                                        </label>
                                    </div>
                                    <button class="btn-link-blue padding-left-0">+ Добавить услугу</button>
                                </div>
                                <div class="col-sm-6">

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Парковка охраняемая
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Домашние животные разрешены
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Доступ в Интернет
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Отдельные номера для некурящих
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Бар
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Мини-кухня (СВЧ-печь) в отеле
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row padding-top-10 padding-bottom-10">
                        <div class="col-sm-3">Инфраструктура:</div>
                        <div class="col-sm-9">
                            <textarea  name="infras" class="form-control" rows="8"></textarea>
                        </div>
                    </div>

                    <!--<div class="row padding-top-10 padding-bottom-10">
                        <div class="col-sm-12">
                            <div class="map-box">
                                <img class="img-responsive" src="../assets/img/lk-map-preview.png" alt="">
                                <a class="uppercase place text-black" href="">Отметить на карте</a>
                            </div>
                        </div>
                    </div>-->

					

                    <div class="row padding-top-10 padding-bottom-10">
                        <div class="col-sm-3">Фотографии:</div>
                        <div class="col-sm-5">
                            <!--<button type="submit" class="light-gray-btn uppercase full-width">Выбрать файл</button>-->
							<a href="#" id="ggg" class="light-gray-btn uppercase full-width" style="text-align: center;">Выбрать файл</a>
                        </div>
                        <div class="col-xs-12 col-sm-offset-3 col-sm-9">
                            <div class="row" id="list">

                            </div>
                        </div>
                    </div>

					<div class="div_inputs" id="div_inputs" style = "display: none">
						
						<input type="file" id="file1" class="file_button" name='files[]'>	
					
					</div>
					

                    <div class="row padding-top-10 padding-bottom-10">
                        <div class="col-sm-offset-3 col-sm-9">
                            <div class="row">
                                <div class="col-sm-6"><button type="submit" class="add_hostel_button blue-btn uppercase full-width">Сохранить</button></div>
                                <div class="col-sm-6 padding-xs-top-10"><button type="submit" class="blue-btn-transparent uppercase full-width">Отмена</button></div>
								
                            </div>
                        </div>
                    </div>
                </div> <!--add number-->

            </div>
            <!--\end content-->
        </div>
    </div>
</section>

<!---->

<!--footer-->
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>