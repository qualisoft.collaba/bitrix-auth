<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Личный кабинет");

CModule::IncludeModule("iblock");
?>
        
                <div class="wrapper">
                        
                       <!-- <div class='tabs_wrap'>
                               <a href='#' class='tab1 active' rel='tab1'>Настройки Финансовая информация</a>
                               <a href='#' class='tab2' rel='tab2'>Настройки</a>
                        </div>-->
                        
                        <div id='tab1' class="tab">
                                <!--p>Указать период для расчетов вознаграждения</p>
                                <div class="element_anketa">
                                        <input type="text" placeholder='дата с' class="date_time datepicker"> <input type="text" placeholder='дата по' class="date_time datepicker"> <button>Обновить</button>
                                </div>
                                <?  global $arGroups, $USER;?>
                                <?if(!in_array(6, $arGroups)){?>
                                        <div class="grafic">
                                                <?  
                                                include($_SERVER["DOCUMENT_ROOT"]."/include/pChart/pDraw.class.php");
                                                include($_SERVER["DOCUMENT_ROOT"]."/include/pChart/pImage.class.php");
                                                include($_SERVER["DOCUMENT_ROOT"]."/include/pChart/pData.class.php");                                        
                                                ?>
                                                <?
                                                $myData = new pData();

                                                $myData->addPoints(1248569,"Total");
                                                $myData->addPoints("15.03.2015","Labels");
                                                $myData->addPoints(2458569,"Total");
                                                $myData->addPoints("17.04.2015","Labels");
                                                $myData->addPoints(500254,"Total");
                                                $myData->addPoints("24.05.2015","Labels");
                                                $myData->addPoints(2548569,"Total");
                                                $myData->addPoints("20.06.2015","Labels");

                                                //Тут должно быть соединение с базой, выполнение запроса.
                                                /*while (list($dt,$sum) = mysql_fetch_row($result)) {
                                                    $myData->addPoints($sum,"Total");
                                                    $myData->addPoints($dt,"Labels");
                                                };*/

                                                $unique = date("Y.m.d_H.i");
                                                $gsFilename_Traffic = "traffic.png";
                                                unlink($gsFilename_Traffic);
                                                $myData->setSerieDescription("Labels","Days");
                                                $myData->setAbscissa("Labels");
                                                $myData->setAxisUnit(1000," руб");
                                                //$myData->setAxisUnit(0," руб");

                                                $serieSettings = array("R"=>229,"G"=>11,"B"=>11,"Alpha"=>100);
                                                $myData->setPalette("Total",$serieSettings);

                                                $myPicture = new pImage(400,200,$myData); // <-- Размер холста
                                                $myPicture->setFontProperties(array("FontName"=>"tahoma.ttf","FontSize"=>8));
                                                $myPicture->setGraphArea(50,20,390,180); // <-- Размещение графика на холсте
                                                $myPicture->drawScale();
                                                $myPicture->drawBestFit(array("Alpha"=>0)); // <-- Прямая статистики

                                                $myPicture->drawLineChart();
                                                $myPicture->drawPlotChart(array("DisplayValues"=>FALSE,"PlotBorder"=>TRUE,"BorderSize"=>0,"Surrounding"=>-60,"BorderAlpha"=>50)); // <-- Точки на графике
                                                //$myPicture->drawLegend(100,10,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_HORIZONTAL));// <-- Размещение легенды
                                                $myPicture->Render($gsFilename_Traffic);

                                                ?>
                                                <IMG SRC="<?=$gsFilename_Traffic;?>"/>                                        
                                        </div>
                                <?}?>
                                
                                <table class='width_60'>
                                        <tr>
                                                <th>Дата заявки</th>
                                                <th>Дата выплаты</th>
                                                <th>Сумма вознаграждения</th>
                                                <th>Статус выплат</th>
                                        </tr>
                                        <tr>
                                                <td>15.03.2015</td>
                                                <td>30.03.2015</td>
                                                <td>1 248 569</td>
                                                <td><a href='#table1' class="lk_table">Подробнее</a></td>
                                        </tr>
                                        <tr>
                                                <td>17.04.2015</td>
                                                <td>30.04.2015</td>
                                                <td>2 458 569</td>
                                                <td><a href='#table2' class="lk_table">Подробнее</a></td>
                                        </tr>
                                        <tr>
                                                <td>24.05.2015</td>
                                                <td>30.05.2015</td>
                                                <td>500 254</td>
                                                <td><a href='#table3' class="lk_table">Подробнее</a></td>
                                        </tr>
                                        <tr>
                                                <td>20.06.2015</td>
                                                <td>30.05.2015 </td>
                                                <td>2 548 569</td>
                                                <td><a href='#table3' class="lk_table">Подробнее</a></td>
                                        </tr>
                                        <tr>
                                                <td></td>
                                                <td>Итого</td>
                                                <td>1 256 256</td>
                                                <td></td>
                                        </tr>
                                </table>
                                <?/*
                                global $arrFilter, $USER;
                                $arrFilter['MODIFIED_BY '] = $USER->GetID();
                                ?>
                                <?$APPLICATION->IncludeComponent(
                                        "bitrix:news.list",
                                        'lk2',
                                        Array(
                                                "IBLOCK_TYPE" => "zayavki",
                                                "IBLOCK_ID" => $_REQUEST["ID"],
                                                "NEWS_COUNT" => $_GET['COUNT'],
                                                "SORT_BY1" => "ACTIVE_FROM",
                                                "SORT_ORDER1" => "DESC",
                                                "SORT_BY2" => "SORT",
                                                "SORT_ORDER2" => "ASC",
                                                "FILTER_NAME" => "arrFilter",
                                                "FIELD_CODE" => array("","undefined",""),
                                                "PROPERTY_CODE" => array("","undefined",""),
                                                "CHECK_DATES" => "Y",
                                                "DETAIL_URL" => "",
                                                "AJAX_MODE" => "N",
                                                "AJAX_OPTION_JUMP" => "N",
                                                "AJAX_OPTION_STYLE" => "Y",
                                                "AJAX_OPTION_HISTORY" => "N",
                                                "CACHE_TYPE" => "N",
                                                "CACHE_TIME" => "36000000",
                                                "CACHE_FILTER" => "N",
                                                "CACHE_GROUPS" => "Y",
                                                "PREVIEW_TRUNCATE_LEN" => "",
                                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                                "SET_TITLE" => "N",
                                                "SET_BROWSER_TITLE" => "N",
                                                "SET_META_KEYWORDS" => "N",
                                                "SET_META_DESCRIPTION" => "N",
                                                "SET_STATUS_404" => "N",
                                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                                "ADD_SECTIONS_CHAIN" => "N",
                                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                                "PARENT_SECTION" => "",
                                                "PARENT_SECTION_CODE" => "",
                                                "INCLUDE_SUBSECTIONS" => "N",
                                                "DISPLAY_DATE" => "N",
                                                "DISPLAY_NAME" => "N",
                                                "DISPLAY_PICTURE" => "N",
                                                "DISPLAY_PREVIEW_TEXT" => "N",
                                                "PAGER_TEMPLATE" => ".default",
                                                "DISPLAY_TOP_PAGER" => "N",
                                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                                "PAGER_TITLE" => "",
                                                "PAGER_SHOW_ALWAYS" => "N",
                                                "PAGER_DESC_NUMBERING" => "N",
                                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                                "PAGER_SHOW_ALL" => "N"
                                        )
                                );*/?>                                
                                <table class='width_100 hide' id="table1">
                                        <tr>
                                                <th>Код БГ</th>
                                                <th>Дата выдачи БГ</th>
                                                <th>Сумма БГ, руб</th>
                                                <th>Стоимость БГ, руб</th>
                                                <th>Вознаграждение агента, руб</th>
                                        </tr>
                                        <tr>
                                                <td>2563</td>
                                                <td>15.08.2015</td>
                                                <td>1 256 256</td>
                                                <td>256 256</td>
                                                <td>6 256</td>
                                        </tr>
                                        <tr>
                                                <td>2564</td>
                                                <td>15.08.2015</td>
                                                <td>1 256 256</td>
                                                <td>256 256</td>
                                                <td>6 256</td>
                                        </tr>
                                        <tr>
                                                <td>2564</td>
                                                <td>15.08.2015</td>
                                                <td>1 256 256</td>
                                                <td>256 256</td>
                                                <td>6 256</td>
                                        </tr>
                                        <tr>
                                                <td></td>
                                                <td>Итого</td>
                                                <td>1 256 256</td>
                                                <td>256 256</td>                                                
                                                <td>56 256</td>
                                        </tr>
                                </table>
                                <table class='width_100 hide' id="table2">
                                        <tr>
                                                <th>Код БГ</th>
                                                <th>Дата выдачи БГ</th>
                                                <th>Сумма БГ, руб</th>
                                                <th>Стоимость БГ, руб</th>
                                                <th>Вознаграждение агента, руб</th>
                                        </tr>
                                        <tr>
                                                <td>2565</td>
                                                <td>15.08.2015</td>
                                                <td>1 256 256</td>
                                                <td>256 256</td>
                                                <td>6 256</td>
                                        </tr>
                                        <tr>
                                                <td>2564</td>
                                                <td>15.08.2015</td>
                                                <td>1 256 256</td>
                                                <td>256 256</td>
                                                <td>6 256</td>
                                        </tr>
                                        <tr>
                                                <td>2564</td>
                                                <td>15.08.2015</td>
                                                <td>1 256 256</td>
                                                <td>256 256</td>
                                                <td>6 256</td>
                                        </tr>
                                        <tr>
                                                <td></td>
                                                <td>Итого</td>
                                                <td>1 256 256</td>
                                                <td>256 256</td>                                                
                                                <td>56 256</td>
                                        </tr>
                                </table>
                                <table class='width_100 hide' id="table3">
                                        <tr>
                                                <th>Код БГ</th>
                                                <th>Дата выдачи БГ</th>
                                                <th>Сумма БГ, руб</th>
                                                <th>Стоимость БГ, руб</th>
                                                <th>Вознаграждение агента, руб</th>
                                        </tr>
                                        <tr>
                                                <td>256</td>
                                                <td>15.08.2015</td>
                                                <td>1 256 256</td>
                                                <td>256 256</td>
                                                <td>6 256</td>
                                        </tr>
                                        <tr>
                                                <td>2564</td>
                                                <td>15.08.2015</td>
                                                <td>1 256 256</td>
                                                <td>256 256</td>
                                                <td>6 256</td>
                                        </tr>
                                        <tr>
                                                <td>2564</td>
                                                <td>15.08.2015</td>
                                                <td>1 256 256</td>
                                                <td>256 256</td>
                                                <td>6 256</td>
                                        </tr>
                                        <tr>
                                                <td></td>
                                                <td>Итого</td>
                                                <td>1 256 256</td>
                                                <td>256 256</td>                                                
                                                <td>56 256</td>
                                        </tr>
                                </table>
                                
                        </div>
                        
                        <div id="tab2" class="tab hide"-->
                                <?
                                $rsUser = CUser::GetByID($USER->GetID());
                                $arUser = $rsUser->Fetch();
$arGroupAvalaible = array(1,5,6); // массив групп, которые в которых нужно проверить доступность пользователя
$arGroups = CUser::GetUserGroup($USER->GetID()); // массив групп, в которых состоит пользователь
$result_intersect = array_intersect($arGroupAvalaible, $arGroups);// далее проверяем, если пользователь вошёл хотя бы в одну из групп, то позволяем ему что-либо делать
                                ?>
                                <form class="on_ajax profil" action="/include/edit_user_profil.php" method="POST">
                                        <div class="element_anketa">
                                                <p>Регистрационные данные</p>
                                        </div>
                                        
                                        <div class="element_anketa">                                        
                                                <input type="text" readonly="readonly" placeholder=<?if(!empty($result_intersect)):?>'ФИО'<?else:?>'Наименование агента'<?endif?> name='NAME' value="<?=$arUser['NAME']?>" required>
                                        </div>
                                        <div class="element_anketa">                                        
                                                <input type="text" readonly="readonly" placeholder=<?if(!empty($result_intersect)):?>'ФИО'<?else:?>'ФИО'<?endif?> name='NAME' value="<?=$arUser['LAST_NAME']?>" required>
                                        </div>
                                        
                                        <div class="element_anketa">                                        
                                                <input id="phone" type="text" placeholder='Телефон' name='PERSONAL_MOBILE' value="<?=$arUser['PERSONAL_MOBILE']?>" required>
                                        </div>
                                        <div class="element_anketa">                                        
                                                <input type="email" placeholder='Email' name='EMAIL' value="<?=$arUser['EMAIL']?>" required>
                                        </div>
                                        <div class="element_anketa">                                        
                                                <input type="password" placeholder='Пароль' name='PASSWORD'>
                                        </div>
                                        <div class="element_anketa">                                        
                                                <input type="password" placeholder='Повтор пароля' name='CONFIRM_PASSWORD'>
                                        </div>
                                        
                                         <?/*if(!in_array(6, $arGroups)){?>
                                                <div class="element_anketa">
                                                        <p>Платежные реквизиты</p>
                                                </div>
                                                <div class="element_anketa">                                        
                                                        <p>Размер вознаграждения 4%</p>
                                                </div>
                                                <div class="element_anketa">                                        
                                                        <textarea cols="100" rows="10" placeholder='Платежные реквизиты' name='PERSONAL_NOTES'><?=$arUser['PERSONAL_NOTES']?></textarea>
                                                </div>
                                        <?}*/?>
                                        <div class="element_anketa">
                                                <div class="error-text"></div>
                                        </div>
                                        <div class="element_anketa">
                                                <div class="message-type-ok"></div>
                                        </div>
                                        <div class="element_anketa">                                        
                                                <button>Сохранить</button>
                                        </div>
                                </form>
                        </div>

                        <!--
                        <div class='pagination'>
                                <a href='#'>1</a>
                                <a href='#'><</a> 
                                <a href='#' class='active'>11</a>
                                <a href='#'>12</a>
                                <a href='#'>13</a> 
                                <a href='#'>></a> 
                                <a href='#'>54</a> 
                        </div>
                        -->
                </div>        
        <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>