<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Форма добавления фин. отчета");
global $arrFilter, $USER;?>

<script type="text/javascript">

$(document).ready(function(){
   $( "input" ).change(function() {
     var nemat = $("tr.nemat input");
	 var res = $("tr.res input");
	 var nem = $("tr.nem input");
	 var mat = $("tr.mat input");
	 var osn = $("tr.osn input");
	 var doh = $("tr.doh input");
	 var fin = $("tr.fin input");
	 var otl = $("tr.otl input");
	 var proch  = $("tr.proch  input");
	 var itog  = $("tr.itog  input");
	 
	 

		var i=0;			
		$('tr.itog  input').each(function(){
		
		   nemat_ = parseInt(nemat[i].value)
		     if (isNaN(nemat_)==true) nemat_=0
		   res_ = parseInt(res[i].value)
		     if (isNaN(res_)==true) res_=0
		   nem_ = parseInt(nem[i].value)
		     if (isNaN(nem_)==true) nem_=0
			 
		   mat_ = parseInt(mat[i].value)
		     if (isNaN(mat_)==true) mat_=0
           osn_ = parseInt(osn[i].value)
		     if (isNaN(osn_)==true) osn_=0
           doh_ = parseInt(doh[i].value)
		     if (isNaN(doh_)==true) doh_=0	

           fin_ = parseInt(fin[i].value)
		     if (isNaN(fin_)==true) fin_=0
           otl_ = parseInt(otl[i].value)
		     if (isNaN(otl_)==true) otl_=0
           proch_ = parseInt(proch[i].value)
		     if (isNaN(proch_)==true) proch_=0				 
			 
           		
		   $(this).attr('value', nemat_ + res_ + nem_ + mat_ + osn_ + doh_ + fin_ + otl_ + proch_);
		   i++;
		});
 
 
 
 
    var zap = $("tr.zap input");
	var nalog = $("tr.nalog input");
	var deb = $("tr.deb input");
	var vtch = $("tr.vtch input");
	var finvl = $("tr.finvl input");
	var deneg = $("tr.deneg input");
	var proch = $("tr.proch input");

    var itog = $("tr.itog input");
    var itog2 = $("tr.itog2 input");
	var itog3 = $("tr.itog3 input");
    var itog4 = $("tr.itog4 input");
	var itog5 = $("tr.itog5 input");

  
    var i=0;			
		$('tr.itog2  input').each(function(){
		
		   zap_ = parseInt(zap[i].value)
		     if (isNaN(zap_)==true) zap_=0
		   nalog_ = parseInt(nalog[i].value)
		     if (isNaN(nalog_)==true) nalog_=0
		   deb_ = parseInt(deb[i].value)
		     if (isNaN(deb_)==true) deb_=0
			 
		   vtch_ = parseInt(vtch[i].value)
		     if (isNaN(vtch_)==true) vtch_=0
           finvl_ = parseInt(finvl[i].value)
		     if (isNaN(finvl_)==true) finvl_=0
           deneg_ = parseInt(deneg[i].value)
		     if (isNaN(deneg_)==true) deneg_=0	
           proch_ = parseInt(proch[i].value)
		     if (isNaN(proch_)==true) proch_=0         		 			        		
		   $(this).attr('value', zap_ + nalog_ + deb_ + vtch_ + finvl_ + deneg_ + proch_);
		   i++;
		});

		    var j=0;			
		$('tr.bal  input').each(function(){
		
		   itog_ = parseInt(itog[j].value)
		     if (isNaN(itog_)==true) itog_=0
		   itog2_ = parseInt(itog2[j].value)
		     if (isNaN(itog2_)==true) itog2_=0        		 			        		
		   $(this).attr('value', itog_ + itog2_);
		   j++;
		});

    //3 раздел
    var ustkap = $("tr.ustkap input");
	var sobakz = $("tr.sobakz input");
	var pervne = $("tr.pervne input");
	var dobkap = $("tr.dobkap input");
	var rezkap = $("tr.rezkap input");
	var nerpri = $("tr.nerpri input");
	
    var i=0;			
		$('tr.itog3  input').each(function(){
		
		   ustkap_ = parseInt(ustkap[i].value)
		     if (isNaN(ustkap_)==true) ustkap_=0
		   sobakz_ = parseInt(sobakz[i].value)
		     if (isNaN(sobakz_)==true) sobakz_=0
		   pervne_ = parseInt(pervne[i].value)
		     if (isNaN(pervne_)==true) pervne_=0
			 
		   dobkap_ = parseInt(dobkap[i].value)
		     if (isNaN(dobkap_)==true) dobkap_=0
           rezkap_ = parseInt(rezkap[i].value)
		     if (isNaN(rezkap_)==true) rezkap_=0
           nerpri_ = parseInt(nerpri[i].value)
		     if (isNaN(nerpri_)==true) nerpri_=0	  
			 
		   $(this).attr('value', ustkap_ + sobakz_ + pervne_ + dobkap_ + rezkap_ + nerpri_);
		   i++;
		});
    //4 раздел
    var zaesred = $("tr.zaesred input");
	var otlnal4 = $("tr.otlnal4 input");
	var ocenoby4 = $("tr.ocenoby4 input");
	var prooby4 = $("tr.prooby4 input");
	
    var i=0;			
		$('tr.itog4  input').each(function(){
		
		   zaesred_ = parseInt(zaesred[i].value)
		     if (isNaN(zaesred_)==true) zaesred_=0
		   otlnal4_ = parseInt(otlnal4[i].value)
		     if (isNaN(otlnal4_)==true) otlnal4_=0
		   ocenoby4_ = parseInt(ocenoby4[i].value)
		     if (isNaN(ocenoby4_)==true) ocenoby4_=0		 
		   prooby4_ = parseInt(prooby4[i].value)
		     if (isNaN(prooby4_)==true) prooby4_=0
        
     	  
			 
		   $(this).attr('value', zaesred_ + otlnal4_ + ocenoby4_ + prooby4_);
		   i++;
		});
		
		 //5 раздел
    var zaesred5 = $("tr.zaesred5 input");
	var kredzad = $("tr.kredzad input");
	var vtchpros5 = $("tr.vtchpros5 input");
	var dohbud = $("tr.dohbud input");
	var oceoby = $("tr.oceoby input");
	var prochoby5 = $("tr.prochoby5 input");
	
    var i=0;			
		$('tr.itog5  input').each(function(){
		
		   zaesred5_ = parseInt(zaesred5[i].value)
		     if (isNaN(zaesred5_)==true) zaesred5_=0
		   kredzad_ = parseInt(kredzad[i].value)
		     if (isNaN(kredzad_)==true) kredzad_=0
		   vtchpros5_ = parseInt(vtchpros5[i].value)
		     if (isNaN(vtchpros5_)==true) vtchpros5_=0		 
		   dohbud_ = parseInt(dohbud[i].value)
		     if (isNaN(dohbud_)==true) dohbud_=0
			 oceoby_ = parseInt(oceoby[i].value)
		     if (isNaN(oceoby_)==true) oceoby_=0
			  prochoby5_ = parseInt(prochoby5[i].value)
		     if (isNaN(prochoby5_)==true) prochoby5_=0
        
     	  
			 
		   $(this).attr('value', zaesred5_ + kredzad_ + vtchpros5_ + dohbud_ + oceoby_ + prochoby5_ );
		   i++;
		});
	
	
		    var k=0;			
		$('tr.bal2  input').each(function(){
		
		   itog3_ = parseInt(itog3[k].value)
		     if (isNaN(itog3_)==true) itog3_=0
		   itog4_ = parseInt(itog4[k].value)
		     if (isNaN(itog4_)==true) itog4_=0  
           itog5_ = parseInt(itog5[k].value)
		     if (isNaN(itog5_)==true) itog5_=0			 
		   $(this).attr('value', itog3_ + itog4_ + itog5_);
		   k++;
		});
		
		
		
		//61 раздел
		var vyr6 = $("tr.vyr6 input");
       	var sebprod6 = $("tr.sebprod6 input");
	
	
    var i=0;			
		$('tr.itog61  input').each(function(){
		
		   vyr6_ = parseInt(vyr6[i].value)
		     if (isNaN(vyr6_)==true) vyr6_=0
		   sebprod6_ = parseInt(sebprod6[i].value)
		     if (isNaN(sebprod6_)==true) sebprod6_=0
		  
     	  
			 
		   $(this).attr('value', vyr6_ + sebprod6_);
		   i++;
		});
	   //62 раздел
	
	    var komras6 = $("tr.komras6 input");
       	var uprras6 = $("tr.uprras6 input");
	
	
    var i=0;			
		$('tr.itog62  input').each(function(){
		
		   komras6_ = parseInt(komras6[i].value)
		     if (isNaN(komras6_)==true) komras6_=0
		   uprras6_ = parseInt(uprras6[i].value)
		     if (isNaN(uprras6_)==true) uprras6_=0
		  
     	  
			 
		   $(this).attr('value', komras6_ + uprras6_);
		   i++;
		});
	
		   //63 раздел
	
	    var dohot6 = $("tr.dohot6 input");
       	var prockpol6 = $("tr.prockpol6 input");
	    var prockupl6 = $("tr.prockupl6 input");
		var prochdoh6 = $("tr.prochdoh6 input");
		var prochras6 = $("tr.prochras6 input");
	
    var i=0;			
		$('tr.itog63  input').each(function(){
		
		   dohot6_ = parseInt(dohot6[i].value)
		     if (isNaN(dohot6_)==true) dohot6_=0
		   prockpol6_ = parseInt(prockpol6[i].value)
		     if (isNaN(prockpol6_)==true) prockpol6_=0
			 
			  prockupl6_ = parseInt(prockupl6[i].value)
		     if (isNaN(prockupl6_)==true) prockupl6_=0
			  prochdoh6_ = parseInt(prochdoh6[i].value)
		     if (isNaN(prochdoh6_)==true) prochdoh6_=0
			  prochras6_ = parseInt(prochras6[i].value)
		     if (isNaN(prochras6_)==true) prochras6_=0
		  
     	  
			 
		   $(this).attr('value', dohot6_ + prockpol6_ + prockupl6_ + prochdoh6_ + prochras6_);
		   i++;
		});
		
		//64 раздел
	
	    var teknal6 = $("tr.teknal6 input");
       	var izmotobl6 = $("tr.izmotobl6 input");
	    var izmotlart6 = $("tr.izmotlart6 input");
		var prochee6 = $("tr.prochee6 input");
		
	
    var i=0;			
		$('tr.itog64  input').each(function(){
		
		   teknal6_ = parseInt(teknal6[i].value)
		     if (isNaN(teknal6_)==true) teknal6_=0
		  izmotobl6_ = parseInt(izmotobl6[i].value)
		     if (isNaN(izmotobl6_)==true) izmotobl6_=0
			 
			  izmotlart6_ = parseInt(izmotlart6[i].value)
		     if (isNaN(izmotlart6_)==true) izmotlart6_=0
			  prochee6_ = parseInt(prochee6[i].value)
		     if (isNaN(prochee6_)==true) prochee6_=0
			 
		  
     	  
			 
		   $(this).attr('value', teknal6_ + izmotobl6_ + izmotlart6_ + prochee6_);
		   i++;
		});
	
	
	});
	
	
	
	

	//alert(document.cookie);
 
	function readCookie(cookieName) {
        var re = new RegExp('[; ]'+cookieName+'=([^\\s;]*)');
        var sMatch = (' '+document.cookie).match(re);
        if (cookieName && sMatch) return unescape(sMatch[1]);
    }
	$(".save").click(function() {
	function readCookie(cookieName) {
        var re = new RegExp('[; ]'+cookieName+'=([^\\s;]*)');
        var sMatch = (' '+document.cookie).match(re);
        if (cookieName && sMatch) return unescape(sMatch[1]);
    }
			function setCookie(name,value,days) {
				if (days) {
					var date = new Date();
					date.setTime(date.getTime() + (days*24*60*60*1000));
					var expires = "; expires=" + date.toGMTString();
				}
				else var expires = "";
				document.cookie = name + "=" + escape(value) + expires + "; path=/";
			}
			//var table;
			$('input').each(function(){
			  
			   //input = parseInt($(this).val());
			   //if (isNaN(input)==true) input=0;
			   table = table+"_"+$(this).val();
			   
			   
			});
			var inn = readCookie("inn")
			setCookie("t"+"_"+inn, table, 10);
            //document.location.href = "http://pbg.nodomain.me/zayavki/dobavlenie-zayavki/";
            $('#hide-layout, #popup').fadeOut(300); // плавно скрываем	
            //$('#hide-layout, #popup').css("display", "none");			
	}); 
	
   
	var inn = readCookie("inn")
    var arr = readCookie("t"+"_"+inn).split('_');
	var t = 1; 
	$('input').each(function(){	         
		      $(this).attr('value', arr[t]);
           t++;		   
	});	
	
})

</script>
<link rel="stylesheet" type="text/css" href="/local/bootstrap/file-input.css">  
<link rel="stylesheet" type="text/css" href="/local/bootstrap/css.css">
<link rel="stylesheet" type="text/css" href="/local/bootstrap/baseStyle.css">

<link rel="stylesheet" type="text/css" href="/local/bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/local/bootstrap/develop.css">

<script language="JavaScript" type="text/javascript" src="/local/bootstrap/js/bootstrap.min.js"></script>
<script language="JavaScript" type="text/javascript" src="/local/bootstrap/jqueryval.js"></script>
<script language="JavaScript" type="text/javascript" src="/local/bootstrap/file-input.js"></script>
<script language="JavaScript" type="text/javascript" src="/local/bootstrap/ko.js"></script>
<script language="JavaScript" type="text/javascript" src="/local/bootstrap/jquery.form.js"></script>
<script language="JavaScript" type="text/javascript" src="/local/bootstrap/notify.js"></script>
<div class="clear-fix" style="padding:50px">
<style type="text/css">
        .tab-first {
            width: 100%;
        }
		input, .uneditable-input {
		  width: 206px;
		  max-width: 120px;
		}
		.currency{
			margin:0!important;
		}
    </style>
<div class="tab-panel">
    <div class="tab tab-quarter tab-first active" content="tab-content-first" style="text-align: left"><div class="swap">&nbsp;</div>
        <i class="icon-th"></i>Финансовая отчетность
    </div>
</div>
<div id="tab-content-first" style="display: block;">

    <div class="panel panel-default">
        <div id="context">

<form action="" data-ajax="true" data-ajax-method="POST" data-ajax-success="success" id="form0" method="post" novalidate="novalidate"><input id="hid" name="hid" value="" type="hidden">
    <div id="deleteDraftDialog" class="modal">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3>Удаление черновика</h3>
        </div>
        <div class="modal-body">
            <div class="control-group">
                <label>Удалить черновик отчетности?</label>
            </div>
        </div>
        <div class="modal-footer">
            <a class="btn btn-default btn-sm" href="/FinancialStatements/DeleteDraft?userId=39826">Удалить</a>
            <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Отмена</button>
        </div>
    </div>
    <div style="margin: 10px;" class=" text-right">
        <!--<button class="btn btn-default btn-sm" data-bind="click: cancelEdit">Отменить</button>
        <button class="btn btn-primary btn-sm" data-bind="click: save">Сохранить</button>-->
		<a class="btn btn-default btn-sm" href="/zayavki/finothet.php">Отменить</a>
        <a class="btn btn-primary btn-sm save" href="#">Сохранить</a>
    </div>
                <div data-bind="visible: isVisible" style="display: none;">
                    <button type="button" class="btn btn-success btn-sm" data-bind="click: ReportToExcel">Выгрузить в XLSX</button>
                </div>
                <div class="divTableWithFloatingHeader table-responsive" style="position:relative;">
				<table class="table table-condensed" id="table" data-bind="with:models">
                    <thead>
                    <tr class="tableFloatingHeader" style="position: fixed; top: -2px; margin-left: 301px; margin-top: 55px; background-color: rgb(255, 255, 255); visibility: hidden; left: 0px; width: 1261px;">
                        <th style="font-size: 16px; width: 518px;">Значения измеряются в тысячах</th>
                        <th style="width: 47px;">Код</th>
                        <!-- ko foreach: Headers-->
                        <th class="input-th" data-bind="text: $data" style="width: 139px;">Год (2015)</th>
                        
                        <th class="input-th" data-bind="text: $data" style="width: 139px;">Квартал 1 (2016)</th>
                        
                        <th class="input-th" data-bind="text: $data" style="width: 139px;">Квартал 2 (2016)</th>
                        
                        <th class="input-th" data-bind="text: $data" style="width: 139px;">Квартал 3 (2016)</th>
                        
                        <th class="input-th" data-bind="text: $data" style="width: 139px;">Год (2016)</th>
                        <!-- /ko -->
                    </tr><tr class="tableFloatingHeaderOriginal">
                        <th style="font-size: 16px">Значения измеряются в тысячах </th>
                        <th>Код</th>
                        <!-- ko foreach: Headers-->
                        <th class="input-th" data-bind="text: $data">Год (2015)</th>
                        
                        <th class="input-th" data-bind="text: $data">Квартал 1 (2016)</th>
                        
                        <th class="input-th" data-bind="text: $data">Квартал 2 (2016)</th>
                        
                        <th class="input-th" data-bind="text: $data">Квартал 3 (2016)</th>
                        
                        <th class="input-th" data-bind="text: $data">Год (2016)</th>
                        <!-- /ko -->
                    </tr>
                    </thead>
                    <tbody data-bind="foreach: ItemsByCategory">
					<tr>
                       
						<td colspan="7" style="text-align: center">
                            <h4 data-bind="text: Title">АКТИВ БАЛАНСА</h4>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" style="text-align: center">
                            <h5 data-bind="text: Title">1. ВНЕОБОРОТНЫЕ АКТИВЫ</h5>
                        </td>
                    </tr>
                    <!-- ko foreach: Items-->
                    <tr class="nemat">
                        <td style="padding-top: 13px;" data-bind="text: Title">Нематериальные активы</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1110</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input   class="currency valid vneob_1" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_1" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency vneob_2" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_2" type="text">
                        </td>
                        
                        <td>
                            <input class="currency vneob_3" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_3" type="text">
                        </td>
                        
                        <td>
                            <input class="currency vneob_4" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_4" type="text">
                        </td>
                        
                        <td>
                            <input class="currency vneob_5" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_5" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="res">
                        <td style="padding-top: 13px;" data-bind="text: Title">Результаты исследований и разработок</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1120</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_6" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_7" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_8" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_9" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_10" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="nem">
                        <td style="padding-top: 13px;" data-bind="text: Title">Нематериальные поисковые активы</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1130</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_11" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_12" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_13" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_14" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_15" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="mat">
                        <td style="padding-top: 13px;" data-bind="text: Title">Материальные поисковые активы</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1140</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_16" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_17" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_18" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_19" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_20" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="osn">
                        <td style="padding-top: 13px;" data-bind="text: Title">Основные средства</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1150</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_21" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_22" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_23" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_24" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_25" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="doh">
                        <td style="padding-top: 13px;" data-bind="text: Title">Доходные вложения в материальные ценности</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1160</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_26" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_27" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_28" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_29" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_30" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="fin">
                        <td style="padding-top: 13px;" data-bind="text: Title">Финансовые вложения</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1170</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_31" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_32" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_33" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_34" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_35" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="otl">
                        <td style="padding-top: 13px;" data-bind="text: Title">Отложенные налоговые активы</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1180</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_36" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_37" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_38" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_39" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_40" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="proch">
                        <td style="padding-top: 13px;" data-bind="text: Title">Прочие внеоборотные активы</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1190</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_41" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_42" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_43" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_44" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_45" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="itog">
                        <td style="padding-top: 13px;" data-bind="text: Title">ИТОГО по разделу I</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1100</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_46" readonly="readonly" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_47" readonly="readonly" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_48" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_49" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_50" readonly="readonly" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    <!-- /ko -->
                    
                    <tr>
                        
						<td colspan="7" style="text-align: center">
                            <h5 data-bind="text: Title">2. ОБОРОТНЫЕ АКТИВЫ</h5>
                        </td>
                    </tr>
                    <!-- ko foreach: Items-->
                    <tr class="zap">
                        <td style="padding-top: 13px;" data-bind="text: Title">Запасы</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1210</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_51" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_52" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_53" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_54" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_55" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="nalog">
                        <td style="padding-top: 13px;" data-bind="text: Title">Налог на добавленную стоимость по приобретённым ценностям</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1220</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_56" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_57" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_58" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_59" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_60" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr  class="deb">
                        <td style="padding-top: 13px;" data-bind="text: Title">Дебиторская задолженность</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1230</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_61" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_62" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_63" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_64" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_65" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="vtch">
                        <td style="padding-top: 13px;" data-bind="text: Title">в т.ч. просроченная</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1231</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_66" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_67" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_68" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_69" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_70" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr  class="finvl">
                        <td style="padding-top: 13px;" data-bind="text: Title">Финансовые вложения (за исключением денежных эквивалентов)</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1240</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_71" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_72" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_73" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_74" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_75" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="deneg">
                        <td style="padding-top: 13px;" data-bind="text: Title">Денежные средства и денежные эквиваленты</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1250</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_76" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_77" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_78" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_79" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_80" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr  class="proch">
                        <td style="padding-top: 13px;" data-bind="text: Title">Прочие оборотные активы</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1260</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_81" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_82" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_83" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_84" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_85" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr  class="itog2">
                        <td style="padding-top: 13px;" data-bind="text: Title">ИТОГО по разделу II</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1200</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_86" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_87" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_88" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_89" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_90" readonly="readonly" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr  class="bal">
                        <td style="padding-top: 13px;" data-bind="text: Title">БАЛАНС</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1600</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_91" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_92" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_93" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_94" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_95" readonly="readonly" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    <!-- /ko -->
                    
                    <tr>
                        <td colspan="7" style="text-align: center">
                            <h4 data-bind="text: Title">РАЗДЕЛ III. КАПИТАЛ И РЕЗЕРВЫ</h4>
                        </td>
                    </tr>
                    <!-- ko foreach: Items-->
                    <tr class="ustkap">
                        <td style="padding-top: 13px;" data-bind="text: Title">Уставный капитал (складочный капитал, уставный фонд, вклады товарищей)</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1310</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_96" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_97" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_98" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_99" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_100" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="sobakz">
                        <td style="padding-top: 13px;" data-bind="text: Title">Собственные акции, выкупленные у акционеров</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1320</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_101" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_102" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_103" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_104" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_105" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="pervne">
                        <td style="padding-top: 13px;" data-bind="text: Title">Переоценка внеоборотных активов</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1340</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_106" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_107" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_108" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_109" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_110" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="dobkap">
                        <td style="padding-top: 13px;" data-bind="text: Title">Добавочный капитал (без переоценки)</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1350</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_111" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_112" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_113" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_114" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_115" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="rezkap">
                        <td style="padding-top: 13px;" data-bind="text: Title">Резервный капитал</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1360</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_116" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_117" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_118" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_119" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_120" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="nerpri">
                        <td style="padding-top: 13px;" data-bind="text: Title">Нераспределенная прибыль (непокрытый убыток)</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1370</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_121" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_122" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_123" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_124" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_125" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="itog3">
                        <td style="padding-top: 13px;" data-bind="text: Title">ИТОГО по разделу III</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1300</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_126" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_127" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_128" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_129" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_130" readonly="readonly" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    <!-- /ko -->
                    
                    <tr>
                        <td colspan="7" style="text-align: center">
                            <h4 data-bind="text: Title">РАЗДЕЛ IV. ДОЛГОСРОЧНЫЕ ОБЯЗАТЕЛЬСТВА</h4>
                        </td>
                    </tr>
                    <!-- ko foreach: Items-->
                    <tr class="zaesred">
                        <td style="padding-top: 13px;" data-bind="text: Title">Заемные средства</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1410</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_131" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_132" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_133" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_134" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_135" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="otlnal4">
                        <td style="padding-top: 13px;" data-bind="text: Title">Отложенные налоговые обязательства</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1420</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_136" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_137" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_138" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_139" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_140" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="ocenoby4">
                        <td style="padding-top: 13px;" data-bind="text: Title">Оценочные обязательства</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1430</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_141" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_142" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_143" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_144" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_145" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="prooby4">
                        <td style="padding-top: 13px;" data-bind="text: Title">Прочие обязательства</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1450</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_146" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_147" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_148" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_149" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_150" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="itog4">
                        <td style="padding-top: 13px;" data-bind="text: Title">ИТОГО по разделу IV</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1400</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_151" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_152" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_153" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_154" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_155" readonly="readonly" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    <!-- /ko -->
                    
                    <tr>
                        <td colspan="7" style="text-align: center">
                            <h4 data-bind="text: Title">РАЗДЕЛ V. КРАТКОСРОЧНЫЕ ОБЯЗАТЕЛЬСТВА</h4>
                        </td>
                    </tr>
                    <!-- ko foreach: Items-->
                    <tr class="zaesred5">
                        <td style="padding-top: 13px;" data-bind="text: Title">Заемные средства</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1510</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_156" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_157" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_158" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_159" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_160" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="kredzad">
                        <td style="padding-top: 13px;" data-bind="text: Title">Кредиторская задолженность</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1520</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_161" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_162" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_163" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_164" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_165" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="vtchpros5">
                        <td style="padding-top: 13px;" data-bind="text: Title">в т.ч. Просроченная</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1521</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_166" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_167" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_168" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_169" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_170" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="dohbud">
                        <td style="padding-top: 13px;" data-bind="text: Title">Доходы будущих периодов</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1530</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_171" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_172" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_173" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_174" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_175" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="oceoby">
                        <td style="padding-top: 13px;" data-bind="text: Title">Оценочные обязательства</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1540</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_176" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_177" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_178" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_179" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_180" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="prochoby5">
                        <td style="padding-top: 13px;" data-bind="text: Title">Прочие обязательства</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1550</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_181" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_182" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_183" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_184" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_185" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="itog5">
                        <td style="padding-top: 13px;" data-bind="text: Title">ИТОГО по разделу V</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1500</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_186" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_187" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_188" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_189" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_190" readonly="readonly" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="bal2">
                        <td style="padding-top: 13px;" data-bind="text: Title">БАЛАНС</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">1700</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_191" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_192" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_193" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_194" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_195" readonly="readonly" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    <!-- /ko -->
    







	
                    <tr>
                        <td colspan="7" style="text-align: center">
                            <h4 data-bind="text: Title">ОТЧЕТ О ФИНАНСОВЫХ РЕЗУЛЬТАТАХ</h4>
                        </td>
                    </tr>
                    <!-- ko foreach: Items-->
					
                    <tr class="vyr6">
                        <td style="padding-top: 13px;" data-bind="text: Title">Выручка</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">2110</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_196" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_197" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_198" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_199" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_200" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="sebprod6">
                        <td style="padding-top: 13px;" data-bind="text: Title">Себестоимость продаж</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">2120</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_201" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_202" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_203" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_204" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_205" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="itog61">
                        <td style="padding-top: 13px;" data-bind="text: Title">Валовая прибыль (убыток)</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">2100</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_206" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_207" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_208" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_209" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_210" readonly="readonly" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="komras6">
                        <td style="padding-top: 13px;" data-bind="text: Title">Коммерческие расходы</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">2210</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_211" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_212" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_213" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_214" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_215" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="uprras6">
                        <td style="padding-top: 13px;" data-bind="text: Title">Управленческие расходы</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">2220</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_216" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_217" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_218" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_219" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_220" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="itog62">
                        <td style="padding-top: 13px;" data-bind="text: Title">Прибыль (убыток) от продаж</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">2200</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_221" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_222" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_223" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_224" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_225" readonly="readonly" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="dohot6">
                        <td style="padding-top: 13px;" data-bind="text: Title">Доходы от участия в других организациях</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">2310</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_226" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_227" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_228" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_229" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_230" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="prockpol6">
                        <td style="padding-top: 13px;" data-bind="text: Title">Проценты к получению</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">2320</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_231" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_232" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_233" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_234" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_235" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="prockupl6">
                        <td style="padding-top: 13px;" data-bind="text: Title">Проценты к уплате</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">2330</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_236" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_237" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_238" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_239" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_240" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="prochdoh6">
                        <td style="padding-top: 13px;" data-bind="text: Title">Прочие доходы</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">2340</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_241" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_242" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_243" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_244" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_245" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="prochras6">
                        <td style="padding-top: 13px;" data-bind="text: Title">Прочие расходы</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">2350</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_246" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_247" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_248" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_249" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_250" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="itog63">
                        <td style="padding-top: 13px;" data-bind="text: Title">Прибыль (убыток) до налогообложения</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">2300</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_251" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_252" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_253" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_254" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_255" readonly="readonly" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="teknal6">
                        <td style="padding-top: 13px;" data-bind="text: Title">Текущий налог на прибыль</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">2410</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_256" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_257" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_258" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_259" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_260" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="izmotobl6">
                        <td style="padding-top: 13px;" data-bind="text: Title">Изменение отложенных налоговых обязательств</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">2430</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_261" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_262" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_263" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_264" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_265" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="izmotlart6">
                        <td style="padding-top: 13px;" data-bind="text: Title">Изменение отложенных налоговых активов</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">2450</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_266" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_267" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_268" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_269" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_270" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="prochee6">
                        <td style="padding-top: 13px;" data-bind="text: Title">Прочее</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">2460</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_271" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_272" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_273" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_274" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_275" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="itog64">
                        <td style="padding-top: 13px;" data-bind="text: Title">Чистая прибыль (убыток)</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">2400</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_276" readonly="readonly" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_277" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_278" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_279" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_280" readonly="readonly" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    <!--
                    <tr>
                        <td style="padding-top: 13px;" data-bind="text: Title">Амортизация</td>
                        <td style="padding-top: 13px;">
                            
                            <span style="font-weight: bold;" data-bind="text: Code">5640</span>
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_281" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_282" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_283" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_284" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_285" type="text">
                        </td>
                        
                    </tr>
                     /ko -->
                    </tbody>
                </table></div>

    <div id="deleteDraftDialog" class="modal">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3>Удаление черновика</h3>
        </div>
        <div class="modal-body">
            <div class="control-group">
                <label>Удалить черновик отчетности?</label>
            </div>
        </div>
        <div class="modal-footer">
            <a class="btn btn-default btn-sm" href="/FinancialStatements/DeleteDraft?userId=39826">Удалить</a>
            <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Отмена</button>
        </div>
    </div>
    <div style="margin: 10px;" class=" text-right">
       <!-- <button class="btn btn-default btn-sm" data-bind="click: cancelEdit">Отменить</button>
        <button class="btn btn-primary btn-sm" data-bind="click: save">Сохранить</button>-->
		<a class="btn btn-default btn-sm" href="/zayavki/finothet.php">Отменить</a>
        <a class="btn btn-primary btn-sm save" href="#">Сохранить</a>
    </div>
</form>
            <div id="loadFileDialog" class="modal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3>Загрузить внешний XML</h3>
                </div>
                <div class="modal-body">
                    <div class="control-group">
                        <label>Отчётный период</label>
                        <select data-bind="options: typesList, optionsText : 'Title', optionsValue: 'id', value: selectedTypeId"><option value="9">Год (2015)</option><option value="10">Квартал 1 (2016)</option><option value="11">Квартал 2 (2016)</option><option value="12">Квартал 3 (2016)</option><option value="13">Год (2016)</option></select>
                        <a class="btn btn-primary btn-sm" data-bind="click: openFinanceStatmentLoader" href="#">Загрузить документ</a>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-bind="click: viewLoadFileDialog, enable: fileSelected" aria-hidden="true" disabled="">Просмотреть</button>
                    <button class="btn btn-primary" data-bind="click: сloseLoadFileDialog">Отмена</button>
                </div>
            </div>
            <input style="display: none" id="financeLoader" data-bind="event : {'change' : selectFile}" accept=".xml" type="file">
        </div>
    </div>
</div>
</div>
<script>
var Tab = {
    Swap: '<div class="swap">&nbsp;</div>',
    Load: function(id) {
        var tabs = id ? $('#' + id).children('.tab') : $('.tab-panel').children('.tab');
        for (var i = 0; i < tabs.length; i++) {
            var element = $(tabs[i]);
            if (i == 0) {
                Tab.InitActive(element);
            }
            Tab.InitTab(element);
        }
    },
    InitActive: function(element) {
        var jelement = $(element);
        jelement.prepend(Tab.Swap);
        jelement.addClass('active');
        var conentId = jelement.attr('content');
        if (conentId) {
            $('#' + conentId).css('display', 'block');
        }
        if (this.ActiveTabChanged) {
            this.ActiveTabChanged();
        }
    },
    RemoveActive: function(element) {
        var active = element ? $($(element).parent().find(".active")[0]) :  $('.tab-panel .active');
        active.removeClass('active');
        for (var i = 0; i < active.length; i++) {
            $(active[i]).children(".swap").remove();
            var conentId = active[i].getAttribute('content');
            if (conentId) {
                $('#' + conentId).css('display', 'none');
            }
        }
    },
    GetActiveItem: function() {
        var active = $('.tab-panel .active');
        return active;
    },
    InitTab: function(element) {
        $(element).on('click', function() {
            Tab.ClickTab(this);
        });
    },
    ClickTab: function (element) {
        if (element && $(element).hasClass('disabled')) {
            return;
        }
        Tab.RemoveActive(element);
        Tab.InitActive(element);
    },
    ActiveTabChanged: function() {}
};

var NavPills = {
    InitActive: function () {
        var jelement = $($('.nav-pills')[0]);
        $(jelement.children()[0]).addClass('active');
        var content = $($('.tab-pane')[0]);
        content.addClass('active');
    },
    RemoveActive: function () {
        var active = $('.nav-pills .active');
        active.removeClass('active');
        var content = $('.tab-pane .active');
        content.removeClass('active');
    }
};
</script>
<script>
   Tab.Load();

    var signingUrl = '/FinancialStatements/SigningData';
    var signedSaveUrl = '/FinancialStatements/SaveSign';
    var redirectUrl = '/FinancialStatements';
    var saveUrl = '/FinancialStatements/Save';
    var model = {"Id":0,"Headers":["Год (2015)","Квартал 1 (2016)","Квартал 2 (2016)","Квартал 3 (2016)","Год (2016)"],"Categories":[{"CategoryId":0,"Title":"РАЗДЕЛ I. ВНЕОБОРОТНЫЕ АКТИВЫ"},{"CategoryId":1,"Title":"РАЗДЕЛ II. ОБОРОТНЫЕ АКТИВЫ"},{"CategoryId":2,"Title":"РАЗДЕЛ III. КАПИТАЛ И РЕЗЕРВЫ"},{"CategoryId":3,"Title":"РАЗДЕЛ IV. ДОЛГОСРОЧНЫЕ ОБЯЗАТЕЛЬСТВА"},{"CategoryId":4,"Title":"РАЗДЕЛ V. КРАТКОСРОЧНЫЕ ОБЯЗАТЕЛЬСТВА"},{"CategoryId":5,"Title":"ОТЧЕТ О ФИНАНСОВЫХ РЕЗУЛЬТАТАХ"}],"Items":[{"CategoryId":0,"Code":"1110","Id":2,"Title":"Нематериальные активы","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":0,"Code":"1120","Id":3,"Title":"Результаты исследований и разработок","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":0,"Code":"1130","Id":4,"Title":"Нематериальные поисковые активы","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":0,"Code":"1140","Id":5,"Title":"Материальные поисковые активы","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":0,"Code":"1150","Id":6,"Title":"Основные средства","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":0,"Code":"1160","Id":7,"Title":"Доходные вложения в материальные ценности","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":0,"Code":"1170","Id":8,"Title":"Финансовые вложения","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":0,"Code":"1180","Id":9,"Title":"Отложенные налоговые активы","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":0,"Code":"1190","Id":10,"Title":"Прочие внеоборотные активы","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":0,"Code":"1100","Id":11,"Title":"Итого по разделу I","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":1,"Code":"1210","Id":12,"Title":"Запасы","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":1,"Code":"1220","Id":13,"Title":"Налог на добавленную стоимость по приобретённым ценностям","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":1,"Code":"1230","Id":14,"Title":"Дебиторская задолженность","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":1,"Code":"1231","Id":15,"Title":"в т.ч. просроченная","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":1,"Code":"1240","Id":16,"Title":"Финансовые вложения (за исключением денежных эквивалентов)","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":1,"Code":"1250","Id":17,"Title":"Денежные средства и денежные эквиваленты","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":1,"Code":"1260","Id":18,"Title":"Прочие оборотные активы","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":1,"Code":"1200","Id":19,"Title":"Итого по разделу II","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":1,"Code":"1600","Id":20,"Title":"БАЛАНС","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":2,"Code":"1310","Id":21,"Title":"Уставный капитал (складочный капитал, уставный фонд, вклады товарищей)","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":2,"Code":"1320","Id":22,"Title":"Собственные акции, выкупленные у акционеров","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":2,"Code":"1340","Id":23,"Title":"Переоценка внеоборотных активов","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":2,"Code":"1350","Id":24,"Title":"Добавочный капитал (без переоценки)","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":2,"Code":"1360","Id":25,"Title":"Резервный капитал","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":2,"Code":"1370","Id":26,"Title":"Нераспределенная прибыль (непокрытый убыток)","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":2,"Code":"1300","Id":27,"Title":"Итого по разделу III","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":3,"Code":"1410","Id":28,"Title":"Заемные средства","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":3,"Code":"1420","Id":29,"Title":"Отложенные налоговые обязательства","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":3,"Code":"1430","Id":30,"Title":"Оценочные обязательства","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":3,"Code":"1450","Id":31,"Title":"Прочие обязательства","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":3,"Code":"1400","Id":32,"Title":"Итого по разделу IV","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":4,"Code":"1510","Id":33,"Title":"Заемные средства","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":4,"Code":"1520","Id":34,"Title":"Кредиторская задолженность","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":4,"Code":"1521","Id":35,"Title":"в т.ч. Просроченная","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":4,"Code":"1530","Id":36,"Title":"Доходы будущих периодов","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":4,"Code":"1540","Id":37,"Title":"Оценочные обязательства","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":4,"Code":"1550","Id":38,"Title":"Прочие обязательства","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":4,"Code":"1500","Id":39,"Title":"Итого по разделу V","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":4,"Code":"1700","Id":40,"Title":"БАЛАНС","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":5,"Code":"2110","Id":41,"Title":"Выручка","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":5,"Code":"2120","Id":42,"Title":"Себестоимость продаж","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":5,"Code":"2100","Id":43,"Title":"Валовая прибыль (убыток)","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":5,"Code":"2210","Id":44,"Title":"Коммерческие расходы","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":5,"Code":"2220","Id":45,"Title":"Управленческие расходы","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":5,"Code":"2200","Id":46,"Title":"Прибыль (убыток) от продаж","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":5,"Code":"2310","Id":47,"Title":"Доходы от участия в других организациях","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":5,"Code":"2320","Id":48,"Title":"Проценты к получению","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":5,"Code":"2330","Id":49,"Title":"Проценты к уплате","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":5,"Code":"2340","Id":50,"Title":"Прочие доходы","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":5,"Code":"2350","Id":51,"Title":"Прочие расходы","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":5,"Code":"2300","Id":52,"Title":"Прибыль (убыток) до налогообложения","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":5,"Code":"2410","Id":53,"Title":"Текущий налог на прибыль","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":5,"Code":"2430","Id":54,"Title":"Изменение отложенных налоговых обязательств","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":5,"Code":"2450","Id":55,"Title":"Изменение отложенных налоговых активов","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":5,"Code":"2460","Id":56,"Title":"Прочее","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":5,"Code":"2400","Id":57,"Title":"Чистая прибыль (убыток)","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]},{"CategoryId":5,"Code":"5640","Id":58,"Title":"Амортизация","Values":[{"Id":0,"TypeId":9,"Value":null,"Type":4},{"Id":0,"TypeId":10,"Value":null,"Type":1},{"Id":0,"TypeId":11,"Value":null,"Type":2},{"Id":0,"TypeId":12,"Value":null,"Type":3},{"Id":0,"TypeId":13,"Value":null,"Type":4}]}],"SigningModel":null,"UserId":39826,"SigningStatus":{"Message":"Подписание не производилось","SignDate":null,"Status":0,"IsEditing":false,"AvailableForEditing":false,"FormattedString":""},"DraftStatus":{"HasDraftInfo":false,"IsInEditByClient":true,"IsInEditByAgent":false,"Message":""},"ApproveStatus":0,"ApproveComment":null,"SigningFullInfo":null,"HasBeenChangedByClient":false};
    var isEditMode = true;
    var isClientOrAgent = true;

    var computedItems = {
        "1100": function(models, typeId) {
            //1100 = 1110 + 1120 + 1130 + 1140 + 1150 + 1160 + 1170 + 1180 + 1190
            return models.findItemValue("1110", typeId) + models.findItemValue("1120", typeId) + models.findItemValue("1130", typeId)
                + models.findItemValue("1140", typeId) + models.findItemValue("1150", typeId) + models.findItemValue("1160", typeId)
                + models.findItemValue("1170", typeId) + models.findItemValue("1180", typeId) + models.findItemValue("1190", typeId);
        },
        "1200": function(models, typeId) {
            //1200 = 1210 + 1220 + 1230 + 1240 + 1250 + 1260
            return models.findItemValue("1210", typeId) + models.findItemValue("1220", typeId) + models.findItemValue("1230", typeId)
                + models.findItemValue("1240", typeId) + models.findItemValue("1250", typeId) + models.findItemValue("1260", typeId);
        },
        "1300": function(models, typeId) {
            //1300 = 1310 + 1320 + 1340 + 1350 + 1360 + 1370
            return models.findItemValue("1310", typeId) + models.findItemValue("1320", typeId) + models.findItemValue("1340", typeId)
                + models.findItemValue("1350", typeId) + models.findItemValue("1360", typeId) + models.findItemValue("1370", typeId);
        },
        "1400": function(models, typeId) {
            //1400 = 1410 + 1420 + 1430 + 1450
            return models.findItemValue("1410", typeId) + models.findItemValue("1420", typeId) + models.findItemValue("1430", typeId)
                + models.findItemValue("1450", typeId);
        },
        "1500": function(models, typeId) {
            //1500 = 1510 + 1520 + 1530 + 1540 + 1550
            return models.findItemValue("1510", typeId) + models.findItemValue("1520", typeId) + models.findItemValue("1530", typeId)
                + models.findItemValue("1540", typeId) + models.findItemValue("1550", typeId);
        },
        "1600": function(models, typeId) {
            //1600 = 1100 + 1200
            return models.findItemValue("1100", typeId) + models.findItemValue("1200", typeId);
        },
        "1700": function(models, typeId) {
            //1700 = 1300 + 1400 + 1500
            return models.findItemValue("1300", typeId) + models.findItemValue("1400", typeId) + models.findItemValue("1500", typeId);
        },
        "2100": function(models, typeId) {
            // 2100 = 2110 – 2120
            return models.findItemValue("2110", typeId) - models.findItemValue("2120", typeId);
        },
        "2200": function(models, typeId) {
            //2200 = 2100 – 2210 – 2220
            return models.findItemValue("2100", typeId) - models.findItemValue("2210", typeId) - models.findItemValue("2220", typeId);
        },
        "2300": function(models, typeId) {
            //2300 = 2200 + 2310 + 2320 – 2330 + 2340 – 2350
            return models.findItemValue("2200", typeId) + models.findItemValue("2310", typeId) + models.findItemValue("2320", typeId)
                - models.findItemValue("2330", typeId) + models.findItemValue("2340", typeId) - models.findItemValue("2350", typeId);
        },
        "2400": function(models, typeId) {
            //2400 = 2300 – 2410 – 2430 + 2450 – 2460
            return models.findItemValue("2300", typeId) - models.findItemValue("2410", typeId) - models.findItemValue("2430", typeId)
                + models.findItemValue("2450", typeId) - models.findItemValue("2460", typeId);
        }
    };

    function createViewModel(model) {
        var self = this;

        self.readonly = isEditMode ? null : "readonly";
        self.selectedTypeId = ko.observable(1);
        self.typesList = ko.observableArray([]);
        self.fileSelected = ko.observable(false);

        var vals = model.Items[0].Values;
        for (var h = 0; h < model.Headers.length;h ++) {
        self.typesList.push({ id: vals[h].TypeId, Title: model.Headers[h] });
    }

    self.initializationFinished = ko.observable(false);

    var computedItemMapping = {
        "Values": {
            "key" : function(data) {
                return ko.utils.unwrapObservable(data.TypeId);
            },
            "create" : function (options) {
                var result = ko.mapping.fromJS(options.data);
                var code = ko.utils.unwrapObservable(options.parent.Code);
                if (!self.readonly && computedItems[code]) {
                    result.Value = ko.computed(function() {
                        if (!self.initializationFinished()) {
                            return 0;
                        }

                        return computedItems[code](self.models, this.TypeId());
                    }, result);
                    result.readonly = "readonly";
                } else {
                    result.readonly = null;
                }

                return result;
            }
        }
    };

    var mainModelMapingOptions = {
        "copy" : ["Categories", "Headers"],
        "Items": {
            key: function(data) {
                return ko.utils.unwrapObservable(data.Code);
            },
            create: function(options) {
                return ko.mapping.fromJS(options.data, computedItemMapping);
            }
        }
    };

    self.models = ko.mapping.fromJS(model, mainModelMapingOptions);
    self.models.findItemValue = function(code, typeId) {
        var index = self.models.Items.mappedIndexOf({ Code: code });
        if (index < 0) {
            return 0;
        }

        var item = self.models.Items()[index];
        index = item.Values.mappedIndexOf({ TypeId: typeId });
        if (index < 0) {
            return 0;
        }

        return item.Values()[index].Value();
    };

    self.initializationFinished(true);

    self.validateSigningData = function() {
        var message = "";
        var values = self.models.Items()[0].Values();
        for (var i = 0; i < values.length; i++) {
            var typeId = values[i].TypeId();
            var val1600 = self.models.findItemValue("1600", typeId);
            var val1700 = self.models.findItemValue("1700", typeId);
            if (Math.abs(val1700 - val1600) > 1) {
                if (message) {
                    message += ", ";
                }

                message += self.models.Headers[i].toLowerCase();
            }
        }

        return message;
    };

    self.models.ItemsByCategory = ko.computed(function() {
        var result = [];
        var dict = {};
        for (var i = 0; i < this.Categories.length; i++) {
            var category = {
                Title: this.Categories[i].Title,
                Id: this.Categories[i].CategoryId,
                Items: []
            };

            result.push(category);
            dict[category.Id] = category;
        }

        var items = this.Items();
        for (i = 0; i < items.length; i++) {
            dict[items[i].CategoryId()].Items.push(items[i]);
        }

        return result;
    }, self.models);

    self.getFormData = function() {
        var sendModel = {
            Headers: self.models.Headers,
            Items: self.models.Items(),

            SigningModel: self.models.SigningModel(),
            UserId: self.models.UserId()
        };
        var formData = ko.mapping.toJSON(sendModel);
        return formData;
    };
    self.openDeleteDraftDialog = function() {
        $("#deleteDraftDialog").modal('show');
    };
    self.cancelEdit = function() {
        window.location.href = '/FinancialStatements/CancelEdit/39826';
    };
    self.takeFromAgent = function() {
        window.location.href = '/FinancialStatements/TakeFromAgent/39826';
    };
    WhaitModal.Load();
    self.save = function() {
        if (!$('form').valid()) {
            displayMessage('Неверный формат ввода данных.');
            return;
        }
        WhaitModal.Show();
        var formData = self.getFormData();
        var options = {
            url: saveUrl,
            type: 'POST',
            data: { model: formData },
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            cache: false
        };
        $.ajax(options).success(function(data) {
            //ko.mapping.fromJS(data, mainModelMapingOptions, self.models);
            WhaitModal.Close();
            displayMessage(data.Text, data.StatusCommand);
            if (data.StatusCommand == 0) {
                window.location.href = '/FinancialStatements/Index/39826';
            }

            //applyValidation();
        });
    };
    self.singAndSave = function() {
        if (!$('form').valid()) {
            displayMessage('Неверный формат ввода данных.');
            return;
        }

        var message = self.validateSigningData();
        if (message) {
            message = "Значение баланса не совпадают за " + message;
            displayMessage(message);
            return;
        }

        WhaitModal.Show();
        var formData = self.getFormData();
        var options = {
            url: signingUrl,
            type: 'POST',
            data: { model: formData },
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            cache: false
        };
        $.ajax(options).success(function(data) {
            WhaitModal.Close();
            SingData(data);
            //ko.mapping.fromJS(data, mainModelMapingOptions, self.models);
            //displayMessage("Данные сохранены!","success");
            //applyValidation();
        });
    };

    self.openLoaderDialog = function (){
        $("#financeLoader").val("");
        self.fileSelected(false);
        $("#loadFileDialog").modal('show');
    };

    self.openFinanceStatmentLoader = function (){
        $('#financeLoader').click();
    };
    self.сloseLoadFileDialog = function(){
        $("#loadFileDialog").modal('hide');
    };
    self.viewLoadFileDialog = function () {
        $("#loadFileDialog").modal('hide');
        self.loadFinanceStatment();
    };

    self.isVisible = model.SigningStatus.Status == 2 && !isClientOrAgent;

    self.ReportToExcel = function () {
        var url = '/FinancialStatements/GetFinanceReport';
        window.location = url + '/?uid=' + self.models.UserId();
    };

    self.selectFile = function(){
        self.fileSelected(document.getElementById("financeLoader").files.length > 0);
    };

    self.prevSelectedPeriod = self.selectedTypeId();
    self.loadFinanceStatment = function (){
        var input = document.getElementById("financeLoader");
        if (input.files.length != 0) {
            var selectedTypeText = '';
            for (var i = 0; i < +self.typesList().length; i++) {
                if (self.typesList()[i].id==self.selectedTypeId()) {
                    selectedTypeText = self.typesList()[i].Title;
                    break;
                }
            }
            if (!confirm('Данные за ' +selectedTypeText+ ' года будут заменены данными из "'+input.files[0].name+'" файла, продолжить?')) {
                return;
            }

            self.prevSelectedPeriod = self.selectedTypeId();
            var doc = new FormData();
            doc.append(input.files[0].name, input.files[0]);
            $.ajax({
                url: '/FinancialStatements/PostDocument',
                type: 'POST',
                data: doc,
                dataType: "json",
                contentType: false,
                processData: false,
                success: function (data) {
                    console.log(self.models.Items().length);
                    var items = self.models.Items();
                    for (var i = 0; i < items.length; i++) {
                        var code = items[i].Code();
                        var d = null;
                        for(var n = 0; n < data.length; n++){
                            if (data[n].Code == code){
                                d = data[n].Value;
                                break;
                            }
                        }

                        //if (d == null) continue;

                        for(n = 0; n <items[i].Values().length; n++){
                            var cell = items[i].Values()[n];
                            if (ko.isWriteableObservable(cell.Value)) {
                                if (self.selectedTypeId() == cell.TypeId()) {
                                    if (d==null) {
                                        d = 0;
                                    }

                                    cell.Value(d);
                                    //break;
                                }
                            }
                        }
                    }
                }
            }).fail(function (err){
                //console.log(err);
            });
        }
    };

    self.LoadDataFromKartoteka = function() {
        WhaitModal.Show();
        var options = {
            url: '/FinancialStatements/LoadKartotekaData',
            type: 'POST',
            dataType: "json",
            contentType: "application/json",
            cache: false,
            data: JSON.stringify({ userId: self.models.UserId()})
        };
        $.ajax(options).success(function(finStatementsData) {
            WhaitModal.Close();
            if (!finStatementsData.data) {
                displayMessage(finStatementsData.message, "error");
            } else {
                var dataItems = finStatementsData.data.Items;
                for (var i = 0; i < dataItems.length; i++) {
                    if (dataItems[i].Values.length > 0) {
                        var index = self.models.Items.mappedIndexOf({ Code: dataItems[i].Code });
                        if (index < 0) {
                            return 0;
                        }

                        var item = self.models.Items()[index];
                        for (var j = 0; j < dataItems[i].Values.length; j++) {
                            index = item.Values.mappedIndexOf({ TypeId: dataItems[i].Values[j].TypeId });
                            if (index < 0) {
                                return 0;
                            }

                            item.Values()[index].Value(dataItems[i].Values[j].Value);
                        }
                        //index = item.Values.mappedIndexOf({ TypeId: dataItems[i].Values[0].TypeId });
                        //if (index < 0) {
                        //    return 0;
                        //}

                        //item.Values()[index].Value(dataItems[i].Values[0].Value);
                    }
                }
            }
        }).error(function(errData) {
            WhaitModal.Close();
        });
    };
    }

    //function applyValidation() {
    //    var currForm = $("form");
    //    currForm.removeData("validator");
    //    currForm.removeData("unobtrusiveValidation");
    //    $.validator.unobtrusive.parse(currForm);
    //    currForm.validate();
    //}

    $(function() {
        ko.applyBindings(new createViewModel(model), document.getElementById('context'));
        $("form").bind("keypress", function (e) {
            if (e.keyCode == 13) {
                return false;
            }
        });
        //applyValidation();
    });

    function SingData(data) {
        var singningObject = {
            signData: data.data,
            signUrl: signedSaveUrl,
            redirectUrl: redirectUrl
        };

        window.akb.signing.new.common.signViewModel.InitModel(singningObject);
        window.akb.signing.new.common.signViewModel.OpenWindow();
    }

    function getIndicesOf(searchStr, str, caseSensitive) {
        var startIndex = 0, searchStrLen = searchStr.length;
        var index, indices = [];
        if (!caseSensitive) {
            str = str.toLowerCase();
            searchStr = searchStr.toLowerCase();
        }
        while ((index = str.indexOf(searchStr, startIndex)) > -1) {
            indices.push(index);
            startIndex = index + searchStrLen;
        }
        return indices;
    }

    function _UpdateTableFloatingHeaders() {
        $("div.divTableWithFloatingHeader").each(function() {
            var originalHeaderRow = $(".tableFloatingHeaderOriginal", this);
            var floatingHeaderRow = $(".tableFloatingHeader", this);
            var offset = $(this).offset();
            var scrollTop = $(window).scrollTop();
            // check if floating header should be displayed
            if ((scrollTop > offset.top) && (scrollTop < offset.top + $(this).height() - originalHeaderRow.height())) {
                floatingHeaderRow.css("visibility", "visible");
                floatingHeaderRow.css("left", -$(window).scrollLeft());
                floatingHeaderRow.css("margin-left", $(this).offset().left);
                
                $("th", floatingHeaderRow).each(function(index) {
                    var cellWidth = $("th", originalHeaderRow).eq(index).css('width');
                    $(this).css('width', cellWidth);
                });

                // Copy row width from whole table
                floatingHeaderRow.css("width", Math.max(originalHeaderRow.width(), $(this).width()) + "px");
            }
            else {
                floatingHeaderRow.css("visibility", "hidden");
            }
        });
    }

    
    function ActivateFloatingHeaders(selector_str){
        $(selector_str).each(function() {
            $(this).wrap("<div class=\"divTableWithFloatingHeader\" style=\"position:relative\;\"></div>");

            // use first row as floating header by default
            var floatingHeaderSelector = "tr:first";
            var explicitFloatingHeaderSelector = "tr.floating-header"
            if ($(explicitFloatingHeaderSelector, this).length){
                floatingHeaderSelector = explicitFloatingHeaderSelector;
            }

            var originalHeaderRow = $(floatingHeaderSelector, this).first();
            var clonedHeaderRow = originalHeaderRow.clone()
            originalHeaderRow.before(clonedHeaderRow);

            clonedHeaderRow.addClass("tableFloatingHeader");
            clonedHeaderRow.css("position", "fixed");
            // not sure why but 0px is used there is still some space in the top
            clonedHeaderRow.css("top", "-2px");
            clonedHeaderRow.css("margin-left", $(this).offset().left);
            clonedHeaderRow.css("margin-top", "55px");
            clonedHeaderRow.css("background-color", "white");
            clonedHeaderRow.css("visibility", "hidden");

            originalHeaderRow.addClass("tableFloatingHeaderOriginal");
        });
        _UpdateTableFloatingHeaders();
        $(window).scroll(_UpdateTableFloatingHeaders);
        $(window).resize(_UpdateTableFloatingHeaders);
    }

    $(document).ready(function() {
        ActivateFloatingHeaders("#finTable");
    });
</script>
<script>
      var monthes = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];

        window.datepickerOptions = {
            dateFormat: 'dd-mm-yy',
            closeText: 'Закрыть',
            prevText: '&#x3c;Пред',
            nextText: 'След&#x3e;',
            currentText: 'Сегодня',
            monthNames: monthes,
            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            weekHeader: 'Не',
            changeMonth: true,
            changeYear: true,
            firstDay: 1,
            yearRange: 'c-70:c+10'
        }

        $(function () {
            $('[data-toggle="tooltip"]').tooltip({
                template: '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner padded"></div></div>'
            });
            generateLinksForTableRows();
            $(document).ajaxComplete(function () {
                generateLinksForTableRows();
            });

            $('input.to-date').attr('placeholder', 'Выберите дату');
            $("input.to-date-back").attr('placeholder', 'Выберите дату');

            $('input.to-date').datepicker(window.datepickerOptions);
            $("input.to-date-back").datepicker($.extend({}, window.datepickerOptions, { maxDate: 0 }));

            $('input.to-date').on('keydown', function (envt) {
                if (envt.keyCode != 8 && envt.keyCode != 46) return false;
                event.target.value = '';
                return true;
            });

            $('input.to-date-back').on('keydown', function (envt) {
                if (envt.keyCode != 8 && envt.keyCode != 46) return false;
                event.target.value = '';
                return true;
            });

            initFileIcons();
            $(document.body).on('change', initFileIcons);
            $.fn.modal.Constructor.prototype.enforceFocus = function () { };
            //initFileinputs();
        });

        function initFileinputs() {
            $('input[type="file"]').each(function() {
                var mainClass = $(this).attr('class') || "span6";
                $(this).fileinput({
                    removeLabel: "Удалить",
                    uploadLabel: "Загрузить",
                    browseLabel: "Выбрать..",

                    removeIcon: '<i class="icon-trash"></i> ',
                    uploadIcon: '<i class="icon-upload"></i> ',
                    browseIcon: '<i class="icon-folder-open icon-white"></i> ',

                    showRemove: false,
                    showUpload: false,
                    showPreview: false,
                    maxFileCount: 10,
                    msgSelected: '{n}',
                    mainClass: mainClass
                });
            });
        }

        function generateLinksForTableRows() {
            $('table a.row-link').parents('table').addClass('clickable-row');
            $('table tr').each(function() {
                var $linkForRow = $(this).find('a.row-link').first();
                if ($linkForRow.length) {
                    $(this).on('click', function(trEvent) {
                        trEvent.stopImmediatePropagation();

                        $linkForRow.off('click').on('click', function(linkEvent) {
                            linkEvent.stopPropagation();
                            return true;
                        });

                        $linkForRow[0].click();
                    });
                }
            });
        }

        function initFileIcons() {
            $('.files .file-name,.well .file-name').each(function() {
                var innerText = $(this).text(),
                    fileName = $.trim(innerText),
                    fileNameSplitted = fileName.split('.'),
                    fileNameSplittedLen = fileNameSplitted.length,
                    fileExt = '';

                if (fileNameSplittedLen > 1) {
                    fileExt = fileNameSplitted[fileNameSplittedLen - 1];
                    fileExt = fileExt.toLocaleLowerCase();
                    if (fileExt == 'jpeg') {
                        fileExt = 'jpg';
                    }
                    $(this).find('i.filetype').addClass(fileExt);
                } else {
                    $(this).find('i.filetype').addClass('doc');
                }
            });
        }

        // Tooltip для валидацый
        (function() {
            $.validator.setDefaults({
                showErrors: function(errorMap, errorList) {
                    this.defaultShowErrors();

                    $("." + this.settings.validClass).tooltip("destroy");

                    for (var i = 0; i < errorList.length; i++) {
                        var error = errorList[i];

                        $("#" + error.element.id).tooltip("destroy");
                        $("#" + error.element.id)
                            .tooltip({
                                html: true,
                                title: '<div class="tooltip-alert alert-danger">' + error.message + '</div>',
                                placement: function(tip, element) {
                                    var position = $(element).position(),
                                        elementWidth = $(element).width(),
                                        bodyWidth = $('body').width();

                                    if (bodyWidth - (position.left + elementWidth) < 200) {
                                        return "left";
                                    }

                                    return "right";
                                }
                            });
                    }
                },
                ignore: []
            });

            $.validator.methods.range = function(value, element, param) {
                var globalizedValue = value.replace(",", ".");
                return this.optional(element) || (globalizedValue >= param[0] && globalizedValue <= param[1]);
            };

            $.validator.methods.number = function(value, element) {
                return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:[\s\.,]\d{3})+)(?:[\.,]\d+)?$/.test(value);
            };

            function UnauthorizedHandler(request) {
                var isLogon = '"_Logon_"' == request.responseText;
                console.log(isLogon);

                if (isLogon) {
                    $('#logonContent').load('/Account/LogonAjax', function () {
                        $('#logonModal').modal('show');
                    });
                }

                try {
                    var resultJson = JSON.parse(request.responseText);
                    if (resultJson.StatusCommand) {
                        displayMessage(resultJson.Text, resultJson.StatusCommand);
                    }
                }
                catch(e) {
                    
                }
            }
            $(document).ajaxComplete(function (event, xhr, settings) {
                UnauthorizedHandler(xhr);
               
                console.log("Triggered ajaxComplete handler." + settings.url);
            });
            $(document).ajaxError(function (event, request, settings) {
                UnauthorizedHandler(request);
                console.log("Triggered ajaxError handler." + settings.url);
            });
        })();
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>