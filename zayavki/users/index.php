<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Пользователи");
?>
<div class="popup-rgba">
        <div class="popup-tovar-zoom-main popup_item">
                <div class="popup-tovar-zoom-inner">
                        <div class="popup-tovar-zoom change_passord">
                                <span class="close-form"></span>
                                <form method="POST" action='/include/change_password.php' class="on_ajax popup"> 
                                        <input type="hidden" name="id_element" value="">
                                        <div class="element_anketa">
                                                <input type="text" name="password" placeholder="Новый пароль">
                                        </div>
                                        <div class="element_anketa">
                                                <input type="text" name="new_password" placeholder="Подтвердить новый пароль">
                                        </div>
                                        <div class="element_anketa error-text">

                                        </div>                                        
                                        <div class="element_anketa">
                                                <button>Сохранить</button>
                                        </div>
                                </form>
                        </div>
                </div>
        </div>
        <div class="popup-tovar-zoom-main popup_delete">
                <div class="popup-tovar-zoom-inner">
                        <div class="popup-tovar-zoom delete">
                                <span class="close-form"></span>
                                <form method="POST" action='/include/user_delete.php' class="on_ajax popup">
                                        <input type="hidden" name="id_element" value="">
                                        <div class="element_anketa">
                                                Вы действительно хотите удалить?
                                        </div>   
                                        <div class="element_anketa error-text">

                                        </div>                                        
                                        <div class="element_anketa">
                                                <button>Удалить</button>
                                        </div>                        
                                </form>
                        </div>                                                        
                </div>
        </div>
        <div class="popup-tovar-zoom-main popup_add_user">
                <div class="popup-tovar-zoom-inner">
                        <div class="popup-tovar-zoom add_user">
                                <span class="close-form"></span>
                                <form class="on_ajax popup" action="/include/add_user.php" method="POST">
                                        <div class="element_anketa">
                                                <input type="text" placeholder='ФИО' name="NAME">
                                        </div>
                                        <div class="element_anketa">
                                                <input type="text" placeholder='E-mail' name="EMAIL" class='requared' required>
                                        </div>
                                        <div class="element_anketa">
                                                <input type="text" placeholder='Телефон' name="PERSONAL_MOBILE">
                                        </div>
                                        <div class="element_anketa">
                                                <input type="radio" name="GROUP" id="ag"><label for="ag">Агент</label>
                                                <input type="radio" name="GROUP" value="6" id="sb"><label for="sb">Сотрудник банка</label>
                                                <input type="radio" name="GROUP" value="7" id="op"><label for="op">Оператор</label>
                                        </div>
                                        <div class="element_anketa">
                                                <input type="password" placeholder='Пароль' name="PASSWORD" class='requared' required>
                                        </div>
                                        <div class="element_anketa">
                                                <input type="password" placeholder='Подтверждение пароля' name="CONFIRM_PASSWORD" class='requared' required>
                                        </div>

                                        <div class="element_anketa">
                                                <div class="error-text"></div>
                                        </div>
                                        <div class="element_anketa">
                                                <button>Добавить</button>
                                        </div>
                                </form>
                                
                        </div>                                                        
                </div>
        </div>
        

</div>

<?
global $USER;
$rsUsers = CUser::GetList(($by="id"), ($order="desc"), array()); // выбираем пользователей
while($arUsers = $rsUsers->GetNext()){
        if(CUser::IsOnLine($arUsers['ID'],120)){
                $arUsers['WORK'] = 'Активный';  
        }else{
                $arUsers['WORK'] = 'Не активный';  
        }
        $arGroups = CUser::GetUserGroup($arUsers['ID']);
        if(in_array(6, $arGroups)){
                $arUsers['STATUS'] = 'Работник банка';
        }elseif(in_array(7, $arGroups)){
                $arUsers['STATUS'] = 'Оператор';
        }else{
                $arUsers['STATUS'] = 'Агент';
        }
        $user[]=$arUsers;
}


//preprint($user);
?>
<div class="wrapper">
        <table class="width_100">
                <tr>
                        <th>Логин</th>
                        <th>E-mail</th>
                        <th>Телефон</th>
                        <th>Статус</th>
                        <th>Работа</th>
                        <th></th>
                        <th></th>
                </tr>
                <?foreach($user as $item){?>
                        <tr>
                                <td><?=$item['LOGIN']?></td>
                                <td><?=$item['EMAIL']?></td>
                                <td><?=$item['PERSONAL_MOBILE']?></td>
                                <td><?=$item['STATUS']?></td>
                                <td><?=$item['WORK']?></td>
                                <td><a href="#" class='change_passord' rel='<?=$item['ID']?>'>Сменить пароль</a></td>
                                <td><a href="#" class='delete' rel='<?=$item['ID']?>'>Удалить</a></td>                                
                        </tr>
                <?}?>
        </table>
        <div class="element_anketa">
                <button class='add_user'>Добавить пользователя</button>
        </div>
        
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>