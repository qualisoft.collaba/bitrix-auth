<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<?if($arResult['PROPERTIES']['STATUS']['VALUE'] == 'Черновик'){?>
                <div class="wrapper">
                        <form method="POST" action='/include/add_BG.php' class='on_ajax add_element' enctype="multipart/form-data">
                                <div class="garantii">
                                        <input type='hidden' name='ID' value='<?=$arResult['ID']?>'>
                                        <?  foreach ($arResult['PROPERTIES'] as $k=>$item){
                                                if(!empty($item['VALUE'])){
                                                        if(strlen($k) == 2){?>
                                                                <input type='hidden' name='<?=$k?>_k' value='1'>
                                                        <?}
                                                }
                                        }?>
                                        <span class="title_table">Условия гарантии</span>
                                        <div class="element_anketa">
                                                <input type="text" name='A' placeholder='Номер извещения/закупки' value='<?=$arResult['PROPERTIES']['A']['VALUE']?>'>
                                        </div>
                                        <div class="element_anketa">
                                                <input class="main_element_form" type="text" name='B' placeholder='Сумма БГ, руб' value='<?=num($arResult['PROPERTIES']['B']['VALUE'])?>'>
                                        </div>
                                        <div class="element_anketa">
                                                <input type="text" name='C' placeholder='Дата опубликования протокола победителей' class="datepicker" autocomplete="false"  value='<?=$arResult['PROPERTIES']['C']['VALUE']?>'>
                                        </div>
                                        <div class="element_anketa">
                                                Требуемый Срок действия банковской гарантии с <input type="text" name='D1' placeholder='' class="date_time datepicker" autocomplete="false"  value='<?=$arResult['PROPERTIES']['D1']['VALUE']?>'> по <input type="text" name='D2' placeholder='' class="date_time datepicker" autocomplete="false"  value='<?=$arResult['PROPERTIES']['D2']['VALUE']?>'> <span class="error text"></span>
                                        </div>
                                        <div class="element_anketa">
                                                Аванс
                                                <input class="required" type="radio" id="avans11" name="E" value="1"<?if($arResult['PROPERTIES']['E']['VALUE']==1){?> checked<?}?>>
                                                <label for="avans11">Да</label>
                                                <input class="required" type="radio" id="avans12" name="E" value="2"<?if($arResult['PROPERTIES']['E']['VALUE']==2){?> checked<?}?>>
                                                <label for="avans12">Нет</label>
                                        </div>
                                        <div class="element_anketa">
                                                Бесспорное списание по БГ 
                                                <input class="required" type="radio" id="avans21" name="F" value="3"<?if($arResult['PROPERTIES']['F']['VALUE']==3){?> checked<?}?>>
                                                <label for="avans21">Да</label>
                                                <input class="required" type="radio" id="avans22" name="F" value="4"<?if($arResult['PROPERTIES']['F']['VALUE']==4){?> checked<?}?>>
                                                <label for="avans22">Нет</label>
                                        </div>
                                        <div class="element_anketa">
                                                Снижение на более 25% от начальной цены
                                                <input class="required" type="radio" id="avans31" name="G" value="5"<?if($arResult['PROPERTIES']['G']['VALUE']==5){?> checked<?}?>>
                                                <label for="avans31">Да</label>
                                                <input class="required" type="radio" id="avans32" name="G" value="6"<?if($arResult['PROPERTIES']['G']['VALUE']==6){?> checked<?}?>>
                                                <label for="avans32">Нет</label>
                                        </div>  
                                        <div class="element_anketa">
                                                <textarea placeholder='Исключение из текста БГ' cols="100" rows="10" name='H'><?=$arResult['PROPERTIES']['H']['VALUE']?></textarea>
                                        </div>       
                                        <div class="element_anketa">
                                                Требования к БГ
                                                <input class="required" type="radio" id="avans141" name="I" value="7"<?if($arResult['PROPERTIES']['I']['VALUE']==7){?> checked<?}?>>
                                                <label for="avans141">Да</label>
                                                <input class="required" type="radio" id="avans142" name="I" value="8"<?if($arResult['PROPERTIES']['I']['VALUE']==8){?> checked<?}?>>
                                                <label for="avans142">Нет</label>
                                        </div>       
                                        <div class="element_anketa">
                                                Система налогооблажения клиента
                                                <div class="new-select-style-wpandyou-anceta">
                                                        <select name="J" class="main_element_form">
                                                                <option value=""<?if($arResult['PROPERTIES']['J']['VALUE']==''){?> selected<?}?>>Тип налогооблажения</option>
                                                                <option value="9"<?if($arResult['PROPERTIES']['J']['VALUE']=='общая'){?> selected<?}?>>Общая</option>
                                                                <option value="10"<?if($arResult['PROPERTIES']['J']['VALUE']=='упрощенная'){?> selected<?}?>>Упрощенная</option>
                                                        </select>
                                                </div>
                                        </div>    
                                </div>                                  
                                <div class="anketa">
                                        <span class="title_table">Анкета</span>
                                        <div class="element_anketa">
                                                <input type="text" name='K' placeholder='Полное наименование ЮЛ' value='<?=$arResult['PROPERTIES']['K']['VALUE']?>'>
                                        </div>
                                        <div class="element_anketa">
                                                <input type="text" name='L' placeholder='Краткое наименование ЮЛ' value='<?=$arResult['PROPERTIES']['L']['VALUE']?>'>
                                        </div>
                                        <div class="element_anketa">
                                                <input type="text" name='M' placeholder='ИНН принципала' value='<?=$arResult['PROPERTIES']['M']['VALUE']?>'>
                                        </div>
                                        <div class="element_anketa">
                                                Шаблон заказчика
                                                <input class="required" type="radio" id="avans41" name="N" value="11"<?if($arResult['PROPERTIES']['N']['VALUE']=='11'){?> selected<?}?>>
                                                <label for="avans41">Да</label>
                                                <input class="required" type="radio" id="avans42" name="N" value="12"<?if($arResult['PROPERTIES']['N']['VALUE']=='12'){?> selected<?}?>>
                                                <label for="avans42">Нет</label>

                                        </div>
                                        <div class="element_anketa">
                                                Срочная отправка оригинала экспесс почтой
                                                <input class="required" type="radio" id="avans51" name="O" value="13"<?if($arResult['PROPERTIES']['O']['VALUE']=='13'){?> selected<?}?>>
                                                <label for="avans51">Да</label>
                                                <input class="required" type="radio" id="avans52" name="O" value="14"<?if($arResult['PROPERTIES']['O']['VALUE']=='14'){?> selected<?}?>>
                                                <label for="avans52">Нет</label>

                                        </div>
                                        <div class="element_anketa">
                                                <input type="text" name='P' placeholder='Заказчик по договору' value='<?=$arResult['PROPERTIES']['P']['VALUE']?>'>                                
                                        </div>
                                        <div class="element_anketa">
                                                <input type="text" name='Q' placeholder='ИНН Заказчика' value='<?=$arResult['PROPERTIES']['Q']['VALUE']?>'>                                
                                        </div>
                                </div>
                                <div class="uslugi">
                                        <?$res = $arResult;?>
                                        <?global $arResult;?>
                                        <?$arResult = $res;?>
                                        <?if(($arResult['PROPERTIES']['B']['VALUE']<10000000)){
                                                if($arResult['PROPERTIES']['J']['VALUE'] == 'общая'){                          //общая
                                                                require($_SERVER["DOCUMENT_ROOT"]."/include/result_a_a.php");
                                                                require($_SERVER["DOCUMENT_ROOT"]."/include/result_a_u.php");
                                                }elseif($arResult['PROPERTIES']['J']['VALUE'] == 'упрощенная'){                    //упрощенка
                                                                require($_SERVER["DOCUMENT_ROOT"]."/include/result_a_b.php");
                                                                require($_SERVER["DOCUMENT_ROOT"]."/include/result_a_u.php");
                                                }
                                        }else{
                                                if($arResult['PROPERTIES']['J']['VALUE'] == 'общая'){                          //общая
                                                                require($_SERVER["DOCUMENT_ROOT"]."/include/result_b_a.php");
                                                                require($_SERVER["DOCUMENT_ROOT"]."/include/result_b_u.php");

                                                }elseif($arResult['PROPERTIES']['J']['VALUE'] == 'упрощенная'){                    //упрощенка
                                                                require($_SERVER["DOCUMENT_ROOT"]."/include/result_b_b.php");
                                                                require($_SERVER["DOCUMENT_ROOT"]."/include/result_b_u.php");
                                                }

                                        }?>
                                </div>
                                <div class="error-text">

                                </div>
                                <div class="element_anketa">
                                        <button name='STATUS' value='15'>Сохранить в черновик</button> <button name='STATUS' value='16' class="real">Передать в банк</button>
                                </div>  
                        </form>
                </div>
        
<?}else{

        $bg = $arResult['PROPERTIES']['B']['VALUE'];
        $arResult['PROPERTIES']['B']['VALUE'] = num($arResult['PROPERTIES']['B']['VALUE']);
        $sys = $arResult['PROPERTIES']['J']['VALUE'];
        if($bg<10000000){
                if($sys == 'общая'){
                        foreach($arResult['PROPERTIES'] as $k=>$item){
                                if(strlen($k)==2){
                                        if((substr($k, 0, 1)=='A')||(substr($k, 0, 1)=='C')){
                                                $propertys[$k] = $item;
                                        }
                                }else{
                                        $propertys[$k] = $item;
                                }
                        }
                }elseif($sys == 'упрощенная'){
                        foreach($arResult['PROPERTIES'] as $k=>$item){
                                if(strlen($k)==2){
                                        if((substr($k, 0, 1)=='B')||(substr($k, 0, 1)=='C')){
                                                $propertys[$k] = $item;
                                        }
                                }else{
                                        $propertys[$k] = $item;
                                }
                        }

                }else{
                        foreach($arResult['PROPERTIES'] as $k=>$item){
                                if(strlen($k)!=2){
                                        $propertys[$k] = $item;
                                }
                        }

                }
        }else{
                if($sys == 'общая'){
                        foreach($arResult['PROPERTIES'] as $k=>$item){
                                if(strlen($k)==2){
                                        if((substr($k, 0, 1)=='D')||(substr($k, 0, 1)=='F')){
                                                $propertys[$k] = $item;
                                        }
                                }else{
                                        $propertys[$k] = $item;
                                }
                        }

                }elseif($sys == 'упрощенная'){
                        foreach($arResult['PROPERTIES'] as $k=>$item){
                                if(strlen($k)==2){
                                        if((substr($k, 0, 1)=='E')||(substr($k, 0, 1)=='F')){
                                                $propertys[$k] = $item;
                                        }
                                }else{
                                        $propertys[$k] = $item;
                                }
                        }
                }else{
                        foreach($arResult['PROPERTIES'] as $k=>$item){
                                if(strlen($k)!=2){
                                        $propertys[$k] = $item;
                                }
                        }
                }
        }
        $arResult['PROPERTIES'] = $propertys;
        ?>

                        <div class="wrapper">
                                <div class="garantii">
                                        <table class="width_100 float_left">
                                        <?foreach($arResult['PROPERTIES'] as $item){?>
                                                <tr>
                                                        <?if ($item['PROPERTY_TYPE']=='F'){?>
                                                                <td>
                                                                        <?=$item['NAME']?>
                                                                        <?
                                                                        $rsfile = CFile::GetByID($item['VALUE']);
                                                                        $arfile = $rsfile->Fetch();
                                                                        $url = CFile::GetPath($item['VALUE']);
                                                                        ?>
                                                                </td><td>                
                                                                        <?if(!empty($url)){?>
                                                                                <a href="/include/upload.php?ID=<?=$item['VALUE']?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                                                        <?}else{?>
                                                                                Нет файла
                                                                        <?}?>
                                                                </td>
                                                        <?}else{?>
                                                                <td>
                                                                        <?=$item['NAME']?>
                                                                </td>
                                                                <td>
                                                                        <?=$item['VALUE']?>
                                                                </td>
                                                        <?}?>
                                                </tr>
                                        <?}?>
                                        </table>
                                        
                                        <?global $USER;
                                        if(in_array(6, CUser::GetUserGroup($USER->GetID()))){?>
                                                <?$status=array(
                                                        17 => 'Согласование условий',
                                                        18 => 'Ожидается оплата',
                                                        19 => 'БГ выдана',
                                                );?>
                                                <div class="error-text"></div>
                                                <form method="POST" action='/include/status_BG.php' class='on_ajax status_BG' enctype="multipart/form-data">
                                                        <input type='hidden' name='ID' value="<?=$arResult['ID']?>">
                                                        <div class="element_anketa">
                                                                Изменить статус:
                                                                <div class="new-select-style-wpandyou-anceta">
                                                                        <select name="STATUS" class="main_element_form">
                                                                                <?foreach($status as $k=>$item){?>
                                                                                <option value="<?=$k?>"<?if($arResult['PROPERTIES']['STATUS']['VALUE']==$item){?> selected<?}?>><?=$item?></option>
                                                                                <?}?>
                                                                        </select>
                                                                </div>                                                        
                                                        </div>  
														
														
                                                        <div class="element_anketa">
                                                                <button>Сохранить</button>
                                                        </div>  
                                                </form>
                                        <?}?>

                                        <a href="/zayavki/">К заявкам</a>
                                </div>
                        </div>
<?}?>