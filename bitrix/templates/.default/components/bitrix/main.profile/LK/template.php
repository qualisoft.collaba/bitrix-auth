<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>

<?if(!empty($arResult["strProfileError"])){?>
        <div class='message-type-error'>
                <?ShowError($arResult["strProfileError"]);?>                
        </div>        
<?}elseif ($arResult['DATA_SAVED'] == 'Y'){?>
        <div class='message-type-ok'>
                <?ShowNote(GetMessage('PROFILE_DATA_SAVED'));?>
        </div>                        
<?}?>




<?if(!empty($_POST)){
        $arResult["arUser"] = $_POST;
}?>
<h1>������������ ������</h1>

<form class="personal-data" method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data" novalidate="">
        <?=$arResult["BX_SESSION_CHECK"]?>
        <input type="hidden" name="lang" value="<?=LANG?>" />
        <input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
        <input type="hidden" name="TITLE" value="<?=$arResult["arUser"]["TITLE"]?>" />
        <input type="hidden" name="LOGIN" value="<?=$arResult["arUser"]["LOGIN"]?>" />

<!--tabs-->
        <dl class="tabs" style="height: 585px;">
            <dt class="active">
                <span>������������ ������</span>
            </dt>

            <dd style="display: block;">
                <!--personal-data__wrap-->
                <div class="personal-data__wrap">

                        <fieldset class="input__layout">
                                <label for="name">���*:</label>
                                <input class="required" type="text" name='NAME' id="name" value="<?=$arResult["arUser"]["NAME"]?>">
                        </fieldset>
                        <fieldset class="input__layout">
                                <label for="last_name">�������*:</label>
                                <input class="required" type="text" name='LAST_NAME' id="last_name" value="<?=$arResult["arUser"]["LAST_NAME"]?>">
                        </fieldset>
                        <fieldset class="input__layout">
                                <label for="paternal">��������:</label>
                                <input type="text" id="paternal" name='SECOND_NAME' value="<?=$arResult["arUser"]["SECOND_NAME"]?>">
                        </fieldset>

                        <fieldset class="input__layout">
                                <label for="date">���� ��������:</label>
                                <input type="text" name="PERSONAL_BIRTHDAY" class="datepicker" id="date" value="<?=$arResult["arUser"]["PERSONAL_BIRTHDAY"]?>"/>
                        </fieldset>
                        
                        <?
                        $date_need = time()-(2049840000+315360000);
                        $date_need_format = date('Y, m, d',$date_need);
                        $date_need_max_format = date('Y, m, d',time());
                        ?>
                        <script>
                                $(document).ready(function() {
                                        $( "input[name=PERSONAL_BIRTHDAY]" ).datepicker("option", "minDate", new Date(<?=$date_need_format?>));
                                        $( "input[name=PERSONAL_BIRTHDAY]" ).datepicker("option", "maxDate", new Date(<?=$date_need_max_format?>));
                                });
                        </script>


                        <fieldset class="input__layout">
                                <label for="sex">���:</label>
                                <select name="PERSONAL_GENDER" id="sex" style="opacity: 0;" >

                                        <option value="">��������</option>
                                        <option value="M"<?if($arResult['arUser']['PERSONAL_GENDER']=='M'){?> selected="SELECTED"<?}?>>�������</option>
                                        <option value="F"<?if($arResult['arUser']['PERSONAL_GENDER']=='F'){?> selected="SELECTED"<?}?>>�������</option>

                                </select>
                        </fieldset>

                        <fieldset class="input__layout">
                            <label for="phone">��������� �������*:</label>
                            <input class="required mobile_mask" type="text" name="PERSONAL_MOBILE" id="phone" value="<?=$arResult["arUser"]["PERSONAL_MOBILE"]?>" placeholder="+7(___) ___ __ __">
                        </fieldset>

                        <fieldset class="input__layout">
                            <label for="email">E-mail*:</label>
                            <input class="required" type="email" name="EMAIL" id="email" value="<?=$arResult["arUser"]["EMAIL"]?>">
                        </fieldset>

                        <fieldset class="input__layout">
                            <label for="password">������*:</label>
                            <input class="" type="password" name="NEW_PASSWORD" id="password" autocomplete="off" placeholder="*********">
                        </fieldset>

                        <fieldset class="input__layout">
                            <label for="password_confirmation">������������� ������*:</label>
                            <input class="" type="password" name="NEW_PASSWORD_CONFIRM" id="password_confirmation" autocomplete="off" placeholder="*********">
                        </fieldset>
<?
                CModule::IncludeModule("subscribe");
                        $subscr = CSubscription::GetList(
                            array("ID"=>"ASC"),
                            array("CONFIRMED"=>"Y", "ACTIVE"=>"Y", "EMAIL"=>$arResult["arUser"]["EMAIL"])
                        );
                        if(($subscr_arr = $subscr->Fetch())){
                                $res = $subscr_arr['ID'];
                        }
                        
                $arOrder = Array("SORT"=>"ASC", "NAME"=>"ASC"); 
                $arFilter = Array("ACTIVE"=>"Y", "LID"=>LANG); 
                $rsRubric = CRubric::GetList($arOrder, $arFilter); 
                $arRubrics = array(); 
                while($arRubric = $rsRubric->GetNext()){ 
                        $Rubric[] = $arRubric; 
                }
                        //preprint($subscr_list)
                        
?>                        

                        <div class="subscribe">
                                <input type="checkbox" id="subscribe" name="subscribe" <?if($res>0){echo 'checked';}?>>
                            <label for="subscribe"><?=$Rubric[0]['NAME']?></label>
                        </div>
                        <input type="submit" name="save"  class="submit-nice" value="���������">

                        <span>*����������� ��� ����������</span>

                </div>
                <!--/personal-data__wrap-->
            </dd>


            <a href="/personal/order/"><span>��� ������</span></a>

        </dl>
<!--/tabs-->

</form>
