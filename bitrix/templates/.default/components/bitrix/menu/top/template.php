<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$userID = $USER->GetID();
if (!empty($arResult) && ($APPLICATION->GetCurDir() != '/zayavki/signature/')):?>
<ul>

<?
foreach($arResult as $arItem):
    if(in_array($userID, array(19, 136))):
		if(($arItem["TEXT"] != "Подать заявку")&&($arItem["TEXT"] != "Контрагенты")):
			if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue;?>
			<?if($arItem["SELECTED"]):?>
				<li><a href="<?=$arItem["LINK"]?>" class="active"><?=$arItem["TEXT"]?></a></li>
			<?else:?>
				<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
			<?endif?>	
		<?endif?>
	<?else:?>
	    <?if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue;?>
		<?if($arItem["SELECTED"]):?>
			<li><a href="<?=$arItem["LINK"]?>" class="active"><?=$arItem["TEXT"]?></a></li>
		<?else:?>
			<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
		<?endif?>
	<?endif?>
<?endforeach?>

</ul>
<?endif?>