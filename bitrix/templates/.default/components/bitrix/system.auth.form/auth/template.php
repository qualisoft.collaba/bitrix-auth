<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CJSCore::Init();
?>

<h2 class="il-main-btn" id="il_sel_login"><?=GetMessage("VHOD_DLA");?><i class="fa fa-chevron-right faVetal1 perevorot" aria-hidden="true"></i></h2>
<div class="il-line-login il-window il-show">

<?
if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
	ShowMessage($arResult['ERROR_MESSAGE']);
?>

<?if($arResult["FORM_TYPE"] == "login"):?>

<form name="system_auth_form<?=$arResult["RND"]?> b-form _auth form" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
<div class="il-line-login il-window il-show">
<?if($arResult["BACKURL"] <> ''):?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?endif?>
<?foreach ($arResult["POST"] as $key => $value):?>
	<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
<?endforeach?>

	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="AUTH" />

		<div class="form-group">
			<input class="form-control" data-val="true" data-val-required="Требуется поле Login." id="il_login_name" name="USER_LOGIN" placeholder="<?=GetMessage("AUTH_LOGIN")?>" type="text" value="">
		</div>
		<div class="form-group">
			<input class="form-control" data-val="true" data-val-required="Требуется поле Password." id="il_login_pass" name="USER_PASSWORD" placeholder="<?=GetMessage("AUTH_PASSWORD")?>" type="password">
		</div>
		<div class="form-group blockVetal">
			<a class="hrefVetal" id="recover" href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow" target="_blank"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
			<input type="submit" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" class="ill-login__btn" id="loginbutton" name="Login">
		</div>
		


</form>
</div>



<?endif?>
</div>
