<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

?>

<?if(count($arResult["ITEMS"])>0){?>
        <div class="wrapper_tow_head" style="    padding-top: 80px;">
                
                <table class='width_100'>
                        <tr>
                                <th>Документ</th>
                                <th>Образец</th>
                        </tr>
                        
                        <?foreach($arResult["ITEMS"] as $arItem):?>
                                <?
                                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                                $url = CFile::GetPath($arItem['PROPERTIES']['DOCUMENT']['VALUE'])
                                ?>                               
                                <tr>
                                        <td><?=$arItem['NAME']?></td>
                                        <td><?if(!empty($url)){?><a href="<?=$url?>">Скачать</a><?}?></td>
                                </tr>
                        <?endforeach;?>
                </table>
                <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
                        <br /><?=$arResult["NAV_STRING"]?>
                <?endif;?>
        </div>
<?}?>

