<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
global $arrFilter, $USER;
//echo "<pre>"; print_r($arResult); echo "</pre>";
?>

<?if(count($arResult["ITEMS"])>0){?>
<?if(!$arrFilter['NO_WRAP']){?>
        <div class="wrapper_tow_head">
<?}?>
<p class="zayavka-status"><?=$_SESSION["ZAYAVKA_STATUS"]?></p>
<?
$_SESSION["ZAYAVKA_STATUS"] = "";
?>           
<?
             $head = array(
                 'ID' => 'Код заявки',
                 'PROPERTY_L' => 'Клиент (ИНН)',
                 //'CREATED_USER_NAME' => 'Агент',
                 'DATE_CREATE' => 'Дата заявки',
                 'PROPERTY_STATUS' => 'Статус заявки',
                 //'PROPERTY_DATE_STATUS' => 'Изменение статуса',
                 'PROPERTY_D2' => 'Срок БГ (дней)',
                 'PROPERTY_B' => 'Размер обеспечения',                 
             );
             ?>   
                <table class='width_100'>
                        <tr>
                                <?foreach($head as $k=>$item){?>
                                        <th>
                                                <a href="#" 
                                                   <?if($_SESSION['SORT'] == $k){?>
                                                        <?if($_SESSION['OR'] == 'ASC'){?>
                                                                or="DESC" 
                                                        <?}else{?>
                                                                or="ASC"
                                                        <?}?>
                                                        class="sort_me active" 
                                                   <?}else{?>
                                                        or="DESC" 
                                                        class="sort_me"                                                         
                                                   <?}?>
                                                   rel="<?=$k?>">
                                                           <?=$item?>
                                                </a>
                                        </th>                                
                                <?}?>
                                <?/*<th><a href="#" class="sort_me<?if($_SESSION['SORT'] == 'PROPERTY_A'){?> active<?}?>" rel="PROPERTY_A">Код заявки</a></th>
                                <th><a href="#" class="sort_me<?if($_SESSION['SORT'] == 'PROPERTY_M'){?> active<?}?>" rel="PROPERTY_M">ИНН принципала</a></th>
                                <th><a href="#" class="sort_me<?if($_SESSION['SORT'] == 'CODE'){?> active<?}?>" rel="CODE">Дата заявки</a></th>
                                <th><a href="#" class="sort_me<?if($_SESSION['SORT'] == 'PROPERTY_STATUS'){?> active<?}?>" rel="PROPERTY_STATUS">Статус заявки</a></th>
                                <th><a href="#" class="sort_me<?if($_SESSION['SORT'] == 'PROPERTY_DATE_STATUS'){?> active<?}?>" rel="PROPERTY_DATE_STATUS">Изменение статуса</a></th>
                                <th><a href="#" class="sort_me<?if($_SESSION['SORT'] == 'PROPERTY_C'){?> active<?}?>" rel="PROPERTY_C">Актуально до</a></th>
                                <th><a href="#" class="sort_me<?if($_SESSION['SORT'] == 'PROPERTY_B'){?> active<?}?>" rel="PROPERTY_B">Размер обеспечения, руб</a></th>*/?>
                                <!-- <th>Срок БГ, дней</th>
                                
                                <th></th>-->
                        </tr>
                        
                        <?foreach($arResult["ITEMS"] as $arItem):?>
                                <?
                                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                                ?>
                                <?$arItem['DAY'] = round((strtotime($arItem['PROPERTIES']['D2']['VALUE'])-strtotime($arItem['PROPERTIES']['D1']['VALUE']))/(3600*24));
                                if($arItem['PROPERTIES']['GARANTE_VIDACHI']['VALUE']=='Да')
                                {   
                                    if ( ( new DateTime($arItem['DATE_CREATE'])) < ( new DateTime($arItem['PROPERTIES']['D2']['VALUE']))){
                                      $days = date_diff(new DateTime($arItem['DATE_CREATE']), new DateTime($arItem['PROPERTIES']['D2']['VALUE']))->d;
                                    }
                                }
                                else{
                                  $days =0;  
                                 //   $days = date_diff(new DateTime($arItem['PROPERTIES']['D1']['VALUE']), new DateTime($arItem['PROPERTIES']['D2']['VALUE']))->d;
                                }    
                                ?>
                                <tr>
                                   <td><a href='/zayavki/detail.php?id=<?=$arItem['ID']?>&edit=Y&CODE=<?=$arItem['ID']?>'><?=$arItem['ID']?></a>
                                        </td>
                                        <td><?=$arItem['PROPERTIES']['L']['VALUE']?><?if(!empty($arItem['PROPERTIES']['M']['VALUE'])){?> (<?=$arItem['PROPERTIES']['M']['VALUE']?>)<?}?></td>
                                        <?/*<td><?=$arItem['CREATED_USER_NAME']?></td> */?>
                                        <td><?=date('d.m.Y H:i',  strtotime($arItem['DATE_CREATE']))?></td>
                                        <td><?=$arItem['PROPERTIES']['STATUS']['VALUE']?></td>
                                        <!--<td><?=$arItem['PROPERTIES']['DATE_STATUS']['VALUE']?></td>-->
                                        <td><!--<?if($days>0 && !empty($arItem['PROPERTIES']['D2']['VALUE'])){?>
                                        <?// echo ' по '.$arItem['PROPERTIES']['D2']['VALUE'].'('.($days+1).')';?>
                                            <?=$arItem['PROPERTIES']['GARANTE_VIDACHI']['VALUE']=='Да'
    ?' по '.$arItem['PROPERTIES']['D2']['VALUE'].'('.($days+1).')'
    :empty($arItem['PROPERTIES']['D1']['VALUE'])?' по '.$arItem['PROPERTIES']['D2']['VALUE'].' ('.($days+1).')':'c '.$arItem['PROPERTIES']['D1']['VALUE'].' по '.$arItem['PROPERTIES']['D2']['VALUE'].' ('.($days+1).')'?>
    

                                        
                                             <?}?>-->
											 <?if(!empty($arItem['PROPERTIES']['D2']['VALUE'])){?>
											 по <?=$arItem['PROPERTIES']['D2']['VALUE']?> (<?echo ($days+1)?>)
											 <?}?>
                                        </td>
                                        <td><?=$arItem['PROPERTIES']['CURRENCY']['VALUE']." ".num($arItem['PROPERTIES']['B']['VALUE'])?></td>
										
                                </tr>
                        <?endforeach;?>
                </table>
                <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
                        <br /><?=$arResult["NAV_STRING"]?>
                <?endif;?>
        </div>
<?}?>

