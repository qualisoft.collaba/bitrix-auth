 $(".il-main-btn").click(function () {
        if ($(this).attr('id') == 'il_sel_login') {

            if ($(".il-line-login").hasClass("il-show") == false) {
                $(".il-line-login").addClass("il-show");
                $(".faVetal1").addClass("perevorot");
                $(".il-line-new-user").removeClass("il-show");
                $("#il_li").css("display", "none");
            } else {
                $(".il-line-login").removeClass("il-show");
                $(".faVetal1").removeClass("perevorot");
            }

        }
        else if ($(this).attr('id') == 'il_sel_registr') {
//            alert($(this).attr('id'));

            if ($(".il-line-new-user").hasClass("il-show") == false) {
                $(".il-line-new-user").addClass("il-show");
                $(".faVetal2").addClass("perevorot");
                $(".il-line-login").removeClass("il-show");
                $("#il_li").css("display", "none");

                var radioValue = $('input[name=optionsRadios]:checked').val();
                if (radioValue == 1 ) {
                    checkUl();
                } else if (radioValue == 2) {
                    checkIp();
                } else if (radioValue == 3) {
                    checkFl();
                }
            } else {
                $(".il-line-new-user").removeClass("il-show");
                $(".faVetal2").removeClass("perevorot");
            }

        }
    })

    $('input[name=optionsRadios]').change(function() {
        if (this.value == 1) {
            checkUl();
        } else if (this.value == 2) {
            checkIp();
        } else if (this.value == 3) {
            checkFl();
        }
    });

    function checkUl () {
        $("#il_li").css("display", "block");

        $("#il_ogpnip").css("display", "none");
        $("#il_ogpnip input").removeAttr("required");

        $("#il_poisk").css("display", "block");
        $("#il_poisk input").attr("required");
        $("#il_kpp").css("display", "block");
        $("#il_kpp input").attr("required");
        $("#il_ogpn").css("display", "block");
        $("#il_ogpn input").attr("required");
        $("#il_naimenovanie").css("display", "block");
        $("#il_naimenovanie input").attr("required");
    }
    function checkIp() {
        $("#il_li").css("display", "block");

        $("#il_kpp").css("display", "none");
        $("#il_kpp input").removeAttr("required");
        $("#il_ogpn").css("display", "none");
        $("#il_ogpn input").removeAttr("required");

        $("#il_poisk").css("display", "block");
        $("#il_poisk input").attr("required");
        $("#il_ogpnip").css("display", "block");
        $("#il_ogpnip input").attr("required");
        $("#il_naimenovanie").css("display", "block");
        $("#il_naimenovanie input").attr("required");
    }
    function checkFl() {
        $("#il_li").css("display", "block");

        $("#il_kpp").css("display", "none");
        $("#il_kpp input").removeAttr("required");
        $("#il_ogpn").css("display", "none");
        $("#il_ogpn input").removeAttr("required");
        $("#il_ogpnip").css("display", "none");
        $("#il_ogpnip input").removeAttr("required");
        $("#il_naimenovanie").css("display", "none");
        $("#il_naimenovanie input").removeAttr("required");
        $("#il_poisk").css("display", "none");
        $("#il_poisk input").removeAttr("required");
    }