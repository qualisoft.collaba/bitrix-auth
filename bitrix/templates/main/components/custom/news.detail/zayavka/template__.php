<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

?>

<span id="operation" data="false"></span>
<?if($arResult['PROPERTIES']['STATUS']['VALUE'] == 'Черновик'){?>
<script type="text/javascript">
$( document ).ready(function() {
	
    	$('#GARANTE1').click(function(){
        		$('#add_new_bg').find('input[name=D1]').prop( "disabled", true );
        		$('#add_new_bg').find('input[name=D1]').css("display", "none");
        	});
    	$('#GARANTE2').click(function(){
    		$('#GARANTE1').prop('checked', false);
    			$('#add_new_bg').find('input[name=D1]').prop( "disabled", false );
    			$('#add_new_bg').find('input[name=D1]').css("display", "initial");
    	});
    });

function comgos(a){
    console.log(a);
    if(a === 1){
        $("#notice").val("Номер извещения");
        $(".notice_box").css("display", "none");
    }else{
        $("#notice").val("");
        $(".notice_box").css("display", "block");
    }
}
</script>
                <div class="wrapper">
                        <form method="POST" action='/include/add_BG.php' id="add_new_bg" class='on_ajax add_element' enctype="multipart/form-data">
                                <div class="garantii">
                                        <input type='hidden' name='ID' value='<?=$arResult['ID']?>'>
                                        <?  foreach ($arResult['PROPERTIES'] as $k=>$item){
                                                if(!empty($item['VALUE'])){
                                                        if(strlen($k) == 2){?>
                                                                <input type='hidden' name='<?=$k?>_k' value='1'>
                                                        <?}
                                                }
                                        }?>
                                        <h1>Заявка № <?=$arResult["ID"]?></h1>
                                        <span class="title_table">Условия гарантии</span>
                                        
                                        <div class="element_anketa">
                                            Тип гарантии
                                            <input class="required" type="radio" onclick="comgos(1)" <?=$arResult['PROPERTIES']['I']['VALUE']=='Коммерческая'?'checked="checked"':''?>  id="avans141" name="I" value="7">
                                            <label for="avans141">Коммерческая</label>
                                            <input class="required" type="radio" onclick="comgos(2)" <?=$arResult['PROPERTIES']['I']['VALUE']=='Госзакупка'?'checked="checked"':''?> id="avans142" name="I" value="8">
                                            <label for="avans142">Госзакупка</label>
                                        </div>
                                        <div class="element_anketa notice_box" style="<?=$arResult['PROPERTIES']['I']['VALUE']=='Коммерческая'?'display:none':''?>">
                                                <input type="text" name='A' placeholder='Номер извещения/закупки' value='<?=empty($arResult['PROPERTIES']['A']['VALUE'])?'&nbsp':$arResult['PROPERTIES']['A']['VALUE']?>'>
                                        </div>
                                        <div class="element_anketa">
                                                <input class="main_element_form" type="text" name='B' placeholder='Сумма БГ, руб' value='<?=num($arResult['PROPERTIES']['B']['VALUE'])?>'>
                                                <div class="new-select-style-wpandyou-anceta currency">
                                                        <select name="CURRENCY" class="main_element_form">
                                                                <option value="0" >руб.</option>
                                                                <option <?if($arResult['PROPERTIES']['CURRENCY']['VALUE_ENUM_ID']==24){?> selected<?}?> value="24">$</option>
                                                                <option <?if($arResult['PROPERTIES']['CURRENCY']['VALUE_ENUM_ID']==25){?> selected<?}?> value="25">€</option>
                                                        </select>
                                                </div>
                                        </div>
                                        <!-- <div class="element_anketa">
                                                <input type="text" name='C' placeholder='Дата опубликования протокола победителей' class="datepicker" autocomplete="false"  value='<?=$arResult['PROPERTIES']['C']['VALUE']?>'>
                                        </div>-->
                                        <div class="element_anketa">
                                        		Срок гарантии с момента выдачи 
                                        		<input class="required" type="radio" id="GARANTE1" <?if($arResult['PROPERTIES']['GARANTE_VIDACHI']['VALUE_ENUM_ID']==26){?> checked<?}?> name="GARANTE_VIDACHI" value="26">
                                                <label for="avans11">Да</label>
                                                <input class="required" type="radio" id="GARANTE2" <?if($arResult['PROPERTIES']['GARANTE_VIDACHI']['VALUE_ENUM_ID']==27){?> checked<?}?> name="GARANTE_VIDACHI" value="27">
                                                <label for="avans12">Нет</label>
                                        </div>
                                        <div class="element_anketa">
                                                Срок действия банковской гарантии <input id="date_from" type="text" name='D1' placeholder='с' style="<?if($arResult['PROPERTIES']['GARANTE_VIDACHI']['VALUE_ENUM_ID']==26){?> display:none<?}?>" class="date_time datepicker" autocomplete="false"  value='<?=$arResult['PROPERTIES']['GARANTE_VIDACHI']['VALUE_ENUM_ID']==26?$arResult['DATE_CREATE']:$arResult['PROPERTIES']['D1']['VALUE']?>'> <input type="text" id="date_to" name='D2' placeholder='по' class="date_time datepicker" autocomplete="false"  value='<?=$arResult['PROPERTIES']['D2']['VALUE']?>'> <span class="error text"></span>
                                        </div>
                                        <div class="element_anketa">
                                                Аванс
                                                <input class="required" type="radio" id="avans11" name="E" value="1"<?if($arResult['PROPERTIES']['E']['VALUE']=='Да'){?> checked<?}?>>
                                                <label for="avans11">Да</label>
                                                <input class="required" type="radio" id="avans12" name="E" value="2"<?if($arResult['PROPERTIES']['E']['VALUE']=='Нет'){?> checked<?}?>>
                                                <label for="avans12">Нет</label>
                                        </div>
                                        <!--  <div class="element_anketa">
                                                Бесспорное списание по БГ 
                                                <input class="required" type="radio" id="avans21" name="F" value="3"<?if($arResult['PROPERTIES']['F']['VALUE']==3){?> checked<?}?>>
                                                <label for="avans21">Да</label>
                                                <input class="required" type="radio" id="avans22" name="F" value="4"<?if($arResult['PROPERTIES']['F']['VALUE']==4){?> checked<?}?>>
                                                <label for="avans22">Нет</label>
                                        </div>-->
                                        <!-- <div class="element_anketa">
                                                Снижение на более 25% от начальной цены
                                                <input class="required" type="radio" id="avans31" name="G" value="5"<?if($arResult['PROPERTIES']['G']['VALUE']==5){?> checked<?}?>>
                                                <label for="avans31">Да</label>
                                                <input class="required" type="radio" id="avans32" name="G" value="6"<?if($arResult['PROPERTIES']['G']['VALUE']==6){?> checked<?}?>>
                                                <label for="avans32">Нет</label>
                                        </div>  -->
                                        <div class="element_anketa">
                                                <textarea placeholder='Комментарий' cols="100" rows="15" name='H'><?=$arResult['PROPERTIES']['H']['VALUE']["TEXT"]?></textarea>
                                        </div>       
                                       <!--  <div class="element_anketa">
                                                Требования к БГ
                                                <input class="required" type="radio" id="avans141" name="I" value="7"<?if($arResult['PROPERTIES']['I']['VALUE']==7){?> checked<?}?>>
                                                <label for="avans141">Да</label>
                                                <input class="required" type="radio" id="avans142" name="I" value="8"<?if($arResult['PROPERTIES']['I']['VALUE']==8){?> checked<?}?>>
                                                <label for="avans142">Нет</label>
                                        </div>       
                                        <div class="element_anketa">
                                                Система налогооблажения клиента
                                                <div class="new-select-style-wpandyou-anceta">
                                                        <select name="J" class="main_element_form">
                                                                <option value=""<?if($arResult['PROPERTIES']['J']['VALUE']==''){?> selected<?}?>>Тип налогооблажения</option>
                                                                <option value="9"<?if($arResult['PROPERTIES']['J']['VALUE']=='общая'){?> selected<?}?>>Общая</option>
                                                                <option value="10"<?if($arResult['PROPERTIES']['J']['VALUE']=='упрощенная'){?> selected<?}?>>Упрощенная</option>
                                                        </select>
                                                </div>
                                        </div>  -->  
                                </div>                                  
                                <div class="anketa">
                                        <span class="title_table">Анкета</span>
                                        <!--  <div class="element_anketa">
                                                <input type="text" name='K' placeholder='Полное наименование ЮЛ' value='<?=$arResult['PROPERTIES']['K']['VALUE']?>'>
                                        </div>-->
                                        <div class="element_anketa">
                                                <input type="text" name='L' placeholder='Наименвоание принципала' value='<?=$arResult['PROPERTIES']['L']['VALUE']?>'>
                                        </div>
                                        <div class="element_anketa">
                                                <input type="text" name='M' placeholder='ИНН принципала' value='<?=$arResult['PROPERTIES']['M']['VALUE']?>'>
                                        </div>
                                        <div class="element_anketa">
                                                Шаблон заказчика
                                                <input class="required" type="radio" id="avans41" name="N" value="11"<?if($arResult['PROPERTIES']['N']['VALUE']=='Да'){?> checked<?}?>>
                                                <label for="avans41">Да</label>
                                                <input class="required" type="radio" id="avans42" name="N" value="12"<?if($arResult['PROPERTIES']['N']['VALUE']=='Нет'){?> checked<?}?>>
                                                <label for="avans42">Нет</label>

                                        </div>
                                       <!-- <div class="element_anketa">
                                                Срочная отправка оригинала экспесс почтой
                                                <input class="required" type="radio" id="avans51" name="O" value="13"<?if($arResult['PROPERTIES']['O']['VALUE']=='13'){?> selected<?}?>>
                                                <label for="avans51">Да</label>
                                                <input class="required" type="radio" id="avans52" name="O" value="14"<?if($arResult['PROPERTIES']['O']['VALUE']=='14'){?> selected<?}?>>
                                                <label for="avans52">Нет</label>

                                        </div>-->
                                        <!--<div class="element_anketa">
                                                <input type="text" name='P' placeholder='Заказчик по договору' value='<?=$arResult['PROPERTIES']['P']['VALUE']?>'>                                
                                        </div>
                                        <div class="element_anketa">
                                                <input type="text" name='Q' placeholder='ИНН Заказчика' value='<?=$arResult['PROPERTIES']['Q']['VALUE']?>'>                                
                                        </div>-->
                                        
                                        <div class="element_anketa">
                                                <textarea placeholder='Смена статусов' disabled cols="100" rows="15"><?=$arResult['COMMENT']?></textarea>
                                        </div>
                                </div>
                                <div class="uslugi">

<h4>Документы для рассмотрения юридического лица Принципала/Залогодателя/Поручителя</h4>

<div class="horizont_line"></div>
<div class="element_anketa">
        Заявление на предоставление банковской гарантии, <a href="/zayavki/document_list/">образец</a>
        <div class="file_button">
                <input type="file" name='AE'>
        </div>
     <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['AE']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" id="file_AE" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?> 

</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Устав с отметкой ФНС со всеми действующими изменениями в Устав с отметкой ФНС (редакция, действующая на дату проведения юридической экспертизы)
        <div class="file_button">
                <input type="file" name='USTAV_S_OTMETKOY_FNS'>
        </div> 
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['USTAV_S_OTMETKOY_FNS']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" id="file_USTAV_S_OTMETKOY_FNS" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>     
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Протокол об избрании/назначении единоличного исполнительного органа (Генерального директора/Директора/др.) и Главного бухгалтера
        <div class="file_button">
                <input type="file" name='PROTOKOL_BUH_GLAV'>
        </div> 
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['PROTOKOL_BUH_GLAV']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" id="file_PROTOKOL_BUH_GLAV" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>     
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Паспорта всех Учредителей, Генерального директора и Главного бухгалтера
        <div class="file_button">
                <input type="file" name='ALL_PASPORT'>
        </div>  
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['ALL_PASPORT']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" id="file_ALL_PASPORT" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>    
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Свидетельство о постановке на учет в налоговом органе по месту нахождения организации (ИНН)
        <div class="file_button">
                <input type="file" name='FB'>
        </div>   
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['FB']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" id="file_FB" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>   
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Лицензии, сертификаты, разрешения на ведение деятельности (в случаях, когда их наличие обязательно)
        <div class="file_button">
                <input type="file" class="non_requared" name='LICENSE'>
        </div>  
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['LICENSE']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>    
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Документы, подтверждающие наличие по местонахождению юридического лица, его постоянно действующего органа управления (свидетельство о праве собственности, договор аренды, договор субаренды и др.)
        <div class="file_button">
                <input type="file" name='REGISTER_URL_LICO'>
        </div> 
<?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['REGISTER_URL_LICO']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" id="file_REGISTER_URL_LICO" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>    
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Протокол общего собрания участников общества  об одобрении крупной сделки.
        <div class="file_button">
                <input type="file" class="non_requared" name='PROTOKOL_BIG_SUMM'>
        </div> 
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['PROTOKOL_BIG_SUMM']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>     
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Проект контракта 
        <div class="file_button">
                <input type="file" name='AH'>
        </div>  
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['AH']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" id="file_AH" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>    
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Согласия юридического и физического лиц на запрос и предоставление информации в Бюро Кредитных Историй для всех учредителей и директора, <a href="/zayavki/document_list/">образец</a>
        <div class="file_button">
                <input type="file" name='SOGLASIE_V_BURO'>
        </div>  
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['SOGLASIE_V_BURO']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" id="file_SOGLASIE_V_BURO" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>    
</div>


<div class="horizont_line"></div>
<h4>Финансовые документы (предоставляются только Принципалом)</h4> 
<div class="horizont_line"></div>
<div class="element_anketa">
        Баланс и отчет о финансовых результатах предприятия за последние 5 отчетных периодов. 
        <div class="file_button">
                <input type="file" name='FIZ_BALANCE'>
        </div>    
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['FIZ_BALANCE']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" id="file_FIZ_BALANCE" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>  
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Годовая декларация по налогу на прибыль предприятия за последний завершенный финансовый год с отметкой ФНС о принятии непосредственно на отчетности или с приложением документа, подтверждающего факт передачи отчетности в налоговой орган /если не применяется режим ЕНВД или УСН/
        <div class="file_button">
                <input type="file" name='FIZ_GOD_DEKL'>
        </div>  
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['FIZ_GOD_DEKL']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" id="file_FIZ_GOD_DEKL" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>    
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Налоговые декларации за последний календарный год, с отметкой ФНС о принятии непосредственно на отчетности или с приложением документа, подтверждающего факт передачи отчетности в налоговой орган /если компания уплачивает ЕНВД или применяет УСН/
        <div class="file_button">
                <input type="file" name='FIZ_NALOG_DEKL'>
        </div> 
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['FIZ_NALOG_DEKL']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" id="file_FIZ_NALOG_DEKL" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>     
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Информационное письмо, содержащее сведения: об открытых расчетных счетах  в других кредитных организациях; об оборотах по указанным счетам за последние 12 месяцев; об отсутствии/наличии картотеки №2; об отсутствии/наличии ссудной задолженности за последние 12 месяцев, <a href="/zayavki/document_list/">образец</a>
        <div class="file_button">
                <input type="file" name='FIZ_SCHET_INFO'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['FIZ_SCHET_INFO']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" id="file_FIZ_SCHET_INFO" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>      
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Карточка 51 счета за последний отчетный период  в разрезе операций.
        <div class="file_button">
                <input type="file" name='FIZ_51_CARD'>
        </div> 
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['FIZ_51_CARD']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" id="file_FIZ_51_CARD" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>     
</div>

<div class="horizont_line"></div>
<div class="element_anketa">
        Отчет о дебиторской и кредиторской задолженности на последнюю отчетную дату по форме банка, образец стр. 7 
        <div class="file_button">
                <input type="file" name='FIZ_OTCHET_DEB'>
        </div>  
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['FIZ_OTCHET_DEB']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" id="file_FIZ_OTCHET_DEB" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>    
</div>



<div class="horizont_line"></div>
<div class="element_anketa">
        Оборотно-сальдовые ведомости в разрезе операций за последний отчетный период следующих строк баланса: финансовые вложения (1170), запасы (1210), дебиторская задолженность (1230), финансовые вложения (1240),  долгосрочные займы (1410), краткосрочные займы (1510), кредиторская задолженность (1520)  и других строк баланса превышающих 10% от валюты баланса.
        <div class="file_button">
                <input type="file" name='FIZ_SALD'>
        </div>   
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['FIZ_SALD']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" id="file_FIZ_SALD" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>   
</div>


<div class="horizont_line"></div>
                                </div>
                                
                                <div class="error-text">

                                </div>
                                <div class="element_anketa">
                                       <button name='DELETE' onclick="$('#operation').attr('data','del')" style="background-color: #ff9595" value='Y'>Удалить!</button> <button onclick="$('#operation').attr('data','save')" name='STATUS' value='15'>Сохранить в черновик</button> <button onclick="$('#operation').attr('data','send')" name='STATUS' value='16' class="real">Передать в банк</button> 
                                </div>  
                        </form>
                </div>
        
<?}else{
/*
        $bg = $arResult['PROPERTIES']['B']['VALUE'];
        $arResult['PROPERTIES']['B']['VALUE'] = num($arResult['PROPERTIES']['B']['VALUE']);
        $sys = $arResult['PROPERTIES']['J']['VALUE'];
        if($bg<10000000){
                if($sys == 'общая'){
                        foreach($arResult['PROPERTIES'] as $k=>$item){
                                if(strlen($k)==2){
                                        if((substr($k, 0, 1)=='A')||(substr($k, 0, 1)=='C')){
                                                $propertys[$k] = $item;
                                        }
                                }else{
                                        $propertys[$k] = $item;
                                }
                        }
                }elseif($sys == 'упрощенная'){
                        foreach($arResult['PROPERTIES'] as $k=>$item){
                                if(strlen($k)==2){
                                        if((substr($k, 0, 1)=='B')||(substr($k, 0, 1)=='C')){
                                                $propertys[$k] = $item;
                                        }
                                }else{
                                        $propertys[$k] = $item;
                                }
                        }

                }else{
                        foreach($arResult['PROPERTIES'] as $k=>$item){
                                if(strlen($k)!=2){
                                        $propertys[$k] = $item;
                                }
                        }

                }
        }else{
                if($sys == 'общая'){
                        foreach($arResult['PROPERTIES'] as $k=>$item){
                                if(strlen($k)==2){
                                        if((substr($k, 0, 1)=='D')||(substr($k, 0, 1)=='F')){
                                                $propertys[$k] = $item;
                                        }
                                }else{
                                        $propertys[$k] = $item;
                                }
                        }

                }elseif($sys == 'упрощенная'){
                        foreach($arResult['PROPERTIES'] as $k=>$item){
                                if(strlen($k)==2){
                                        if((substr($k, 0, 1)=='E')||(substr($k, 0, 1)=='F')){
                                                $propertys[$k] = $item;
                                        }
                                }else{
                                        $propertys[$k] = $item;
                                }
                        }
                }else{
                        foreach($arResult['PROPERTIES'] as $k=>$item){
                                if(strlen($k)!=2){
                                        $propertys[$k] = $item;
                                }
                        }
                }
        }
        $arResult['PROPERTIES'] = $propertys;*/
        ?>

                        <div class="wrapper">
                                <div class="garantii">
                                        <!-- <table class="width_100 float_left">
                                        <?foreach($arResult['PROPERTIES'] as $item){?>
                                                <tr>
                                                        <?if ($item['PROPERTY_TYPE']=='F'){?>
                                                                <td>
                                                                        <?=$item['NAME']?>
                                                                        <?
                                                                        $rsfile = CFile::GetByID($item['VALUE']);
                                                                        $arfile = $rsfile->Fetch();
                                                                        $url = CFile::GetPath($item['VALUE']);
                                                                        ?>
                                                                </td><td>                
                                                                        <?if(!empty($url)){?>
                                                                                <a href="/include/upload.php?ID=<?=$item['VALUE']?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                                                        <?}else{?>
                                                                                Нет файла
                                                                        <?}?>
                                                                </td>
                                                        <?}else{?>
                                                                <td>
                                                                        <?=$item['NAME']?>
                                                                </td>
                                                                <td>
                                                                        <?=$item['VALUE']?>
                                                                </td>
                                                        <?}?>
                                                </tr>
                                        <?}?>
                                        </table>-->
                                        
                                        <div class="garantii">
                                        <input type='hidden' name='ID' value='<?=$arResult['ID']?>'>
                                        <?  foreach ($arResult['PROPERTIES'] as $k=>$item){
                                                if(!empty($item['VALUE'])){
                                                        if(strlen($k) == 2){?>
                                                                <input type='hidden' name='<?=$k?>_k' value='1'>
                                                        <?}
                                                }
                                        }?>
                                        <h1>Заявка № <?=$arResult["ID"]?></h1>
                                        <span class="title_table">Условия гарантии</span>
                                        
                                        <div class="element_anketa">
                                            Тип гарантии
                                            <input type="radio" disabled <?=$arResult['PROPERTIES']['I']['VALUE']=='Коммерческая'?'checked="checked"':''?> value="7">
                                            <label for="avans141">Коммерческая</label>
                                            <input type="radio" disabled <?=$arResult['PROPERTIES']['I']['VALUE']=='Госзакупка'?'checked="checked"':''?> value="8">
                                            <label for="avans142">Госзакупка</label>
                                        </div>
                                        <div class="element_anketa notice_box" style="<?=$arResult['PROPERTIES']['I']['VALUE']=='Коммерческая'?'display:none':''?>">
                                                <input type="text" placeholder='Номер извещения/закупки' disabled value='<?=empty($arResult['PROPERTIES']['A']['VALUE'])?'&nbsp':$arResult['PROPERTIES']['A']['VALUE']?>'>
                                        </div>
                                        <div class="element_anketa">
                                                <input class="main_element_form" type="text" placeholder='Сумма БГ, руб' disabled value='<?=num($arResult['PROPERTIES']['B']['VALUE'])?>'>
                                                <div class="new-select-style-wpandyou-anceta currency">
                                                        <select disabled class="main_element_form">
                                                                <option value="0" >руб.</option>
                                                                <option <?if($arResult['PROPERTIES']['CURRENCY']['VALUE_ENUM_ID']==24){?> selected<?}?> value="24">$</option>
                                                                <option <?if($arResult['PROPERTIES']['CURRENCY']['VALUE_ENUM_ID']==25){?> selected<?}?> value="25">€</option>
                                                        </select>
                                                </div>
                                        </div>
                                        <div class="element_anketa">
                                        		Срок гарантии с момента выдачи 
                                        		<input disabled type="radio" <?if($arResult['PROPERTIES']['GARANTE_VIDACHI']['VALUE_ENUM_ID']==26){?> checked<?}?> value="26">
                                                <label for="avans11">Да</label>
                                                <input disabled type="radio" <?if($arResult['PROPERTIES']['GARANTE_VIDACHI']['VALUE_ENUM_ID']==27){?> checked<?}?> value="27">
                                                <label for="avans12">Нет</label>
                                        </div>
                                        <div class="element_anketa">
                                                Срок действия банковской гарантии <input disabled type="text" placeholder='с' style="<?if($arResult['PROPERTIES']['GARANTE_VIDACHI']['VALUE_ENUM_ID']==26){?> display:none<?}?>" class="date_time datepicker" autocomplete="false"  value='<?=$arResult['PROPERTIES']['GARANTE_VIDACHI']['VALUE_ENUM_ID']==26?$arResult['DATE_CREATE']:$arResult['PROPERTIES']['D1']['VALUE']?>'> <input disabled type="text" placeholder='по' class="date_time datepicker" autocomplete="false"  value='<?=$arResult['PROPERTIES']['D2']['VALUE']?>'> <span class="error text"></span>
                                        </div>
                                        <div class="element_anketa">
                                                Аванс
                                                <input disabled class="required" type="radio" value="1"<?if($arResult['PROPERTIES']['E']['VALUE']=='Да'){?> checked<?}?>>
                                                <label for="avans11">Да</label>
                                                <input disabled class="required" type="radio" value="2"<?if($arResult['PROPERTIES']['E']['VALUE']=='Нет'){?> checked<?}?>>
                                                <label for="avans12">Нет</label>
                                        </div>
                                        <div class="element_anketa">
                                                <textarea placeholder='Комментарий' cols="100" rows="15" disabled><?=$arResult['PROPERTIES']['H']['VALUE']["TEXT"]?></textarea>
                                        </div>        
                                </div>                                  
                                <div class="anketa">
                                        <span class="title_table">Анкета</span>
                                        <div class="element_anketa">
                                                <input type="text" disabled placeholder='Наименвоание принципала' value='<?=$arResult['PROPERTIES']['L']['VALUE']?>'>
                                        </div>
                                        <div class="element_anketa">
                                                <input type="text" disabled placeholder='ИНН принципала' value='<?=$arResult['PROPERTIES']['M']['VALUE']?>'>
                                        </div>
                                        <div class="element_anketa">
                                                Шаблон заказчика
                                                <input class="required" type="radio" disabled value="11"<?if($arResult['PROPERTIES']['N']['VALUE']=='Да'){?> checked<?}?>>
                                                <label for="avans41">Да</label>
                                                <input class="required" type="radio" disabled value="12"<?if($arResult['PROPERTIES']['N']['VALUE']=='Нет'){?> checked<?}?>>
                                                <label for="avans42">Нет</label>

                                        </div>
                                        <div class="element_anketa">
                                                <textarea placeholder='Смена статусов' disabled cols="100" rows="15"><?=$arResult['COMMENT']?></textarea>
                                        </div>
                                </div>
                                <div class="uslugi">

                    <h4>Документы для рассмотрения юридического лица Принципала/Залогодателя/Поручителя</h4>
                    
                    <div class="horizont_line"></div>
                    <div class="element_anketa">
                            Заявление на предоставление банковской гарантии, <a href="/zayavki/document_list/">образец</a>
                         <?
                         ;
                         if(!empty($arResult)){
                                    $ID = $arResult['PROPERTIES']['AE']['VALUE'];
                                    echo $ID;
                                    if(!empty($ID)){
                                            $rsfile = CFile::GetByID($ID);
                                            $arfile = $rsfile->Fetch();?>
                                            <div class="file_url">
                                                    <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                            </div>
                                    <?}
                            }?> 
                    
                    </div>
                    <div class="horizont_line"></div>
                    <div class="element_anketa">
                            Устав с отметкой ФНС со всеми действующими изменениями в Устав с отметкой ФНС (редакция, действующая на дату проведения юридической экспертизы)

                            <?if(!empty($arResult)){
                                    $ID = $arResult['PROPERTIES']['USTAV_S_OTMETKOY_FNS']['VALUE'];
                                    if(!empty($ID)){
                                            $rsfile = CFile::GetByID($ID);
                                            $arfile = $rsfile->Fetch();?>
                                            <div class="file_url">
                                                    <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                            </div>
                                    <?}
                            }?>     
                    </div>
                    <div class="horizont_line"></div>
                    <div class="element_anketa">
                            Протокол об избрании/назначении единоличного исполнительного органа (Генерального директора/Директора/др.) и Главного бухгалтера
                            <?if(!empty($arResult)){
                                    $ID = $arResult['PROPERTIES']['PROTOKOL_BUH_GLAV']['VALUE'];
                                    if(!empty($ID)){
                                            $rsfile = CFile::GetByID($ID);
                                            $arfile = $rsfile->Fetch();?>
                                            <div class="file_url">
                                                    <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                            </div>
                                    <?}
                            }?>     
                    </div>
                    <div class="horizont_line"></div>
                    <div class="element_anketa">
                            Паспорта всех Учредителей, Генерального директора и Главного бухгалтера>  
                            <?if(!empty($arResult)){
                                    $ID = $arResult['PROPERTIES']['ALL_PASPORT']['VALUE'];
                                    if(!empty($ID)){
                                            $rsfile = CFile::GetByID($ID);
                                            $arfile = $rsfile->Fetch();?>
                                            <div class="file_url">
                                                    <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                            </div>
                                    <?}
                            }?>    
                    </div>
                    <div class="horizont_line"></div>
                    <div class="element_anketa">
                            Свидетельство о постановке на учет в налоговом органе по месту нахождения организации (ИНН)
                            <?if(!empty($arResult)){
                                    $ID = $arResult['PROPERTIES']['FB']['VALUE'];
                                    if(!empty($ID)){
                                            $rsfile = CFile::GetByID($ID);
                                            $arfile = $rsfile->Fetch();?>
                                            <div class="file_url">
                                                    <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                            </div>
                                    <?}
                            }?>   
                    </div>
                    <div class="horizont_line"></div>
                    <div class="element_anketa">
                            Лицензии, сертификаты, разрешения на ведение деятельности (в случаях, когда их наличие обязательно)
                            <?if(!empty($arResult)){
                                    $ID = $arResult['PROPERTIES']['LICENSE']['VALUE'];
                                    if(!empty($ID)){
                                            $rsfile = CFile::GetByID($ID);
                                            $arfile = $rsfile->Fetch();?>
                                            <div class="file_url">
                                                    <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                            </div>
                                    <?}
                            }?>    
                    </div>
                    <div class="horizont_line"></div>
                    <div class="element_anketa">
                            Документы, подтверждающие наличие по местонахождению юридического лица, его постоянно действующего органа управления (свидетельство о праве собственности, договор аренды, договор субаренды и др.)
                    <?if(!empty($arResult)){
                                    $ID = $arResult['PROPERTIES']['REGISTER_URL_LICO']['VALUE'];
                                    if(!empty($ID)){
                                            $rsfile = CFile::GetByID($ID);
                                            $arfile = $rsfile->Fetch();?>
                                            <div class="file_url">
                                                    <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                            </div>
                                    <?}
                            }?>    
                    </div>
                    <div class="horizont_line"></div>
                    <div class="element_anketa">
                            Протокол общего собрания участников общества  об одобрении крупной сделки.
                            <?if(!empty($arResult)){
                                    $ID = $arResult['PROPERTIES']['PROTOKOL_BIG_SUMM']['VALUE'];
                                    if(!empty($ID)){
                                            $rsfile = CFile::GetByID($ID);
                                            $arfile = $rsfile->Fetch();?>
                                            <div class="file_url">
                                                    <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                            </div>
                                    <?}
                            }?>     
                    </div>
                    <div class="horizont_line"></div>
                    <div class="element_anketa">
                            Проект контракта 
                            <?if(!empty($arResult)){
                                    $ID = $arResult['PROPERTIES']['AH']['VALUE'];
                                    if(!empty($ID)){
                                            $rsfile = CFile::GetByID($ID);
                                            $arfile = $rsfile->Fetch();?>
                                            <div class="file_url">
                                                    <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                            </div>
                                    <?}
                            }?>    
                    </div>
                    <div class="horizont_line"></div>
                    <div class="element_anketa">
                            Согласия юридического и физического лиц на запрос и предоставление информации в Бюро Кредитных Историй для всех учредителей и директора, <a href="/zayavki/document_list/">образец</a>>  
                            <?if(!empty($arResult)){
                                    $ID = $arResult['PROPERTIES']['SOGLASIE_V_BURO']['VALUE'];
                                    if(!empty($ID)){
                                            $rsfile = CFile::GetByID($ID);
                                            $arfile = $rsfile->Fetch();?>
                                            <div class="file_url">
                                                    <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                            </div>
                                    <?}
                            }?>    
                    </div>
                    
                    
                    <div class="horizont_line"></div>
                    <h4>Финансовые документы (предоставляются только Принципалом)</h4> 
                    <div class="horizont_line"></div>
                    <div class="element_anketa">
                            Баланс и отчет о финансовых результатах предприятия за последние 5 отчетных периодов.    
                            <?if(!empty($arResult)){
                                    $ID = $arResult['PROPERTIES']['FIZ_BALANCE']['VALUE'];
                                    if(!empty($ID)){
                                            $rsfile = CFile::GetByID($ID);
                                            $arfile = $rsfile->Fetch();?>
                                            <div class="file_url">
                                                    <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                            </div>
                                    <?}
                            }?>  
                    </div>
                    <div class="horizont_line"></div>
                    <div class="element_anketa">
                            Годовая декларация по налогу на прибыль предприятия за последний завершенный финансовый год с отметкой ФНС о принятии непосредственно на отчетности или с приложением документа, подтверждающего факт передачи отчетности в налоговой орган /если не применяется режим ЕНВД или УСН/ 
                            <?if(!empty($arResult)){
                                    $ID = $arResult['PROPERTIES']['FIZ_GOD_DEKL']['VALUE'];
                                    if(!empty($ID)){
                                            $rsfile = CFile::GetByID($ID);
                                            $arfile = $rsfile->Fetch();?>
                                            <div class="file_url">
                                                    <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                            </div>
                                    <?}
                            }?>    
                    </div>
                    <div class="horizont_line"></div>
                    <div class="element_anketa">
                            Налоговые декларации за последний календарный год, с отметкой ФНС о принятии непосредственно на отчетности или с приложением документа, подтверждающего факт передачи отчетности в налоговой орган /если компания уплачивает ЕНВД или применяет УСН/
 
                            <?if(!empty($arResult)){
                                    $ID = $arResult['PROPERTIES']['FIZ_NALOG_DEKL']['VALUE'];
                                    if(!empty($ID)){
                                            $rsfile = CFile::GetByID($ID);
                                            $arfile = $rsfile->Fetch();?>
                                            <div class="file_url">
                                                    <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                            </div>
                                    <?}
                            }?>     
                    </div>
                    <div class="horizont_line"></div>
                    <div class="element_anketa">
                            Информационное письмо, содержащее сведения: об открытых расчетных счетах  в других кредитных организациях; об оборотах по указанным счетам за последние 12 месяцев; об отсутствии/наличии картотеки №2; об отсутствии/наличии ссудной задолженности за последние 12 месяцев, <a href="/zayavki/document_list/">образец</a>

                            <?if(!empty($arResult)){
                                    $ID = $arResult['PROPERTIES']['FIZ_SCHET_INFO']['VALUE'];
                                    if(!empty($ID)){
                                            $rsfile = CFile::GetByID($ID);
                                            $arfile = $rsfile->Fetch();?>
                                            <div class="file_url">
                                                    <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                            </div>
                                    <?}
                            }?>      
                    </div>
                    <div class="horizont_line"></div>
                    <div class="element_anketa">
                            Карточка 51 счета за последний отчетный период  в разрезе операций.

                            <?if(!empty($arResult)){
                                    $ID = $arResult['PROPERTIES']['FIZ_51_CARD']['VALUE'];
                                    if(!empty($ID)){
                                            $rsfile = CFile::GetByID($ID);
                                            $arfile = $rsfile->Fetch();?>
                                            <div class="file_url">
                                                    <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                            </div>
                                    <?}
                            }?>     
                    </div>
                    
                    <div class="horizont_line"></div>
                    <div class="element_anketa">
                            Отчет о дебиторской и кредиторской задолженности на последнюю отчетную дату по форме банка, образец стр. 7 

                            <?if(!empty($arResult)){
                                    $ID = $arResult['PROPERTIES']['FIZ_OTCHET_DEB']['VALUE'];
                                    if(!empty($ID)){
                                            $rsfile = CFile::GetByID($ID);
                                            $arfile = $rsfile->Fetch();?>
                                            <div class="file_url">
                                                    <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                            </div>
                                    <?}
                            }?>    
                    </div>
                    <div class="horizont_line"></div>
                    <div class="element_anketa">
                            Оборотно-сальдовые ведомости в разрезе операций за последний отчетный период следующих строк баланса: финансовые вложения (1170), запасы (1210), дебиторская задолженность (1230), финансовые вложения (1240),  долгосрочные займы (1410), краткосрочные займы (1510), кредиторская задолженность (1520)  и других строк баланса превышающих 10% от валюты баланса.
  
                            <?if(!empty($arResult)){
                                    $ID = $arResult['PROPERTIES']['FIZ_SALD']['VALUE'];
                                    if(!empty($ID)){
                                            $rsfile = CFile::GetByID($ID);
                                            $arfile = $rsfile->Fetch();?>
                                            <div class="file_url">
                                                    <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                            </div>
                                    <?}
                            }?>   
                    </div>
                    <?if(!in_array(6, CUser::GetUserGroup($USER->GetID()))){?>
                        <h4>Документы для лицензии</h4>
                        <div class="horizont_line"></div>
                        <div class="element_anketa">
                                Проект БГ 
                                <?if(!empty($arResult)){
                                        $ID = $arResult['PROPERTIES']['PROJECT_BG']['VALUE'];
                                        if(!empty($ID)){
                                                $rsfile = CFile::GetByID($ID);
                                                $arfile = $rsfile->Fetch();?>
                                                <div class="file_url">
                                                        <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                                </div>
                                        <?}
                                }?>    
                        </div>  
                        <div class="horizont_line"></div>
                        <div class="element_anketa">
                                Договор БГ  
                                <?if(!empty($arResult)){
                                        $ID = $arResult['PROPERTIES']['CONTRACT_BG']['VALUE'];
                                        if(!empty($ID)){
                                                $rsfile = CFile::GetByID($ID);
                                                $arfile = $rsfile->Fetch();?>
                                                <div class="file_url">
                                                        <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                                </div>
                                        <?}
                                }?>    
                        </div>  
                        <div class="horizont_line"></div>
                        <div class="element_anketa">
                                Счет 
                                <?if(!empty($arResult)){
                                        $ID = $arResult['PROPERTIES']['BILL']['VALUE'];
                                        if(!empty($ID)){
                                                $rsfile = CFile::GetByID($ID);
                                                $arfile = $rsfile->Fetch();?>
                                                <div class="file_url">
                                                        <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                                </div>
                                        <?}
                                }?>    
                        </div>  
                        <div class="horizont_line"></div>
                        <div class="element_anketa">
                                Скан 
                                <?if(!empty($arResult)){
                                        $ID = $arResult['PROPERTIES']['DUPLICATE']['VALUE'];
                                        if(!empty($ID)){
                                                $rsfile = CFile::GetByID($ID);
                                                $arfile = $rsfile->Fetch();?>
                                                <div class="file_url">
                                                        <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                                </div>
                                        <?}
                                }?>    
                        </div>     
                    </div>
                    <?}?>
                    
                    
                                
    <?global $USER;
    if(in_array(6, CUser::GetUserGroup($USER->GetID()))){?>
            <?
            $status=array(
                    16 => 'Передано в банк',
                    18 => 'Ожидается оплата',
                    19 => 'БГ выдана',
                    20 => 'Отказ'
            );?>
            <div class="error-text"></div>
            <form method="POST" action='/include/status_BG.php' class='on_ajax status_BG' enctype="multipart/form-data">
                    <input type='hidden' name='ID' value="<?=$arResult['ID']?>">
                    <div class="horizont_line"></div>
                    
                        <h4>Документы банковской гарантии</h4>
                        <div class="horizont_line"></div>
                        <div class="element_anketa">
                                Проект БГ 
                                <?if(empty($arResult['PROPERTIES']['PROJECT_BG']['VALUE'])){?>
                                <div class="file_button">
                                        <input type="file" name='PROJECT_BG'>
                                </div>  
                                <?}?>
                                <?if(!empty($arResult)){
                                        $ID = $arResult['PROPERTIES']['PROJECT_BG']['VALUE'];
                                        if(!empty($ID)){
                                                $rsfile = CFile::GetByID($ID);
                                                $arfile = $rsfile->Fetch();?>
                                                <div class="file_url">
                                                        <a href="/include/upload.php?ID=<?=$ID?>" id="file_PROJECT_BG" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                                </div>
                                        <?}
                                }?>    
                        </div>  
                        <div class="horizont_line"></div>
                        <div class="element_anketa">
                                Договор БГ 
                                <?if(empty($arResult['PROPERTIES']['CONTRACT_BG']['VALUE'])){?>
                                <div class="file_button">
                                        <input type="file" name='CONTRACT_BG'>
                                </div> 
                                <?}?> 
                                <?if(!empty($arResult)){
                                        $ID = $arResult['PROPERTIES']['CONTRACT_BG']['VALUE'];
                                        if(!empty($ID)){
                                                $rsfile = CFile::GetByID($ID);
                                                $arfile = $rsfile->Fetch();?>
                                                <div class="file_url">
                                                        <a href="/include/upload.php?ID=<?=$ID?>" id="file_CONTRACT_BG" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                                </div>
                                        <?}
                                }?>    
                        </div>  
                        <div class="horizont_line"></div>
                        <div class="element_anketa">
                                Счет
                                <?if(empty($arResult['PROPERTIES']['BILL']['VALUE'])){?> 
                                <div class="file_button">
                                        <input type="file" name='BILL'>
                                </div>
								<?}?>
                                <?if(!empty($arResult)){
                                        $ID = $arResult['PROPERTIES']['BILL']['VALUE'];
                                        if(!empty($ID)){
                                                $rsfile = CFile::GetByID($ID);
                                                $arfile = $rsfile->Fetch();?>
                                                <div class="file_url">
                                                        <a href="/include/upload.php?ID=<?=$ID?>" id="file_BILL" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                                </div>
                                        <?}
                                }?>    
                        </div>  
                        <div class="horizont_line"></div>
                        <div class="element_anketa">
                                Скан 
                                <?if(empty($arResult['PROPERTIES']['DUPLICATE']['VALUE'])){?>
                                <div class="file_button">
                                        <input type="file" name='DUPLICATE'>
                                </div>  
                                <?}?>
                                <?if(!empty($arResult)){
                                        $ID = $arResult['PROPERTIES']['DUPLICATE']['VALUE'];
                                        if(!empty($ID)){
                                                $rsfile = CFile::GetByID($ID);
                                                $arfile = $rsfile->Fetch();?>
                                                <div class="file_url">
                                                        <a href="/include/upload.php?ID=<?=$ID?>" id="file_DUPLICATE" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                                                </div>
                                        <?}
                                }?>    
                        </div>     
                    
                                                        
                                                        <div class="element_anketa">
                                                                Изменить статус:
                                                                <div class="new-select-style-wpandyou-anceta">
                                                                        <select name="STATUS" onchange="changeStatusBank(this)" id="status_select" class="main_element_form">
                                                                                <?foreach($status as $k=>$item){?>
                                                                                <option value="<?=$k?>"<?if($arResult['PROPERTIES']['STATUS']['VALUE']==$item){?> selected<?}?>><?=$item?></option>
                                                                                <?}?>
                                                                        </select>
                                                                </div>                                                        
                                                        </div>  
                                                        <div class="element_anketa">
                                                        	<div class="error_comment" style="color:red"></div>
                                                        	<textarea placeholder='Комментарий по возврату' cols="100" rows="5" id="back_text" style="display:none" name='back_text'></textarea>
                                                        </div>
                                                        <div class="element_anketa">
                                                                <button name="operation" onclick="status_operation('save')" value="save">Сохранить</button> <button onclick="status_operation('back')" name="operation" value="back">Вернуть</button>
                                                        </div>  
                                                </form>
                                                <script>
                                                function status_operation(operation)
                                                {
                                                	if(operation == 'save')
                                                	{
                                                		$('#back_text').attr('style','display:none');
                                                		$('#operation').attr('data','save');
                                                	}
                                                	else
                                                	{
                                                		$('#back_text').attr('style','display:block');
                                                		$('#operation').attr('data','back');
                                                	}
                                                	
                                                }
                                                function changeStatusBank(obj)
                                                {
													if($(obj).val()=='20')
													{
                                                		$('#back_text').attr('style','display:block')
													}
													else
													{
                                                		$('#back_text').attr('style','display:none');
													}
                                                }
                                                </script>
                                        <?}?>

                                        <a href="/zayavki/">К заявкам</a>
                                </div>
                        </div>
<?}?>