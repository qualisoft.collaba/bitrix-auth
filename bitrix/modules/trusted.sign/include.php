<?
IncludeModuleLangFile(__FILE__);


class CTrustedSign
{

    function Script()
    {
        CJSCore::RegisterExt("trusted_sign", Array(
            "js" => "/bitrix/js/trusted.sign/trustedsign.js",
            "lang" => "/bitrix/modules/trusted.sign/lang/ru/lang_js.php",
            "rel" => array('jquery')
        ));
        global $APPLICATION;
        if (CModule::IncludeModule('trusted.sign')) {
            if (!defined(ADMIN_SECTION) && ADMIN_SECTION !== true) {
                CJSCore::Init(array("trusted_sign"));
                $Activate_sign_disk = (COption::GetOptionString("trusted.sign", "Activate_sign_disk") != null) ? COption::GetOptionString("trusted.sign", "Activate_sign_disk") : "N";
                $Activate_cosign_disk = (COption::GetOptionString("trusted.sign", "Activate_cosign_disk") != null) ? COption::GetOptionString("trusted.sign", "Activate_cosign_disk") : "N";
                $Activate_remove_disk = (COption::GetOptionString("trusted.sign", "Activate_remove_disk") != null) ? COption::GetOptionString("trusted.sign", "Activate_remove_disk") : "N";

                $Activate_sign_webdav = (COption::GetOptionString("trusted.sign", "Activate_sign_webdav") != null) ? COption::GetOptionString("trusted.sign", "Activate_sign_webdav") : 0;
                $Activate_cosign_webdav = (COption::GetOptionString("trusted.sign", "Activate_cosign_webdav") != null) ? COption::GetOptionString("trusted.sign", "Activate_cosign_webdav") : 0;
                $Activate_remove_webdav = (COption::GetOptionString("trusted.sign", "Activate_remove_webdav") != null) ? COption::GetOptionString("trusted.sign", "Activate_remove_webdav") : 0;
                $APPLICATION->AddHeadString("<script>
				Activate_remove_webdav = '" . $Activate_remove_webdav . "';
				Activate_cosign_webdav = '" . $Activate_cosign_webdav . "';
				Activate_sign_webdav = '" . $Activate_sign_webdav . "';

				Activate_remove_disk = '" . $Activate_remove_disk . "';
				Activate_cosign_disk = '" . $Activate_cosign_disk . "';
				Activate_sign_disk = '" . $Activate_sign_disk . "';
				</script>", true);
            }
        }
    }
}

?>