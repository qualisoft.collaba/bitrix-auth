<?

$trusted_sign_default_option = array(
    "Activate_sign_disk" => false,
    "Activate_cosign_disk" => false,
    "Activate_remove_disk" => false,
    "Openssl_path" => "openssl"
);
$trusted_sign_default_sign = array(
    0 => GetMessage("trustedsign_options_off"),
    1 => GetMessage("trustedsign_options_on_icon"),
    2 => GetMessage("trustedsign_options_on_context"),
    3 => GetMessage("trustedsign_options_on_all")
);
?>
