<?
$MESS ['TBSIGN_ATT'] = "Подпись документа";
$MESS ['TBSIGN_SIGN_DOWNLOAD'] = "Скачать подписанный документ";
$MESS ['TBSIGN_NOSIGN_DOWNLOAD'] = "Скачать неподписанный документ";
$MESS ['TBSIGN_ADD_SIGN'] = "Добавить подпись";
$MESS ['TBSIGN_ADD_SIGN_TITLE'] = "Добавить подпись к подписанному документу";
$MESS ['TBSIGN_SIGN'] = "Подписать документ";
$MESS ['TBSIGN_WAIT'] = "Пожалуйста, подождите...";
$MESS ['TBSIGN_CANCEL'] = "Закрыть";
$MESS ['TBSIGN_CRIPTOARM'] = "Скачать «КриптоАрм»";
$MESS ['TBSIGN_ERROR_AJAX'] = "Произошла ошибка в отправке запроса. ";
$MESS ['TBSIGN_SUCCESS'] = "Документ успешно подписан.";
$MESS ['TBSIGN_ERROR_DOWNLOAD_NOSIGN'] = "Не найден сертификат в подписанном документе.";
$MESS ['TBSIGN_ERROR_DOWN_NOSIGN'] = "Произошла ошибка во время скачивания неподписанного документа.";
$MESS ['TBSIGN_ERROR_SAVE_FILE'] = "Произошла ошибка сохранения документа";
$MESS ['TBSIGN_ERROR_PROACTIVE'] = "Нет доступа к изменению файла. Обратитесь к системному администратору.";
$MESS ['TBSIGN_SIGN_POPUP1'] = "Выберите сертификат и дождитесь окончания процедуры подписи документа";
$MESS ['TBSIGN_SIGN_POPUP2'] = "Идёт подпись документа...";
$MESS ['TBSIGN_COSIGN_POPUP1'] = "Выберите сертификат и дождитесь окончания процедуры добавления подписи к документу";
$MESS ['TBSIGN_COSIGN_POPUP2'] = "Идёт добавление подписи к документу...";
$MESS ['TBSIGN_ERROR_TITLE'] = "Произошла ошибка. Документ не подписан.";
$MESS ['TBSIGN_SEARCH_TITLE'] = "Скачать документ";
$MESS ['TBSIGN_NO_PARAM'] = "Нет необходимых параметров.";
?>