<?
$MESS ['Activate_sign'] = "Подпись документов";
$MESS ['Activate_cosign'] = "Добавление подписи к подписанным документам";
$MESS ['Activate_remove'] = "Скачивание документов<sup><span class=\"required\">1</span></sup>";
$MESS ['trustedsign_options_sys'] = "Настройка подписи в модуле «Библиотека документов»";
$MESS ['trustedsign_options_disk'] = "Настройка подписи в модуле «Диск»";
$MESS ['trustedsign_options_openssl'] = "Настройка OpenSSL";
$MESS ['trustedsign_options_off'] = "Выключить";
$MESS ['trustedsign_options_on_icon'] = "Включить только иконку";
$MESS ['trustedsign_options_on_context'] = "Включить только в контекстном меню";
$MESS ['trustedsign_options_on_all'] = "Включить";
$MESS["Activate_remove_info"] = "<span class=\"required\">1</span> - Добавляет 2 иконки или 2 пункта меню к подписанному документу: <b>«Скачать неподписанный документ»</b> и <b>«Скачать подписанный документ»</b>.";
$MESS['NO_TRAPI_DETAIL'] = "Установите модуль.";
$MESS['NO_TRAPI'] = "Не установлен модуль «<a href='https://www.google.ru/?gws_rd=ssl#newwindow=1&q=trusted+api' target='_blank'>Trusted.Api</a>»";
$MESS['Openssl_path'] = "Путь к OpenSSL<sup><span class=\"required\">2</span></sup>";
$MESS['Openssl_path_info'] = "<span class=\"required\">2</span> - Путь к <b>«openssl.exe»</b> не должен содержать пробелы и русские символы.";

?>