<?
IncludeModuleLangFile(__FILE__);
if (class_exists("trusted_sign"))
    return;

Class trusted_sign extends CModule
{
    var $MODULE_ID = "trusted.sign";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_GROUP_RIGHTS = "Y";


    function trusted_sign()
    {
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        } else {
            $this->MODULE_VERSION = TASKFROMEMAIL_MODULE_VERSION;
            $this->MODULE_VERSION_DATE = TASKFROMEMAIL_MODULE_VERSION_DATE;
        }

        $this->MODULE_NAME = GetMessage("SIGN_MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("SIGN_MODULE_DESCRIPTION");

        $this->PARTNER_NAME = GetMessage("SIGN_PARTNER_NAME");
        $this->PARTNER_URI = "http://digt.ru/";
    }

    function DoInstall()
    {
        global $DB, $APPLICATION, $step;
        $step = IntVal($step);
        if (!check_bitrix_sessid()) {
            return false;
        }
        if (!IsModuleInstalled("trusted.api")) {
            if ($step < 1) {

                if (IsModuleInstalled($this->MODULE_ID)) {
                    return false;
                }

                $APPLICATION->IncludeAdminFile(GetMessage("FORM_INSTALL_TSIGN"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/trusted.sign/install/step1.php");
            } elseif ($step == 1) {
                if (!IsModuleInstalled("trusted.sign")) {
                    $this->InstallDB();
                    $this->InstallEvents();
                    $this->InstallFiles();
                }
                return true;
            }
        } else {
            if (!IsModuleInstalled("trusted.sign")) {
                $this->InstallDB();
                $this->InstallEvents();
                $this->InstallFiles();
            }
            return true;
        }
    }

    function DoUninstall()
    {
        $this->UnInstallDB();
        $this->UnInstallEvents();
        $this->UnInstallFiles();
        return true;
    }


    function InstallDB()
    {

        RegisterModule("trusted.sign");
        return true;


    }

    function UnInstallDB()
    {
        UnRegisterModule("trusted.sign");
        return true;
    }


    function InstallEvents()
    {
        RegisterModuleDependences("main", "OnBeforeEndBufferContent", $this->MODULE_ID, "CTrustedSign", "Script", "100");
        return true;
    }

    function UnInstallEvents()
    {
        UnRegisterModuleDependences("main", "OnBeforeEndBufferContent", $this->MODULE_ID, "CTrustedSign", "Script");
        COption::RemoveOption("trusted.sign", "");
        return true;
    }

    function InstallFiles()
    {
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/trusted.sign/install/activities", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/activities", true, true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/trusted.sign/install/js", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/js/trusted.sign", true, true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/trusted.sign/install/tmp", $_SERVER["DOCUMENT_ROOT"] . "bitrix/tmp", true, true);
        return true;
    }

    function UnInstallFiles()
    {
        DeleteDirFilesEx("/bitrix/activities/custom/trustedsign/");
        DeleteDirFilesEx("/bitrix/js/trusted.sign/");
        DeleteDirFilesEx("/bitrix/tmp/trustedsign/");
        return true;
    }


} //end class
?>