<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arActivityDescription = array(
    "NAME" => GetMessage("SIGN_DESCR_NAME"),
    "DESCRIPTION" => GetMessage("SIGN_DESCR_DESCR"),
    "TYPE" => "activity",
    "CLASS" => "TrustedSign",
    "JSCLASS" => "TrustedSign",
    "CATEGORY" => array(
        "ID" => "document",
    ),
    "RETURN" => array(
        "Comments" => array(
            "NAME" => GetMessage("SIGN_DESCR_CM"),
            "TYPE" => "string",
        ),
        "VotedCount" => array(
            "NAME" => GetMessage("SIGN_DESCR_VC"),
            "TYPE" => "int",
        ),
        "TotalCount" => array(
            "NAME" => GetMessage("SIGN_DESCR_TC"),
            "TYPE" => "int",
        ),
        "VotedPercent" => array(
            "NAME" => GetMessage("SIGN_DESCR_VP"),
            "TYPE" => "int",
        ),
        "ApprovedPercent" => array(
            "NAME" => GetMessage("SIGN_DESCR_AP"),
            "TYPE" => "int",
        ),
        "NotApprovedPercent" => array(
            "NAME" => GetMessage("SIGN_DESCR_NAP"),
            "TYPE" => "int",
        ),
        "ApprovedCount" => array(
            "NAME" => GetMessage("SIGN_DESCR_AC"),
            "TYPE" => "int",
        ),
        "NotApprovedCount" => array(
            "NAME" => GetMessage("SIGN_DESCR_NAC"),
            "TYPE" => "int",
        ),
        "LastApprover" => array(
            "NAME" => GetMessage("SIGN_DESCR_LA"),
            "TYPE" => "user",
        ),
        "Approvers" => array(
            "NAME" => GetMessage("SIGN_DESCR_APPROVERS"),
            "TYPE" => "string",
        ),
        "Rejecters" => array(
            "NAME" => GetMessage("SIGN_DESCR_REJECTERS"),
            "TYPE" => "string",
        ),
        "IsTimeout" => array(
            "NAME" => GetMessage("SIGN_DESCR_TA1"),
            "TYPE" => "int",
        ),
    ),
);
?>