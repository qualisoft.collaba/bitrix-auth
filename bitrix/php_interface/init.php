<?
function preprint($arr){
	echo '<pre>';
	print_r($arr);
	echo '</pre>';
}

function preprintjs($arr){
        echo '
                <style>.hide{display: none;}</style>                
        ';
	$time = rand(0, 1000);
	echo '<div class="hide" id="'.$time.'">';
	print_r($arr);
	echo '</div><script>
	console.groupCollapsed("%c%s", "font-size: 10pt", "PHP Debug");
	console.log(document.getElementById('.$time.').innerHTML);
	console.log("••••••••••••••••••••••••••••••••••••••••••••••••••");
	console.dirxml('.json_encode($arr).');
	console.count("Count");
	console.groupEnd();</script>';
}

function num($number){
        return number_format($number, 0, '', ' ');
}
/*AddEventHandler("main", "OnAfterUserAdd", "OnAfterUserAddHandler");
function OnAfterUserAddHandler(&$arFields){
        $arEventFields = array('EMAIL'=> $arFields['EMAIL'], 'KOD'=>$arFields['PERSONAL_PAGER']);
        CEvent::Send("EMAIL_true", 's1', $arEventFields);
        die();
}*/

function changeStatusOld($productId, $statusId)
{
    CModule::IncludeModule("iblock");

    $resStatus = CIBlockElement::GetProperty(3, $productId, "sort", "asc", array("CODE" => "STATUS"));
    if ($obStatus = $resStatus->Fetch())
        $statusActual = $obStatus["VALUE"];

    $resNomerNakladnoi = CIBlockElement::GetProperty(3, $productId, "sort", "asc", array("CODE" => "NOMER_NAKLADNOJ"));
    if ($obStatus_nakl = $resNomerNakladnoi->Fetch())
        $NomerNakladnoi = $obStatus_nakl["VALUE"];
    if ($NomerNakladnoi){
    $NomerNakladnoi='Накладная №'.$NomerNakladnoi;
    }
    if($statusActual!=$statusId)
    {
        
        $propertyStatusEnum = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>3, "CODE"=>"STATUS"));
        while($statusEnums = $propertyStatusEnum->GetNext())
            $statusFields[$statusEnums["ID"]] = $statusEnums["VALUE"];        
        
        $resStatusHistory = CIBlockElement::GetProperty(3, $productId, "sort", "asc", array("CODE" => "STATUS_HISTORY"));
        if ($obStatusHistory = $resStatusHistory->GetNext())
            $statusHistory = $obStatusHistory['VALUE']["TEXT"];
        
        $statusHistory.=date('d/m G:i').' Смена статуса: '.$statusFields[$statusId].' '.$NomerNakladnoi;
        $PROP["STATUS_HISTORY"] = $statusHistory; 
        CIBlockElement::SetPropertyValuesEx($productId,3,$PROP);
    }
}


function changeStatus($productId, $statusId, $reason = '')
{
    CModule::IncludeModule("iblock");
    $statuslist = array(15=>28,16=>29,18=>30,19=>31,20=>32,33=>33);
    
    $arSelect = Array("ID", "NAME","PROPERTY_STATUS","NOMER_ZAYAVKI");   
    $arFilter = Array("IBLOCK_ID"=>5,"PROPERTY_ID_BID"=>$productId);
    $res = CIBlockElement::GetList(Array('DATE_CREATE'=>'DESC'), $arFilter, false, false, $arSelect);
    if($arFields = $res->Fetch())    
        $currentStatus = $arFields['PROPERTY_STATUS_ENUM_ID'];

    if($statuslist[$statusId]!=$currentStatus)
    {
        echo 'GO!';
        $PROP = array(81=>$productId,83=>$statuslist[$statusId],82=>$reason);
    
        $el = new CIBlockElement;
        echo "ss";
        $arLoadProductArray = Array(
            "IBLOCK_ID"      => 5,
            "PROPERTY_VALUES"=> $PROP,
            "NAME"           => "Изменение статуса у заявки № ".$arFields['PROPERTY_NOMER_ZAYAVKI_VALUE']." статус ".$statusId,
            "ACTIVE"         => "Y",
            "DATE_CREATE"=>ConvertTimeStamp(time(), "FULL"),
            );
        if($PRODUCT_ID = $el->Add($arLoadProductArray))
            echo "New ID: ".$PRODUCT_ID;
            else
                echo "Error: ".$el->LAST_ERROR;
    
    }
}

function sendMailBackBid($bidId, $backReason)
{
    CModule::IncludeModule("iblock");
    $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_L","PROPERTY_B","CREATED_BY");
    $arFilter = Array("IBLOCK_ID"=>3,"ID"=>$bidId);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    if($arFields = $res->Fetch())
    {
        $rsUser = CUser::GetByID($arFields["CREATED_BY"]);
        $arUser = $rsUser->Fetch();
        $arSendMailFields["AGENT_EMAIL"] = $arUser["EMAIL"];
        $arSendMailFields["FIRM_NAME"] = $arFields["PROPERTY_L_VALUE"];
        $arSendMailFields["GARANTIE_SUMM"] = $arFields["PROPERTY_B_VALUE"];
        $arSendMailFields["BACK_REASON"] = $backReason;
        
        CEvent::Send("BACK_BID", 's1', $arSendMailFields);
    }
}
function sendMailRefuseBid($bidId, $backReason)
{
    CModule::IncludeModule("iblock");
    $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_L","PROPERTY_B","CREATED_BY");
    $arFilter = Array("IBLOCK_ID"=>3,"ID"=>$bidId);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    if($arFields = $res->Fetch())
    {
        $rsUser = CUser::GetByID($arFields["CREATED_BY"]);
        $arUser = $rsUser->Fetch();
        $arSendMailFields["AGENT_EMAIL"] = $arUser["EMAIL"];
        $arSendMailFields["FIRM_NAME"] = $arFields["PROPERTY_L_VALUE"];
        $arSendMailFields["GARANTIE_SUMM"] = $arFields["PROPERTY_B_VALUE"];
        $arSendMailFields["BACK_REASON"] = $backReason;

        CEvent::Send("REFUSE_BID", 's1', $arSendMailFields);
    }
}

function sendMailPayBid($bidId)
{
    CModule::IncludeModule("iblock");
    $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_L","PROPERTY_B","CREATED_BY","PROPERTY_BILL","PROPERTY_PROJECT_BG","PROPERTY_CONTRACT_BG");
    $arFilter = Array("IBLOCK_ID"=>3,"ID"=>$bidId);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    if($arFields = $res->Fetch())
    {
        
        
        $rsfile = CFile::GetByID($arFields["PROPERTY_BILL_VALUE"]);
        $arfile = $rsfile->Fetch();
        $arSendMailFields["PAY_FILE"] = '<a href="'.$_SERVER['SERVER_NAME'].'/include/upload.php?ID='.$arfile["ID"].'" target="_blank">Счет</a>';
        
        $rsfile = CFile::GetByID($arFields["PROPERTY_PROJECT_BG_VALUE"]);
        $arfile = $rsfile->Fetch();
        $arSendMailFields["PROJECT_FILE"] = '<a href="'.$_SERVER['SERVER_NAME'].'/include/upload.php?ID='.$arfile["ID"].'" target="_blank">Проект БГ</a>';
        
        $rsfile = CFile::GetByID($arFields["PROPERTY_CONTRACT_BG_VALUE"]);
        $arfile = $rsfile->Fetch();
        $arSendMailFields["ACCOUNT_FILE"] = '<a href="'.$_SERVER['SERVER_NAME'].'/include/upload.php?ID='.$arfile["ID"].'" target="_blank">Договор БГ</a>';
        
        //$rsUser = CUser::GetByID($arFields["CREATED_BY"]);
        //$arUser = $rsUser->Fetch();
        //$arSendMailFields["AGENT_EMAIL"] = $arUser["EMAIL"];
        $res_u = CIBlockElement::GetByID($arFields["ID"]);
        if($ar_res_u = $res_u->GetNext())
        //  print_r($ar_res);
        $create_user=$ar_res_u["CREATED_BY"]; 
        $GLOBALS['users'] = array("CREATED_BY" => $create_user); 
        $userid= $create_user; 
        $rsUser = CUser::GetByID($userid); 
        $arUser = $rsUser->Fetch(); 
        // echo $arUser["EMAIL"];
        $arSendMailFields["AGENT_EMAIL"] = $arUser["EMAIL"];
        
        $arSendMailFields["FIRM_NAME"] = $arFields["PROPERTY_L_VALUE"];
        $arSendMailFields["GARANTIE_SUMM"] = $arFields["PROPERTY_B_VALUE"];

        CEvent::Send("PAYED_BID", 's1', $arSendMailFields);
    }
}

function sendMailBGBid($bidId)
{
    CModule::IncludeModule("iblock");
    $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_L","PROPERTY_B","CREATED_BY","PROPERTY_DUPLICATE", "PROPERTY_*");
    $arFilter = Array("IBLOCK_ID"=>3,"ID"=>$bidId);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    if($arFields = $res->Fetch())
    {
        $rsfile = CFile::GetByID($arFields["PROPERTY_DUPLICATE_VALUE"]);
        $arfile = $rsfile->Fetch();
        $arSendMailFields["DUPLICATE_FILE"] = '<a href="'.$_SERVER['SERVER_NAME'].'/include/upload.php?ID='.$arfile["ID"].'" target="_blank">Счет</a>';


        $rsUser = CUser::GetByID($arFields["CREATED_BY"]);
        $arUser = $rsUser->Fetch();
        $arSendMailFields["AGENT_EMAIL"] = $arUser["EMAIL"];
        $arSendMailFields["FIRM_NAME"] = $arFields["PROPERTY_L_VALUE"];
        //$arSendMailFields["GARANTIE_SUMM"] = $arFields["PROPERTY_B_VALUE"];
        $nom_zayav=number_format($arFields["PROPERTY_88"], 0, '.', '');
        $nom_naklad=$_POST['NOMER_NAKLADNOJ'];
		$text= "Уважаемый,  ".$arSendMailFields["FIRM_NAME"]."\n\n".
		"Заявка №".$nom_zayav." выполнена.\n".
		"Оригиналы документов по банковской гарантии направлены по адресу указанному в анкете.\n".
		"Проследить доставку можно по адресу: https://www.pochta.ru/Tracking".
		//"\nНомер накладной: ".$arFields["PROPERTY_87"]."\n\n"."С уважением,\nБанк";
        "\nНомер накладной: ".$nom_naklad."\n\n"."С уважением,\nБанк";
        $arSendMailFields["GARANTIE_SUMM"]=$text;
        

        CEvent::Send("BG_BID", 's1', $arSendMailFields);
    }
}



// в публичке, после ДОБАВЛЕНИЯ элемента со свойством "статус = передана в банк" отправляем email
// в /include/add_BG.php лежит код для ИЗМЕНЕННОГО элемента на свойство "статус = передана в банк" отправляем email
AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("addElementStatusToBank", "OnAfterIBlockElementAddHandler"));
class addElementStatusToBank
{
    // создаем обработчик события "OnAfterIBlockElementAdd"
    function OnAfterIBlockElementAddHandler(&$arFields)
    {
CModule::IncludeModule("iblock");
$rs = CIBlockElement::GetList(array("ID"=>'DESC'), array("IBLOCK_ID"=>3), false,  array("nPageSize" => "1"),array("ID", "PROPERTY_NOMER_ZAYAVKI")); 
if($ar = $rs->GetNext())
$nomer_zayavki=$ar["PROPERTY_NOMER_ZAYAVKI_VALUE"]; 
        
        
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html;charset=utf-8 \r\n";    
        
        
        if($arFields["ID"]>0 && $arFields["IBLOCK_ID"]==3 && $arFields["PROPERTY_VALUES"]["STATUS"]==16) {
					$adminEmail = COption::GetOptionString('main', 'email_from', 'default@admin.email');
					$subject ="Новая Заявка ".$nomer_zayavki." со статусом \"Передано в банк\"";
					$text= "Новая Заявка ".$nomer_zayavki." со статусом \"Передано в банк\". Ссылка: http://".$_SERVER['SERVER_NAME'].
					"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=3&type=zayavki&ID=".$arFields["ID"]."&lang=ru";
                    if(mail($adminEmail, $subject, $text,$headers) )
					{ AddMessage2Log('init.php: Успешно отправлено! Новый статус: Передано в банк'); }
					else{ AddMessage2Log('init.php: Отправка не удалась! Новый статус: Передано в банк'); }
        } else {
             AddMessage2Log("init.php (Новый статус: Передано в банк): Ошибка добавления записи (".$arFields["RESULT_MESSAGE"].").");
        }
    }
}

// в публичке, после сохранения черновика и перебрасывании в общий список, 
// вверху общего списка нужно писать: «Черновик №… сохранен»
// в /include/add_BG.php лежит код для ИЗМЕНЕННОГО элемента
AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("addElementStatusDelete", "OnAfterIBlockElementAddHandler"));
class addElementStatusDelete
{
    
    
    // создаем обработчик события "OnAfterIBlockElementAdd"
    function OnAfterIBlockElementAddHandler(&$arFields)
    {
        if($arFields["ID"]>0 && $arFields["IBLOCK_ID"]==3 && $arFields["PROPERTY_VALUES"]["STATUS"]==15) {
        	$_SESSION["ZAYAVKA_STATUS"] = "Черновик №".$arFields["PROPERTY_VALUES"]["NOMER_ZAYAVKI"]." сохранен";
        } else {
          AddMessage2Log("init.php (Создание нового Черновика): Ошибка добавления записи (".$arFields["RESULT_MESSAGE"].").");
        }
    }
}

// в админке, после изменения элемента на свойство "статус = предварительное одобрение" и 
// "статус = отказ", "статус = БГ отправлена" отправляем письмо
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("updateElementStatusPredOdob", "OnAfterIBlockElementUpdateHandler"));
class updateElementStatusPredOdob
{
    // создаем обработчик события "OnAfterIBlockElementUpdate"
    function OnAfterIBlockElementUpdateHandler(&$arFields)
    {
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html;charset=utf-8 \r\n";    
        
        if($arFields["RESULT"] && $arFields["IBLOCK_ID"]==3 && $arFields["PROPERTY_VALUES"][55][0]["VALUE"]==18) {
        sendMailPayBid($arFields["ID"]);
         }
       
    		// статус = предварительное одобрение
        if($arFields["RESULT"] && $arFields["IBLOCK_ID"]==3 && $arFields["PROPERTY_VALUES"][55][0]["VALUE"]==33) {
            //AddMessage2Log("<pre>".print_r($arFields, true)."</pre>");
            
            $orgname = $arFields["PROPERTY_VALUES"][88]["VALUE"].":13";
            $bgsumm = $arFields["PROPERTY_VALUES"][88]["VALUE"].":2";
						$adminEmail = COption::GetOptionString('main', 'email_from', 'default@admin.email');
						$subject ="Статус Заявки ".$arFields["PROPERTY_VALUES"][88]["VALUE"].": \"Предварительное одобрение\"";
						$text= "Статус Заявки ".$arFields["PROPERTY_VALUES"][88]["VALUE"]." сменился на \"Предварительное одобрение\".\n".
						"Наименование организации: ".$arFields["PROPERTY_VALUES"][13][$orgname]["VALUE"]."\n".
						"Сумма гарантии: ".$arFields["PROPERTY_VALUES"][2][$bgsumm]["VALUE"]."\n".
						"Статус: Предварительное одобрение\n".
						"Ссылка: http://".$_SERVER['SERVER_NAME']."/zayavki/detail.php?id=".$arFields["ID"];
						if(mail($adminEmail, $subject, $text,$headers) )
						{ AddMessage2Log('init.php: Письмо успешно отправлено! Новый статус: Предварительное одобрение'); }
						else{ AddMessage2Log('init.php: Отправка не удалась! Новый статус: Предварительное одобрение'); }
				// статус = отказ
        } elseif($arFields["RESULT"] && $arFields["IBLOCK_ID"]==3 && $arFields["PROPERTY_VALUES"][55][0]["VALUE"]==20) {
            //AddMessage2Log("<pre>".print_r($arFields, true)."</pre>");
            $orgname = $arFields["PROPERTY_VALUES"][88]["VALUE"].":13";
            $bgsumm = $arFields["PROPERTY_VALUES"][88]["VALUE"].":2";
            $bgprichinaOtkazaID = $arFields["ID"].":86";
            if($arFields["PROPERTY_VALUES"][86][n0]["VALUE"]["TEXT"]) {
            	$bgprichinaOtkaza = $arFields["PROPERTY_VALUES"][86][n0]["VALUE"]["TEXT"];
            }else{
            	$bgprichinaOtkaza = $arFields["PROPERTY_VALUES"][86][$bgprichinaOtkazaID]["VALUE"]["TEXT"];
            }
						$adminEmail = COption::GetOptionString('main', 'email_from', 'default@admin.email');
						$subject ="Статус Заявки ".$arFields["PROPERTY_VALUES"][88]["VALUE"].": \"Отказ\"";
						$text= "Статус Заявки ".$arFields["PROPERTY_VALUES"][88]["VALUE"]." сменился на \"Отказ\".\n".
						"Наименование организации: ".$arFields["PROPERTY_VALUES"][13][$orgname]["VALUE"]."\n".
						"Сумма гарантии: ".$arFields["PROPERTY_VALUES"][2][$bgsumm]["VALUE"]."\n".
						"Статус: Отказ\n".
						"Причина отказа: ".$bgprichinaOtkaza."\n".
						"Ссылка: http://".$_SERVER['SERVER_NAME']."/zayavki/detail.php?id=".$arFields["ID"];
						if(mail($adminEmail, $subject, $text,$headers) )
						{ AddMessage2Log('init.php: Письмо успешно отправлено! Новый статус: Отказ'); }
						else{ AddMessage2Log('init.php: Отправка письма не удалась! Новый статус: Отказ'); }
				// статус = БГ отправлена
        } elseif($arFields["RESULT"] && $arFields["IBLOCK_ID"]==3 && $arFields["PROPERTY_VALUES"][55][0]["VALUE"]==19) {
						//выводит имя и email создателя (агента) заявки
            $res = CIBlockElement::GetByID($arFields["ID"]);
            if($ar_res = $res->GetNext()) {
  						//отделяем имя от логина
            	$chars = preg_split('/\)/', $ar_res["CREATED_USER_NAME"]);
            	$elementAgent = $chars[1];
	            //email агента
							$rsUser = CUser::GetByID($ar_res["CREATED_BY"]);
							$arUser = $rsUser->Fetch();
							$AgentEmail = $arUser["EMAIL"];
            }	

						//Наименование организации
            $naimenovanieOrgID = $arFields["PROPERTY_VALUES"][88]["VALUE"].":13";
            if($arFields["PROPERTY_VALUES"][13][n0]["VALUE"]) {
            	$naimenovanieOrg = $arFields["PROPERTY_VALUES"][13][n0]["VALUE"];
            }else{
            	$naimenovanieOrg = $arFields["PROPERTY_VALUES"][13][$naimenovanieOrgID]["VALUE"];
            }

						//номер накладной
            $nomerNakladnoyID = $arFields["PROPERTY_VALUES"][88]["VALUE"].":87";
            if($arFields["PROPERTY_VALUES"][87][n0]["VALUE"]) {
            	$nomerNakladnoy = $arFields["PROPERTY_VALUES"][87][n0]["VALUE"];
            }else{
            	$nomerNakladnoy = $arFields["PROPERTY_VALUES"][87][$nomerNakladnoyID]["VALUE"];
            }

            AddMessage2Log("init.php: Статус Заявки №".$arFields["PROPERTY_VALUES"][88]["VALUE"]." изменено на \"БГ отправлена\". (".$arFields["RESULT_MESSAGE"].")");
            
            //отправляем письмо
						$subject = $elementAgent." | ".$naimenovanieOrg." | БГ отправлена";
						$text= "Уважаемый,  ".$elementAgent."\n\n".
						"Заявка №".$arFields["PROPERTY_VALUES"][88]["VALUE"]." выполнена.\n".
						"Оригиналы документов по банковской гарантии направлены по адресу указанному в анкете.\n".
						"Проследить доставку можно по адресу: https://www.pochta.ru/Tracking".
						"\nНомер накладной: ".$nomerNakladnoy."\n\n"."С уважением,\nБанк";
						if(mail($AgentEmail, $subject, $text,$headers) )
						{ AddMessage2Log('Письмо успешно отправлено! Новый статус: БГ отправлена'); }
						else{ AddMessage2Log('Отправка письма не удалась! Новый статус: БГ отправлена'); }         
        } else {
            AddMessage2Log("init.php: Ошибка изменения записи ".$arFields["PROPERTY_VALUES"][88]["VALUE"]." (".$arFields["RESULT_MESSAGE"].").");
        }
    }
}
