/**
 * Trusted SIGN
 * ��� "�������� ����������"
 *            ################################################
 *            #  _______      ___    _______   ____________  #
 *            # |    _   \   |   |  |   ____\ |____    ____| #
 *            # |   | |   \  |   |  |  |   ___     |  |      #
 *            # |   | |    | |   |  |  |  |_  |    |  |      #
 *            # |   |_|   /  |   |  |  |___|  |    |  |      #
 *            # |________/   |___|  |_________/    |__|      #
 *            ################################################
 */

BX.ready(function () {
	jQuery.fn.exists = function () {
		return $(this).length;
	}
	/**
	 * ��� ������ ���������� ����������
	 */
	$('.bx-interface-grid').mouseup(function (event) {
		$("#TBSign, #TBSignc, #TBSignr ").remove();
		function InsertSignButton(ext, flag, id) {
			if (!$(".bx-popup-menu").exists()) setTimeout(function () {
				InsertSignButton(ext, flag, id)
			}, 1);
			else if ($(".bx-popup-menu").exists()) {
				setTimeout(function () {
					// ���� ���� sig �� �������� ��� ��������
					if (ext.toLowerCase() == "sig") {
						if ((Activate_remove_webdav === "2") || (Activate_remove_webdav === "3")) {
							// ������� ������������� ��������
							$('<tr><td><div id="TBSignr" class="popupitem" onclick="TrustedSign(' + id + ', \'remove\', \'webdav\');">	<div style="width:100%;"><table style="width:100% !important" cellpadding="0" cellspacing="0" border="0" dir="ltr">	<tbody><tr>	<td class="gutter"><div class="icon element_download"></div></td>	<td class="item" title="' + BX.message('TBSIGN_NOSIGN_DOWNLOAD') + '">' + BX.message('TBSIGN_NOSIGN_DOWNLOAD') + '</td></tr></tbody></table></div></div></td></tr>').insertAfter($(".popupmenu tr:first"));
							// ������ ����� � ������ ������� �� ����� "������� ����������� ��������"
							$('[title = "' + BX.message('TBSIGN_SEARCH_TITLE') + '"]')[0].innerHTML = BX.message('TBSIGN_SIGN_DOWNLOAD');
						}
						if (((Activate_cosign_webdav === "2") || (Activate_cosign_webdav === "3")) && flag) {
							// �������� ������� � ���������
							$('<tr><td><div id="TBSignc" class="popupitem" onclick="TrustedSign(' + id + ', \'cosign\', \'webdav\');">	<div style="width:100%;"><table style="width:100% !important" cellpadding="0" cellspacing="0" border="0" dir="ltr">	<tbody><tr>	<td class="gutter"><div class="icon element_edit"></div></td>	<td class="item" title="' + BX.message('TBSIGN_ADD_SIGN_TITLE') + '">' + BX.message('TBSIGN_SIGN') + '</td></tr></tbody></table></div></div></td></tr>').insertAfter($(".popupmenu tr:first"));
						}
					} else {
						if (((Activate_sign_webdav === "3") || (Activate_sign_webdav === "2")) && flag) {
							// ��������� ��������
							$('<tr><td><div id="TBSign" class="popupitem" onclick="TrustedSign(' + id + ', \'sign\', \'webdav\');">	<div style="width:100%;"><table style="width:100% !important" cellpadding="0" cellspacing="0" border="0" dir="ltr">	<tbody><tr>	<td class="gutter"><div class="icon element_edit"></div></td>	<td class="item" title="' + BX.message('TBSIGN_SIGN') + '">' + BX.message('TBSIGN_SIGN') + '</td></tr></tbody></table></div></div></td></tr>').insertAfter($(".popupmenu tr:first"));
						}
					}
					$(".popupitem").mouseover(function () {
						$(this).addClass("popupitemover");
					}).mouseleave(function () {
						$(this).removeClass("popupitemover");
					});
				}, 10);
			}
		}

		event.preventDefault();
// ���� �� ������ ������ ���� � �����
		if ((event.button == 2) || (event.button == 0)) {
			try {
				var idFile = "";
				var FileName = "";
				var redStatus = false;
				var obj = event.currentTarget.children[0].children;
				for (var i in obj) {
					var objClassName = obj[i].className;
					if (objClassName.indexOf("bx-over") + 1) {
						// ��������� ��������. ����� �������� ���-�� �������������
						if (event.currentTarget.children[0].children[i].children[0].children.length !== 0) {
							idFile = event.currentTarget.children[0].children[i].children[0].children[0].defaultValue;
						} else redStatus = true;
						var obj2 = event.currentTarget.children[0].children[i].children;
						if (obj2 !== undefined)
							for (var a in obj2) {
								var objClassName2 = obj2[a].firstChild.className;
								if (objClassName2 === undefined) continue;
								if (objClassName2.indexOf("element-name") + 1) {
									var obj3 = event.currentTarget.children[0].children[i].children[a].children[0].children[1].children;
									if ((obj3.length > 0) || (obj3 !== undefined))
										for (var b in obj3) {
											var objClassName3 = obj3[b].className;

											if (objClassName3.indexOf("element-title") + 1) {
												FileName = event.currentTarget.children[0].children[i].children[a].children[0].children[1].children[b].textContent;
												if (redStatus)
													idFile = event.currentTarget.children[0].children[i].children[a].children[0].children[1].children[b].id;
												break;
											}
											if (b == (obj3.length - 1)) {
												break;
											}
										}
									break;
								}
								if (a == (obj2.length - 1)) {
									break;
								}
							}
						break;
					}
					if (i == (obj.length - 1)) {
						break;
					}
				}

				if (idFile !== undefined) {
					//if (idFile.indexOf("S") + 1)
					//  console.log('�����');
					var parts,
						ext = (parts = FileName.split("/").pop().split(".")).length > 1 ? parts.pop() : ""; // �������� ����������
					if (idFile.indexOf("E") + 1) {
						idFile = idFile.replace(/[^\d,]/g, '');
						if (FileName)
							InsertSignButton(ext, true, idFile);
					}
					else {
						//console.log('���� ;)');
						idFile = idFile.replace(/[^\d,]/g, '');
						InsertSignButton(ext, false, idFile);
					}
				}
			}
			catch (e) {
				console.error(e.message);
			}
		}
	});

	if (!($(".trustbut").exists()))
		if ($(".element-name-wrapper").exists())
			$(".element-name-wrapper").each(function (i, element) { // ������� �������
				var FlagEdit = false;
				var obj = element.children;
				for (var i in obj) {
					var objClassName = obj[i].className;
					if (objClassName.indexOf("element-status-red") + 1) {
						FlagEdit = true;
						break;
					}
					if (i == (obj.length - 1)) {
						break;
					}
				}
				var reb = $(this).children(); // ������� �����
				var fileUrl = reb[0].textContent, // ��� ����� �������� ����������
					parts,
					ext = (parts = fileUrl.split("/").pop().split(".")).length > 1 ? parts.pop() : ""; // �������� ����������
				if (ext.toLowerCase() == "sig") { // ���� ���������� sig
					var s = reb[0].id; // �������� id ��������� � ����������� sig
					s = s.replace(/[^\d,]/g, ''); // �������� id ��������� ��� ����. ������ �����
					if ((Activate_remove_webdav === "1") || (Activate_remove_webdav === "3")) {
						var hrefForDownload = 'OpenDoc("' + reb[0].textContent + '", false);';
						$(reb[0]).after("<img class='trustbut' src='/bitrix/js/trusted.sign/document.png'  title='" + BX.message('TBSIGN_SIGN_DOWNLOAD') + "' style='margin:0px 0px -5px;cursor: pointer;' alt='original' onclick='" + hrefForDownload + "'/>");
						$(reb[0]).after("<img class='trustbut' src='/bitrix/js/trusted.sign/extract.png'  title='" + BX.message('TBSIGN_NOSIGN_DOWNLOAD') + "' style='margin:0px 10px -5px;cursor: pointer;' onclick='TrustedSign(" + s + ", \"remove\", \"webdav\");'/>");
					}
					if ((Activate_cosign_webdav === "1") || (Activate_cosign_webdav === "3")) {
						if (!FlagEdit)
							$(reb[0]).after("<img class='trustbut' src='/bitrix/js/trusted.sign/sign.png'  title='" + BX.message('TBSIGN_ADD_SIGN') + "' style='margin:0px 0px -5px 10px;cursor: pointer;' onclick='TrustedSign(" + s + ", \"cosign\", \"webdav\");'/>");
					}
				} // ������ ������ � ��������� � ��������, ���� �� ������ ����� ����.
				else {  // ���� �� sig , �� ��������� �������� ���������
					var s = reb[0].id; // �������� id ��������� � ������ �����������
					s = s.replace(/[^\d,]/g, ''); // �������� id ��������� ��� ����. ������ �����
					if ((Activate_sign_webdav === "1") || (Activate_sign_webdav === "3")) {
						if (!FlagEdit)
							$(reb[0]).after("<img src='/bitrix/js/trusted.sign/sign.png'  title='" + BX.message('TBSIGN_SIGN') + "' style='margin:0px 10px -5px;cursor: pointer;' onclick='TrustedSign(" + s + ", \"sign\", \"webdav\");'/>");
					}
				}
			});

	/**
	 * ��� ������ ����
	 */
	$('.bx-disk-table-body, .bx-disk-file-container').mouseup(function (event) {       //console.log(event);
		$("#TBSignDisk, #TBSignDiskc, #TBSignDiskr").remove();
		function insertInMenu(id, file) {
			if (!(($('#menu-popup-grid_action_pm').css('display')) == "block")) setTimeout(function () {
				insertInMenu(id, file)
			}, 1);
			else if (($('#menu-popup-grid_action_pm').css('display')) == "block") {
				setTimeout(function () {
					if (!($("#TBSignDisk").exists())) {
						var parts,
							ext = (parts = file.split("/").pop().split(".")).length > 1 ? parts.pop() : ""; // �������� ����������
						ext = $.trim(ext);
						if (ext.toLowerCase() == "sig") {
							if (Activate_remove_disk === "Y") {
								// ������� ������������� ��������
								$('<span id="TBSignDiskr" class="menu-popup-item menu-popup-no-icon" onclick="TrustedSign(' + id + ', \'remove\', \'disk\')"><span class="menu-popup-item-icon"></span><span class="menu-popup-item-text" title="' + BX.message('TBSIGN_NOSIGN_DOWNLOAD') + '">' + BX.message('TBSIGN_NOSIGN_DOWNLOAD') + '</span><span class="menu-popup-item-left"></span><span class="menu-popup-item-right"></span></span>' +
								'<div class="popup-window-hr"><i></i></div>').insertAfter($("#popup-window-content-menu-popup-grid_action_pm").find(".menu-popup-items  span:first"));
							}
							if (Activate_cosign_disk === "Y") {
								// �������� ������� � ���������
								$('<span id="TBSignDiskc" class="menu-popup-item menu-popup-no-icon" onclick="TrustedSign(' + id + ', \'cosign\', \'disk\')"><span class="menu-popup-item-icon"></span><span class="menu-popup-item-text" title="' + BX.message('TBSIGN_ADD_SIGN_TITLE') + '">' + BX.message('TBSIGN_SIGN') + '</span><span class="menu-popup-item-left"></span><span class="menu-popup-item-right"></span></span>' +
								'<div class="popup-window-hr"><i></i></div>').insertAfter($("#popup-window-content-menu-popup-grid_action_pm").find(".menu-popup-items  span:first"));
							}
						} else {
							// ��������� ��������
							if (Activate_sign_disk === "Y") {
								$('<span id="TBSignDisk" class="menu-popup-item menu-popup-no-icon" onclick="TrustedSign(' + id + ', \'sign\', \'disk\')"><span class="menu-popup-item-icon"></span><span class="menu-popup-item-text" title="' + BX.message('TBSIGN_SIGN') + '">' + BX.message('TBSIGN_SIGN') + '</span><span class="menu-popup-item-left"></span><span class="menu-popup-item-right"></span></span>' +
								'<div class="popup-window-hr"><i></i></div>').insertAfter($("#popup-window-content-menu-popup-grid_action_pm").find(".menu-popup-items  span:first"));
							}
						}
					}
				}, 10);
			}
		}

		event.preventDefault();
		if ((event.button == 2) || (event.button == 0) || (event.button == 1)) { // ���� �� ������ � ����� � ������� ������ ����
			try {
				var idFile = '';
				var FileName = '';
				var classClick = event.currentTarget.className;
				// ���� ����������
				if (classClick.indexOf("bx-disk-file-container") + 1) {
					// ���� �����
					if (!(event.currentTarget.children[0].children[0].className.indexOf("bx-disk-folder-icon") + 1)) {
						idFile = event.currentTarget.children[0].children[1].firstElementChild.defaultValue;
						FileName = event.currentTarget.children[0].textContent;

						insertInMenu(idFile, FileName);
					}
				}
				//���� �������
				else if (classClick.indexOf("bx-disk-table-body") + 1) {
					// ���� �����
					if (!(event.currentTarget.children[2].innerHTML.indexOf("bx-disk-folder-icon") + 1)) {
						idFile = event.currentTarget.children[0].firstChild.value;
						var obj = event.currentTarget.children;
						for (var i in obj) {
							if (obj[i].firstChild.nextSibling == null) continue;
							var objClassName = obj[i].firstChild.nextSibling.className;
							if (objClassName.indexOf("bx-disk-object-name") + 1) {
								FileName = event.currentTarget.children[i].firstElementChild.textContent;
								break;
							}

							if (i == (obj.length - 1)) {
								break;
							}
						}
						insertInMenu(idFile, FileName);
					}
				}
				// �����
				else {
					// console.log('���� (');
				}
			}
			catch (e) {
				console.error(e.message);
			}
		}

	});

});

function TrustedSign(id, alt, module) {
	$('.bx-popup-menu').css('visibility', 'hidden');
	//(new BX.CDialog({'content_url':'/docs/element/upload/0/?use_light_view=Y','width':'600','height':'200'})).Show();
	var popup = new BX.CDialog({
		'content': '<div class="bx-core-waitwindow" style="margin:0 auto; background-color:transparent; border:none; position:relative;">' + BX.message('TBSIGN_WAIT') + '</div>',
		'title': BX.message('TBSIGN_ATT'),
		'width': '400',
		'height': '80'
	});
	popup.Show();
	$(".bx-core-adm-icon-close").remove();
	var clickId = id; // id �������� �� �������� ��������
	var clickExt = alt; // alt �������� �� �������� ��������
	var clickModule = module; // module disk ��� webdav
	// ���������� 1 ������ � ����������� ����� � ��� id
	if ((clickId === "") || (clickExt === "") || (clickModule === "")) {
		popup.ClearButtons();
		popup.SetContent(BX.message('TBSIGN_NO_PARAM'));
		popup.SetButtons([BX.CDialog.prototype.btnClose]);
		console.log('id=' + id);
		console.log('alt=' + alt);
		console.log('module=' + module);
	}
	else {
		BX.ajax({
			method: "POST",
			async: true,
			url: '/bitrix/js/trusted.sign/ajax.php', // ��������� URL �
			data: {'id': clickId, 'ext': clickExt, 'module': clickModule}, // ������� id
			dataType: "text", // ��� ����������� ������
			processData: true,
			scriptsRunFirst: true,
			emulateOnload: true,
			onsuccess: function (data) { // ������ ���� ���������� �� ������� success
				if (clickExt === 'sign') { // ��������� �������
					var signature = new TBSignedData(); // ������ ������ �������
					popup.SetContent('<div class="bx-core-waitwindow" style="margin:0 auto; background-color:transparent; border:none; position:relative;">' + BX.message('TBSIGN_SIGN_POPUP1') + '</div>');
					var fileSign = signature.SignFile(data); // ���������� ����� ��������� �������
					popup.SetContent('<div class="bx-core-waitwindow" style="margin:0 auto; background-color:transparent; border:none; position:relative;">' + BX.message('TBSIGN_SIGN_POPUP2') + '</div>');
					for (var key in fileSign.error) {      // ��������� ������
						if (key === '0') {                     // ���� 0, �� ������ ���, ���������� ���������� ���������
							ajax2(clickId, fileSign.signature, clickModule);
						}
						else if (key === '5') {    // ��� 5, �� �� ������ ������� ������, ������� ���� � ������
							popup.ClearButtons();
							popup.SetContent(fileSign.error[key]);
							popup.SetButtons([
								{
									title: BX.message('TBSIGN_CRIPTOARM'),
									name: 'download',
									id: 'downloadCryptoArm',
									action: function () {
										window.open('http://www.trusted.ru/products/cryptoarm/', '_blank');
										this.parentWindow.Close();
									}
								},
								BX.CDialog.prototype.btnClose]);
						}
						else {
							popup.ClearButtons();
							popup.SetContent(fileSign.error[key]);
							popup.SetButtons([BX.CDialog.prototype.btnClose]);
						}   // � ��������� ������� ������ ���������� ��������� � ������� fileSign.error[key]
					}
				}
				else if (clickExt === 'cosign') { // ��������� ������� �����
					var signature = new TBSignedData(); // ������ ������ �������
					popup.SetContent('<div class="bx-core-waitwindow" style="margin:0 auto; background-color:transparent; border:none; position:relative;">' + BX.message('TBSIGN_COSIGN_POPUP1') + '</div>');
					var fileSign = signature.CosignFile(data); // ���������� ����� ��������� �������
					popup.SetContent('<div class="bx-core-waitwindow" style="margin:0 auto; background-color:transparent; border:none; position:relative;">' + BX.message('TBSIGN_COSIGN_POPUP2') + '</div>');
					for (var key in fileSign.error) {      // ��������� ������
						if (key === '0') {                     // ���� 0, �� ������ ���, ���������� ���������� ���������
							ajax2(clickId, fileSign.signature, clickModule);
						}
						else if (key === '5') {    // ��� 5, �� �� ������ ������� ������, ������� ���� � ������
							popup.ClearButtons();
							popup.SetContent(fileSign.error[key]);
							popup.SetButtons([
								{
									title: BX.message('TBSIGN_CRIPTOARM'),
									name: 'download',
									id: 'downloadCryptoArm',
									action: function () {
										window.open('http://www.trusted.ru/products/cryptoarm/', '_blank');
										this.parentWindow.Close();
									}
								},
								BX.CDialog.prototype.btnClose]);
						}
						else {
							popup.ClearButtons();
							popup.SetContent(fileSign.error[key]);
							popup.SetButtons([BX.CDialog.prototype.btnClose]);
						}   // � ��������� ������� ������ ���������� ��������� � ������� fileSign.error[key]
					}
				}
				else {  // �������� � ����������� ���������� ���������
					if (data == 0) {
						popup.ClearButtons();
						popup.SetContent(BX.message('TBSIGN_ERROR_DOWNLOAD_NOSIGN'));
						popup.SetButtons([BX.CDialog.prototype.btnClose]);
					}
					else if (data == 2) {
						popup.ClearButtons();
						popup.SetContent(BX.message('TBSIGN_ERROR_DOWN_NOSIGN'));
						popup.SetButtons([BX.CDialog.prototype.btnClose]);
					}
					else {
						window.open(data, '_blank');
						popup.Close();
					}
				}

			},
			onfailure: function (xhr, status, error) {
				// ���� �� ������ �� �����������, ������� ���� � �������
				popup.ClearButtons();
				popup.SetContent(BX.message('TBSIGN_ERROR_AJAX') + status + ': ' + error);
				popup.SetButtons([BX.CDialog.prototype.btnClose]);
			}
		});

		function ajax2(clickId, pkcs7, clickModule) {
			$.ajax({
				type: "POST",
				async: false,
				url: '/bitrix/js/trusted.sign/ajax.php', // ��������� URL �
				data: {'id2': clickId, 'pkcs7': pkcs7, 'clickModule': clickModule}, // ������� id
				dataType: "text", // ��� ����������� ������
				success: function (data, textStatus) { // ������ ���� ���������� �� ������� success
					if (textStatus == 'success') {
						if (data.indexOf('____SECFILTER_ACCEPT_JS') + 1) {
							popup.ClearButtons();
							popup.SetTitle(BX.message('TBSIGN_ERROR_TITLE'));
							popup.SetContent(BX.message('TBSIGN_ERROR_PROACTIVE'));
							popup.SetButtons([BX.CDialog.prototype.btnClose]);
						}
						else if (data == 1) {
							popup.ClearButtons();
							popup.SetContent('<div id="wd_upload_ok_message" style="color:#009900;"><br>' + BX.message('TBSIGN_SUCCESS'));
							+'<br></div>';
							popup.SetButtons([
								{
									title: BX.message('TBSIGN_CANCEL'),
									action: function () {
										window.location.reload();
										this.parentWindow.Close();
									}
								}
							]);
						}
						else if (data == 0) {
							popup.ClearButtons();
							popup.SetTitle(BX.message('TBSIGN_ERROR_TITLE'));
							popup.SetContent(BX.message('TBSIGN_ERROR_SAVE_FILE'));
							popup.SetButtons([BX.CDialog.prototype.btnClose]);
						}
						else {
							popup.ClearButtons();
							popup.SetTitle(BX.message('TBSIGN_ERROR_TITLE'));
							popup.SetContent(data);
							popup.SetButtons([BX.CDialog.prototype.btnClose]);
						}
					}
				},
				error: function (xhr, status, error) {
					// ���� �� ������ �� �����������, ������� ���� � �������
					popup.ClearButtons();
					popup.SetContent(BX.message('TBSIGN_ERROR_AJAX') + status + ': ' + error);
					popup.SetButtons([BX.CDialog.prototype.btnClose]);
				}
			});
		}
	}

}


